import os
import re
from abc import abstractmethod
from typing import Dict, Union, Tuple, Set, List, Callable, Any, Iterable

from potable.saveable import State
from dataanalyser.sources.textparser.textcaster import TextCaster
from dataanalyser.structure.database import Parcel, ParcelTable, Link
from dataanalyser.structure.mutative import Mutative
from dataanalyser.structure.path import Path
from dataanalyser.structure.source import Source, DuplicateFieldName

TAttributeId = Union[str, Iterable[str]]


class FileFinder(Mutative):
    EVENT_EXPORT_FIELD_ADDED = 'export_field_added'
    EVENT_EXPORT_FIELD_REMOVED = 'export_field_removed'
    EVENT_EXPORT_FIELD_RENAMED = 'export_field_renamed'

    def __init__(self):
        super().__init__()
        self.search_path = Path()

        # File attributes names associated to the function that will evaluate the attribute.
        self.file_attributes: Dict[TAttributeId, Callable[[str], Any]] = {
            'access timestamp': os.path.getatime,
            'modification timestamp': os.path.getmtime,
            'creation timestamp': os.path.getctime
        }
        # File attributes names associated its export name.
        self.file_attributes_export_names: Dict[TAttributeId, str] = {}
        # Enabled attributes, only enabled attribute will be evaluated.
        self.enabled_file_attributes = set()

        self._export_names_to_attributes: Dict[str, TAttributeId] = {}

    def __eq__(self, other: 'FileFinder') -> bool:
        if super().__eq__(other):
            return (self.search_path == other.search_path and
                    self.enabled_file_attributes == other.enabled_file_attributes and
                    self.file_attributes_export_names == other.file_attributes_export_names)
        else:
            return False

    @classmethod
    def class_path(cls):
        return 'FileFinder'

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.update([FileFinder.EVENT_EXPORT_FIELD_REMOVED,
                       FileFinder.EVENT_EXPORT_FIELD_RENAMED,
                       FileFinder.EVENT_EXPORT_FIELD_ADDED])
        return events

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.search_path.get_mutable_members())
        return mm

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'enabled_file_attributes': self.enabled_file_attributes,
            'search_path': self.search_path,
            'file_attributes_export_names': self.file_attributes_export_names
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.enabled_file_attributes = params['enabled_file_attributes']
        self.search_path = params['search_path']
        self.file_attributes_export_names = params['file_attributes_export_names']
        self._export_names_to_attributes = {exp: attr for attr, exp in self.file_attributes_export_names.items()}

    def add_attribute(self, attribute, attr_function, enabled=False):
        self.file_attributes[attribute] = attr_function
        try:
            export_name = self.file_attributes_export_names[attribute]
        except KeyError:
            base_export_name = '.'.join(attribute)
            i = 0
            export_name = base_export_name
            while base_export_name in self._export_names_to_attributes.keys():
                export_name = f'{base_export_name}_{i}'
            self.file_attributes_export_names[attribute] = export_name
        self._export_names_to_attributes[export_name] = attribute
        self.enable_attribute(attribute, enabled)

    def set_attribute_export_name(self, attribute, export_name):
        try:
            old_export_name = self.file_attributes_export_names.pop(attribute)
        except KeyError:
            pass
        else:
            self._export_names_to_attributes.pop(old_export_name)
        self.file_attributes_export_names[attribute] = export_name
        self._export_names_to_attributes[export_name] = attribute

    def get_attribute_for_export_name(self, export_name):
        return self._export_names_to_attributes[export_name]

    def rename_attribute_export_name(self, old_name, new_name):
        attribute = self._export_names_to_attributes.pop(old_name)
        self._export_names_to_attributes[new_name] = attribute
        self.file_attributes_export_names[attribute] = new_name
        self.emit(FileFinder.EVENT_EXPORT_FIELD_RENAMED, attribute, new_name)

    def remove_attribute(self, attribute):
        del self.file_attributes[attribute]
        try:
            self.enabled_file_attributes.remove(attribute)
        except KeyError:
            enabled = False
        else:
            enabled = True
        try:
            exp = self.file_attributes_export_names.pop(attribute)
        except KeyError:
            pass
        else:
            self._export_names_to_attributes.pop(exp)
            if enabled:
                self.emit(FileFinder.EVENT_EXPORT_FIELD_REMOVED, exp)

    def enable_attribute(self, attribute, enabled):
        exp_name = self.file_attributes_export_names[attribute]
        if enabled:
            if attribute in self.enabled_file_attributes:
                return
            self.enabled_file_attributes.add(attribute)
            self.emit(FileFinder.EVENT_EXPORT_FIELD_ADDED, attribute, exp_name)
        else:
            try:
                self.enabled_file_attributes.remove(attribute)
            except KeyError:
                pass
            else:
                self.emit(FileFinder.EVENT_EXPORT_FIELD_REMOVED, attribute, exp_name)

    def get_name(self):
        """
        :rtype: str
        :return: Some human readable name for the file finder.
        """
        return 'A File Finder.'

    def find_files(self) -> List[str]:
        """ Find the files corresponding to the description.
        :return: absolute path to the found files."""
        files = []
        for f in self.enumerate_files(self.search_path.path):
            if f is not None:
                files.append(f)
        return files

    @abstractmethod
    def enumerate_files(self, path: str):
        """ Find the files corresponding to the description.
        :rtype: generator[str|None]
        :return: absolute path to the found files."""
        raise NotImplementedError()

    def get_file_attributes(self, path):
        ans = {}
        for attribute in self.enabled_file_attributes:
            fct = self.file_attributes[attribute]
            ans[attribute] = fct(path)
        return ans

    def get_active_attributes_export_names(self) -> Dict[TAttributeId, str]:
        """ Returns the export names of all active attributes
        """
        return {attribute: self.file_attributes_export_names.get(attribute, '')
                for attribute in self.enabled_file_attributes}

    def get_all_attributes_export_names(self) -> Dict[TAttributeId, str]:
        """ Returns the export names of all attributes
        """
        return {attribute: export_name for attribute, export_name in self.file_attributes_export_names.items()}

    def get_files_and_attributes(self) -> Dict[str, Dict[TAttributeId, Any]]:
        """ Search files corresponding to the description and return their full path and the attributes of files.
        :return: absolute path to the found files and associated attributes."""
        return {path: self.get_file_attributes(path) for path in self.find_files()}


class FileFinderSource(Source):
    EVENT_FILE_FINDER_SET = 'file-finder-set'

    def __init__(self, name=''):
        super().__init__(name)
        self.file_finder: Union[FileFinder, None] = None

        # ids of file finder observers
        self._ff_observers: Set[int] = set()

        self.set_file_finder(FindByName())

        self.path_export_name = 'Path'
        """Export name for the file path.
        :type: str"""

    def __eq__(self, other):
        """
        :param FileFinderSource other:
        :return:
        """
        if super().__eq__(other):
            return (self.file_finder == other.file_finder and
                    self.path_export_name == other.path_export_name)
        else:
            return False

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.add(FileFinderSource.EVENT_FILE_FINDER_SET)
        return events

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'file_finder': self.file_finder,
            'user_path_export_name': self.path_export_name
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.set_file_finder(params['file_finder'])
        self.path_export_name = params['user_path_export_name']
        for value_source in self.file_finder.value_sources():
            self.connect_value_source(value_source)

    def _disconnect_file_finder(self):
        while self._ff_observers:
            obs = self._ff_observers.pop()
            self.file_finder.remove_event_observer(obs)

    def _connect_file_finder(self):
        self._ff_observers.add(self.file_finder.add_event_observer(FileFinder.EVENT_EXPORT_FIELD_REMOVED,
                                                                   self.on_export_field_removed)
                               )
        self._ff_observers.add(self.file_finder.add_event_observer(FileFinder.EVENT_EXPORT_FIELD_ADDED,
                                                                   self.on_export_field_added)
                               )

    def set_file_finder(self, file_finder: FileFinder):
        if file_finder is self.file_finder:
            return

        if self.file_finder is not None:
            # Removes previous file_finder
            self.remove_child_mutative(self.file_finder)
            old_export_names = set(self.file_finder.get_active_attributes_export_names().values())
            self._disconnect_file_finder()
        else:
            old_export_names = set()

        # Set new file_finder
        self.add_child_mutative(file_finder)
        self.file_finder = file_finder
        self._connect_file_finder()
        self.emit(FileFinderSource.EVENT_FILE_FINDER_SET)

        # Updates exported fields
        new_export_name = set(self.file_finder.get_active_attributes_export_names().values())
        if self.manager:
            for field in old_export_names - new_export_name:
                self.manager.remove_field(field)
            for field in new_export_name - old_export_names:
                self.manager.add_field(self, field)

    def on_export_field_removed(self, file_finder: FileFinder, _, field):
        if self.manager is None:
            return
        self.manager.remove_field(field)

    def on_export_field_added(self, file_finder: FileFinder, _, field):
        if self.manager is None:
            return
        old_field = field
        field = self.get_new_field_name(field)
        if field != old_field:
            file_finder.rename_attribute_export_name(old_field, field)
        self.manager.add_field(self, field)

    def get_new_field_name(self, field):
        if field == self.path_export_name:
            i = 0
            base_field = f'{field}_{{}}'
            field = base_field.format(i)
            while field in self.get_data_fields():
                field = base_field.format(i)
        return field

    def do_evaluate(self):
        file_path_list = []
        attributes_values_dict: Dict[Union[str, Tuple[str]], List] = {}

        parameters_to_file = (self.name, 'fk_files')
        parameters_to_file_keys = []
        parameter_table = {(self.name, 'dummy'): [0]}
        file_table = {parameters_to_file: parameters_to_file_keys, self.path_export_name: file_path_list}
        export_names = self.file_finder.get_active_attributes_export_names()

        # Prepares the table to store the attributes values of each file found
        for file_attribute in self.file_finder.file_attributes.keys():
            try:
                attribute_export_name = export_names[file_attribute]
            except KeyError:
                continue
            else:
                attribute_values = []
                file_table[attribute_export_name] = attribute_values
                # Fast track to access to the values of a file attribute
                attributes_values_dict[file_attribute] = attribute_values

        # Fills the output tables with data
        for i, (input_file, attributes) in enumerate(self.file_finder.get_files_and_attributes().items()):
            file_path_list.append(input_file)
            parameters_to_file_keys.append(0)
            for attribute, value in attributes.items():
                try:
                    attributes_values_dict[attribute].append(value)
                except KeyError:
                    pass
        return Parcel([ParcelTable(parameter_table), ParcelTable(file_table)],
                      [Link(1, 0, parameters_to_file)])

    def get_data_fields(self):
        ans = {self.path_export_name}
        if self.file_finder is not None:
            file_finder_exp = set(self.file_finder.get_active_attributes_export_names().values())
            if file_finder_exp.intersection(ans):
                raise DuplicateFieldName()
            ans.update(file_finder_exp)
        return ans

    def do_rename_field(self, field, new_name):
        if field == self.path_export_name:
            self.path_export_name = new_name
        elif field in self.file_finder.get_active_attributes_export_names().values():
            attribute = self.file_finder.get_attribute_for_export_name(field)
            self.file_finder.set_attribute_export_name(attribute, new_name)
        else:
            raise KeyError('{} not found in {}'.format(field, self.name))


TRegexGroupId = Tuple[str, str, str]


class FindByRegex(FileFinder):
    EVENT_REGEX_CHANGED = 'regex-changed'

    def __init__(self):
        super().__init__()
        self._regex: str = ''
        self.match_subdirs: bool = False
        """If True, name must also match the subdirectories."""

        self.group_caster: Dict[TRegexGroupId, TextCaster] = {}
        """TextCaster associated to each group in the regex."""

        self._match: Dict[str, re.Match] = {}
        self.group_index: Dict[TRegexGroupId, int] = {}
        """Index of the group in the regex"""

    def __eq__(self, other: 'FindByRegex') -> bool:
        if super().__eq__(other):
            return (self.regex == other.regex and
                    self.match_subdirs == other.match_subdirs and
                    self.group_caster == other.group_caster)
        else:
            return False

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.add(FindByRegex.EVENT_REGEX_CHANGED)
        return events

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'regex': self.regex,
            'match_subdirs': self.match_subdirs,
            'group_caster': self.group_caster
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        enabled_file_attributes = set(self.enabled_file_attributes)
        self.regex = params['regex']
        self.enabled_file_attributes = enabled_file_attributes
        self.match_subdirs = params['match_subdirs']
        self.group_caster = params['group_caster']

    def get_name(self):
        return 'Regex'

    @property
    def regex(self) -> str:
        return self._regex

    @regex.setter
    def regex(self, regex: str):
        self._regex = regex
        c_re = re.compile(regex)
        group_indexes = c_re.groupindex.values()

        old_group_indexes = self.group_index
        # Gets indexes of groups in the regex
        self.group_index = {}
        for i in range(1, c_re.groups + 1):
            if i not in group_indexes:
                self.group_index[('regex', 'index', str(i))] = i - 1

        for group_name, i in c_re.groupindex.items():
            self.group_index[('regex', 'name', group_name)] = i - 1

        # Removes groups that are not in the regex anymore
        old_groups = set(old_group_indexes.keys())
        to_remove = old_groups - self.group_index.keys()
        to_remove_indexes = {g: i for g, i in old_group_indexes.items() if g in to_remove}
        to_add = self.group_index.keys() - old_groups

        # Before removing, tries to attribute parameters of fields to remove to new fields.
        new_groups_attributes = self.find_modified_group(to_remove_indexes, to_add)

        for old_group in old_groups - self.group_index.keys():
            self._remove_regex_group(old_group)

        # Adds new groups
        old_groups = set(self.group_caster.keys())
        for group_name in self.group_index.keys() - old_groups:
            self._add_regex_group(group_name)

        for group, attributes in new_groups_attributes.items():
            self.set_attribute_export_name(group, attributes[0])
            self.enable_attribute(group, attributes[1])

        self.emit(FindByRegex.EVENT_REGEX_CHANGED)

    def find_modified_group(self, old_groups_indexes: Dict[TRegexGroupId, int], new_groups: Set[TRegexGroupId]):
        new_groups_attributes = {}
        for old_group, old_group_index in old_groups_indexes.items():
            for new_group in new_groups:
                if self.group_index[new_group] == old_group_index:
                    new_groups_attributes[new_group] = (self.file_attributes_export_names.get(old_group, ''),
                                                        old_group in self.enabled_file_attributes)
        return new_groups_attributes

    def _remove_regex_group(self, group_name: TRegexGroupId):
        self.remove_attribute(group_name)
        mm = self.group_caster.pop(group_name)
        self.remove_child_mutative(mm)

    def _add_regex_group(self, group_name: TRegexGroupId):
        self.add_attribute(group_name, lambda p: self.get_match_group(p, group_name), False)
        mm = TextCaster()
        self.group_caster[group_name] = mm
        self.add_child_mutative(mm)

    def get_match_group(self, path: str, group_name: TRegexGroupId):
        group_value = self._match[path].groups()[self.group_index[group_name]]
        return self.group_caster[group_name].evaluate(group_value)

    def enumerate_files(self, path: str):
        self._match = {}
        for root, dirs, files in os.walk(path):
            for fn in files:
                if self.match_subdirs:
                    path_chk = os.path.join(os.path.relpath(root, self.search_path.path), fn)
                else:
                    path_chk = fn
                match = re.match(self.regex, path_chk)
                if match:
                    file_path = os.path.join(root, fn)
                    self._match[file_path] = match
                    yield file_path
                else:
                    yield None


class FindByName(FileFinder):
    EVENT_FILE_NAME_CHANGED = 'file-name-changed'

    def __init__(self):
        super().__init__()
        self.file_name = ''

    def __eq__(self, other: 'FindByName') -> bool:
        if super().__eq__(other):
            return self.file_name == other.file_name
        else:
            return False

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.add(FindByName.EVENT_FILE_NAME_CHANGED)
        return events

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('file_name', self.file_name)
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.file_name = state.get_parameters()['file_name']

    def get_name(self):
        return 'Name'

    def enumerate_files(self, path: str):
        ans = []
        path = os.path.join(path, self.file_name)
        if os.path.exists(path):
            ans.append(path)
        for f in ans:
            yield f


FindByName.register_class()
FindByRegex.register_class()
FileFinderSource.register_class()
