from abc import abstractmethod
from typing import Set, List, Any, Dict

from dataanalyser.structure.mutative import Mutative


class TextStructureParser(Mutative):
    EVENT_FIELD_ADDED = 'field_added'
    EVENT_FIELD_REMOVED = 'field_removed'
    EVENT_REQUEST_FIELD_RENAMING = 'request_field_rename'
    EVENT_FIELD_RENAMED = 'field_renamed'
    
    def __init__(self):
        super().__init__()

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.update(
            [TextStructureParser.EVENT_FIELD_ADDED,
             TextStructureParser.EVENT_FIELD_REMOVED,
             TextStructureParser.EVENT_FIELD_RENAMED])
        return events

    @abstractmethod
    def parse_data(self, structure_content: List[str]) -> 'StructureData':
        """
        :param structure_content: The data to parse.
        :return: The values associated to each data item
        """
        raise NotImplementedError()

    @classmethod
    def class_path(cls):
        return 'TextStructureParser'

    @abstractmethod
    def get_data_fields(self) -> List[Any]:
        """
        :return: the fields names handled by the parser."""
        raise NotImplementedError()

    @abstractmethod
    def rename_data_field(self, field, new_name):
        """
        :param field:
        :param new_name:
        :return:
        """
        raise NotImplementedError()


class StructureData:
    """Data returned by a TextStructureParser."""
    __slots__ = ['singles', 'couples']

    def __init__(self, singles: Dict[str, Any] = None, couples: Dict[str, List[Any]] = None):
        if singles is None:
            singles = {}
        if couples is None:
            couples = {}
        self.singles: Dict[str, Any] = singles
        """Fields that are always associated to a single entry are stored here with their entry."""

        self.couples: Dict[str, List[Any]] = couples
        """Fields that can have more than one entry are stored here with their entries."""

    def __eq__(self, other):
        if not other.singles == self.singles:
            return False
        if not other.couples == self.couples:
            return False
        return True

    def update(self, other):
        self.singles.update(other.singles)
        self.couples.update(other.couples)
