from abc import abstractmethod
from itertools import zip_longest
from typing import List, Dict, Set, Any, Optional

import numpy as np

from potable.saveable import State
from dataanalyser.sources.textparser.basetextparser import TextStructureParser, StructureData
from dataanalyser.sources.textparser.textcaster import TextCaster
from dataanalyser.structure.mutative import Mutative, MutableMemberSetter


class TableParser(TextStructureParser):
    def __init__(self):
        super().__init__()
        self.delimiter: str = '\t'
        """Values delimiter."""
        self.data_in_row: bool = False
        """True if values are stored in rows"""
        self.has_header: bool = False
        """True if the table has header. The header is the first row or column if data are stored in column or rows
        respectively."""
        self.columns: List[Column] = []
        self.names: Dict[str, int] = {}
        """Value names"""
        self.columns_parsers: List[Dict[str, ColumnParser]] = []
        """Parsers to use for each column associated to the identifier of the data"""
        self.parser_observers: Dict[str, Set[int]] = {}
        """Observers connected to parsers"""

    def __eq__(self, other):
        if super().__eq__(other):
            return (self.delimiter == other.delimiter and
                    self.data_in_row == other.data_in_row and
                    self.has_header == other.has_header and
                    self.columns == other.columns and
                    self.columns_parsers == other.columns_parsers)
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'delimiter': self.delimiter,
            'data_in_row': self.data_in_row,
            'has_header': self.has_header,
            'columns': self.columns,
            'columns_parsers': self.columns_parsers
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        # Clears actual data
        while self.columns:
            self.remove_column(-1)
        self.delimiter = params['delimiter']
        self.data_in_row = params['data_in_row']
        self.has_header = params['has_header']
        self.columns = params['columns']
        self.columns_parsers = params['columns_parsers']
        for i, column in enumerate(self.columns_parsers):
            for name, parser in column.items():
                self.add_child_mutative(parser)
                self.names[name] = i
                self.emit(TableParser.EVENT_FIELD_ADDED, name)

    def get_mutable_members(self):
        mm = super().get_mutable_members()

        for column, column_parsers in zip(self.columns, self.columns_parsers):
            mm.extend(column.get_mutable_members())
            for parser in column_parsers.values():
                mm.extend(parser.get_mutable_members())
        return mm

    def check_name_exists(self, name):
        return name in self.names.keys()

    def _name_must_not_exist(self, name):
        if self.check_name_exists(name):
            raise KeyError('"{}" is already defined'.format(name))

    def _name_must_exist(self, name):
        if not self.check_name_exists(name):
            raise KeyError('"{}" not found'.format(name))

    def add_column(self, column: 'Column') -> int:
        """
        :param column: The column to add.
        :return: The index of the newly added column, index of the column if already in the parser.
        """
        self.columns.append(column)
        self.columns_parsers.append({})
        return len(self.columns)-1

    def insert_column(self, column_index: int, column: 'Column'):

        self.columns.insert(column_index, column)
        self.columns_parsers.insert(column_index, {})

    def add_parser(self, column_index, data_name, column_parser):
        self._name_must_not_exist(data_name)
        self.add_child_mutative(column_parser)
        self.columns_parsers[column_index][data_name] = column_parser
        self.names[data_name] = column_index
        self.emit(TextStructureParser.EVENT_FIELD_ADDED, data_name)

    def change_parser(self, data_name, new_parser):
        column = self.names[data_name]
        self.remove_child_mutative(self.columns_parsers[column][data_name])
        self.columns_parsers[column][data_name] = new_parser
        self.add_child_mutative(new_parser)

    def rename_parser(self, old_name, new_name):
        self._name_must_not_exist(new_name)
        column = self.names.pop(old_name)
        parsers = self.columns_parsers[column]
        parser = parsers.pop(old_name)
        parsers[new_name] = parser
        self.names[new_name] = column

    def remove_column(self, column_index):
        del self.columns[column_index]
        parsers = self.columns_parsers.pop(column_index)
        for parser in parsers.values():
            self.remove_child_mutative(parser)
        to_remove = [name for name, i in self.names.items() if i == column_index]
        for name in to_remove:
            self.names.pop(name)
            self.emit(TextStructureParser.EVENT_FIELD_REMOVED, name)

    def remove_data(self, column_index, data_name):
        parser = self.columns_parsers[column_index].pop(data_name)
        self.remove_child_mutative(parser)
        self.names.pop(data_name)
        self.emit(TextStructureParser.EVENT_FIELD_REMOVED, data_name)

    def parse_data(self, structure_content):
        header, columns = self.extract_header_and_columns(structure_content)
        sd = StructureData()
        for column, parsers in zip(self.columns, self.columns_parsers):
            s_columns = column.get_columns(columns, header)
            for s_column in s_columns:
                for identifier, parser in parsers.items():
                    extracted_values = parser.extract(s_column)
                    if column.single() and parser.single():
                        sd.singles[identifier] = extracted_values[0]
                    else:
                        try:
                            sd.couples[identifier].extend(extracted_values)
                        except KeyError:
                            sd.couples[identifier] = extracted_values
        return sd

    def get_data_fields(self):
        ans = []
        for parser in self.columns_parsers:
            ans.extend(parser.keys())
        return ans

    def rename_data_field(self, field, new_name):
        self.rename_parser(field, new_name)
        self.emit(TextStructureParser.EVENT_FIELD_RENAMED, field, new_name)

    def extract_header_and_columns(self, structure_content):
        rows: List[List[str]] = [line.strip().split(self.delimiter) for line in structure_content]
        header = []
        if self.has_header:
            if self.data_in_row:
                header = [row[0] for row in rows]
                for row in rows:
                    del row[0]
            else:
                header = rows[0]
                del rows[0]

        if self.data_in_row:
            columns = rows
        else:
            columns = [list(column) for column in zip_longest(*rows, fillvalue='')]
        return header, columns


class Column(Mutative):
    """Identifies columns in the Table."""

    def __init__(self):
        super().__init__()
        self.user_name: str = self.__class__.__name__
        """Name given by the user to identify the column."""

    @classmethod
    def class_path(cls):
        return 'Column'

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('user_name', self.user_name)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.user_name = state.get_parameters()['user_name']

    @abstractmethod
    def get_columns(self, columns: List[List[str]], header: Optional[List[str]] = None) -> List[List[str]]:
        """
        :param columns: columns extracted from the file.
        :param header: header of the columns if any.
        :return: Columns handled by this object.
        """
        raise NotImplementedError()

    @abstractmethod
    def single(self) -> bool:
        """
        :return: True if the object returns always a single column."""
        raise NotImplementedError()


class NamedColumn(Column):
    def __init__(self, name=''):
        super().__init__()
        self.header_name: str = name
        """Name of the column, in case the Table has headers."""

    def __eq__(self, other):
        if super().__eq__(other):
            return self.header_name == other.header_name
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('name', self.header_name)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.header_name = state.get_parameters()['name']

    def get_columns(self, columns, header=None):
        return [columns[header.index(self.header_name)]]

    def single(self):
        return True


class IndexColumn(Column):
    def __init__(self, index=0, enable_stepping=False, step=1):
        super().__init__()
        self.index = index
        """Index of the column"""
        self.enable_stepping: bool = enable_stepping
        """Enables stepping through all columns of the file."""
        self.step: int = step
        """Size of the step."""

        self.add_mutable_member('index', MutableMemberSetter('index', 0, int))

    def __eq__(self, other):
        if super().__eq__(other):
            return (self.index == other.index and
                    self.enable_stepping == other.enable_stepping and
                    self.step == other.step)
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'index': self.index,
            'enable_stepping': self.enable_stepping,
            'step': self.step
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.index = params['index']
        self.enable_stepping = params['enable_stepping']
        self.step = params['step']

    def get_columns(self, columns, header=None):
        if self.enable_stepping:
            assert self.step != 0
            return [columns[i] for i in range(self.index, len(columns), self.step)]
        else:
            return [columns[self.index]]

    def single(self):
        return not self.enable_stepping


class ColumnParser(Mutative):
    def __init__(self):
        super().__init__()
        self.text_caster: Optional[TextCaster] = None
        self.set_text_caster(TextCaster())

    def __eq__(self, other):
        if super().__eq__(other):
            return self.text_caster == other.text_caster

    @classmethod
    def class_path(cls):
        return 'ColumnParser'

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('text_caster', self.text_caster)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.set_text_caster(state.get_parameters()['text_caster'])

    def set_text_caster(self, text_caster: TextCaster):
        if self.text_caster is not None:
            self.remove_child_mutative(self.text_caster)
        self.text_caster = text_caster
        self.add_child_mutative(self.text_caster)

    def extract(self, column: List[Any]):
        """Extracts the data from the given column.
        :param column: the column from the table (or row if data are in rows)"""
        return [np.array([self.text_caster.evaluate(value) for value in column])]

    def single(self) -> bool:
        """
        :return: True if the object returns always a single value."""
        return True


class ColumnIndex(ColumnParser):
    """Extracts indexes of the input column."""

    def __init__(self, index=0, enable_stepping=False):
        super().__init__()
        self.index: int = index
        self.enable_stepping: bool = enable_stepping
        """Enable stepping through the input column.
        For each step a list of one value will be append to the output list."""
        self.step: int = 1
        """Step size between each values."""

        self.add_mutable_member('index', MutableMemberSetter('index', 0, int))

    def __eq__(self, other):
        if super().__eq__(other):
            return (self.index == other.index and
                    self.enable_stepping == other.enable_stepping and
                    self.step == other.step)
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'index': self.index,
            'enable_stepping': self.enable_stepping,
            'step': self.step
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.index = params['index']
        self.enable_stepping = params['enable_stepping']
        self.step = params['step']

    def extract(self, column):
        if self.enable_stepping:
            assert self.step != 0
            return [self.text_caster.evaluate(column[i]) for i in range(self.index, len(column), self.step)]
        else:
            return [self.text_caster.evaluate(column[self.index])]

    def single(self):
        return not self.enable_stepping


class ColumnSubArray(ColumnParser):
    """Extracts sub arrays of the input column."""

    def __init__(self):
        super().__init__()
        self.start_index: int = 0
        self.length: int = 1
        self.enable_stepping: bool = False
        """Enable stepping through the input column.
        For each step a list of one value will be append to the output list."""
        self.step: int = 1
        """Step size between each values."""

    def __eq__(self, other):
        if super().__eq__(other):
            return (self.start_index == other.start_index and
                    self.length == other.length and
                    self.enable_stepping == other.enable_stepping and
                    self.step == other.step)
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'start_index': self.start_index,
            'length': self.length,
            'enable_stepping': self.enable_stepping,
            'step': self.step
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.start_index = params['start_index']
        self.length = params['length']
        self.enable_stepping = params['enable_stepping']
        self.step = params['step']

    def extract(self, column):
        if self.enable_stepping:
            assert self.step != 0
            result = [column[i:i + self.length] for i in range(self.start_index,
                                                               len(column) - self.start_index - self.length,
                                                               self.step)]
        else:
            result = [column[self.start_index:self.start_index + self.length]]
        return [np.array([self.text_caster.evaluate(value) for value in sub_array]) for sub_array in result]

    def single(self):
        return not self.enable_stepping


IndexColumn.register_class()
NamedColumn.register_class()

ColumnParser.register_class()
ColumnIndex.register_class()
ColumnSubArray.register_class()

TableParser.register_class()
