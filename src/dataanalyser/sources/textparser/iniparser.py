from configparser import ConfigParser
from typing import Optional, Dict

from potable.saveable import State, Saveable
from dataanalyser.sources.textparser.basetextparser import TextStructureParser, StructureData
from dataanalyser.sources.textparser.textcaster import TextCaster


class IniOption(Saveable):
    def __init__(self, section: Optional[str] = None, option: Optional[str] = None,
                 caster: Optional[TextCaster] = None):
        super().__init__()
        self.section: Optional[str] = section
        self.option: Optional[str] = option
        if caster is None:
            caster = TextCaster()
        self.caster: TextCaster = caster

    def __eq__(self, o: object) -> bool:
        if not super().__eq__(o):
            return False
        return (self.section == o.section and
                self.option == o.option and
                self.caster == o.caster)

    @classmethod
    def class_path(cls):
        return 'IniOption'

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'section': self.section,
            'option': self.option,
            'caster': self.caster
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.section = params['section']
        self.option = params['option']
        self.caster = params['caster']


IniOption.register_class()


class IniParser(TextStructureParser):
    def __init__(self):
        super().__init__()
        self.data_items: Dict[str, IniOption] = {}
        """ Name of the returned data associated to the section and option of the element."""

    def __eq__(self, other: 'IniParser') -> bool:
        if super().__eq__(other):
            return self.data_items == other.data_items
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('data_items', self.data_items)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.data_items = state.get_parameters()['data_items']

    def parse_data(self, structure_content):
        config = ConfigParser()
        config.read_file(structure_content)
        result = {}
        for name, ini_option in self.data_items.items():
            result[name] = ini_option.caster.evaluate(config[ini_option.section][ini_option.option])
        return StructureData(singles=result)

    def add_data_field(self, name, section, option, caster=None):
        ini_option: IniOption = IniOption(section, option, caster)
        self.data_items[name] = ini_option
        self.add_child_mutative(ini_option.caster)
        self.emit(TextStructureParser.EVENT_FIELD_ADDED, name)

    def remove_data_field(self, name):
        ini_option: IniOption = self.data_items.pop(name)
        self.remove_child_mutative(ini_option.caster)
        self.emit(TextStructureParser.EVENT_FIELD_REMOVED, name)

    def get_data_fields(self):
        return list(self.data_items.keys())

    def request_field_rename(self, field, new_name):
        self.emit(TextStructureParser.EVENT_REQUEST_FIELD_RENAMING, field, new_name)

    def rename_data_field(self, field, new_name):
        value = self.data_items.pop(field)
        self.data_items[new_name] = value
        self.emit(TextStructureParser.EVENT_FIELD_RENAMED, field, new_name)


IniParser.register_class()
