from typing import Optional, Any

from potable.saveable import State
from dataanalyser.structure.mutative import Mutative, MutableMemberSetter


class TextCaster(Mutative):
    def __init__(self, caster: Optional['TextCaster']=None):
        """
        :param caster: caster used to copy common parameters to the new caster.
        """
        super().__init__()
        self.raise_on_exception = True
        self.value_on_exception = ''
        self.add_mutable_member('value_on_exception', MutableMemberSetter('Value on exception', ''))
        if caster is not None:
            self.raise_on_exception = caster.raise_on_exception
            self.value_on_exception = caster.value_on_exception
            self.mutable_members['value_on_exception'] = caster.mutable_members['value_on_exception'].copy()

    def __eq__(self, o: object) -> bool:
        if super().__eq__(o):
            return (self.raise_on_exception == o.raise_on_exception and
                    self.value_on_exception == o.value_on_exception)
        return False

    @classmethod
    def class_path(cls):
        return 'TextCaster'

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'raise_on_exception': self.raise_on_exception,
            'value_on_exception': self.value_on_exception
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.raise_on_exception = params['raise_on_exception']
        self.value_on_exception = params['value_on_exception']

    def cast(self, text: str) -> Any:
        """Cast the input string to the desired class.
        :param text: the string to cast
        :return: An object built from the input string."""
        return text

    def evaluate(self, text):
        try:
            ans = self.cast(text)
        except Exception:
            if self.raise_on_exception:
                raise
            else:
                try:
                    ans = self.cast(self.value_on_exception)
                except Exception:
                    ans = self.value_on_exception
        return ans


class TextToInt(TextCaster):
    def cast(self, text):
        return int(text)


class TextToFloat(TextCaster):
    def cast(self, text):
        return float(text)


class TextToBool(TextCaster):
    def __init__(self, caster: TextCaster = None):
        super().__init__(caster)
        self.true_strings = ['1', 'true']
        self.false_strings = ['0', 'false']
        self.case_sensitive = False

    def __eq__(self, other: 'TextToBool') -> bool:
        if super().__eq__(other):
            return (self.true_strings == other.true_strings and
                    self.false_strings == other.false_strings and
                    self.case_sensitive == other.case_sensitive)
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'true_strings': self.true_strings,
            'false_strings': self.false_strings,
            'case_sensitive': self.case_sensitive
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.true_strings = params['true_strings']
        self.false_strings = params['false_strings']
        self.case_sensitive = params['case_sensitive']

    def cast(self, text):
        if not self.case_sensitive:
            text = text.lower()
        text = text.strip()
        if text in self.true_strings:
            return True
        elif text in self.false_strings:
            return False
        else:
            raise ValueError('String does not match true nor false strings.')


TextCaster.register_class()
TextToInt.register_class()
TextToFloat.register_class()
TextToBool.register_class()
