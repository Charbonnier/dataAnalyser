import re
from abc import abstractmethod
from itertools import repeat
from typing import Set, List, Tuple, Iterable, Type, Optional

from potable.saveable import State, Saveable
from dataanalyser.sources.textparser.basetextparser import StructureData, TextStructureParser
from dataanalyser.structure.database import Parcel, ParcelTable, Link
from dataanalyser.structure.mutative import Mutative
from dataanalyser.structure.path import Path
from dataanalyser.structure.source import Source, DuplicateFieldName


class TextFileStructure(Mutative):
    EVENT_SET_PARSER = 'set_parser'

    """Define a structure inside a text file."""
    def __init__(self):
        super().__init__()
        self.start: MatchLine = MatchFirstLine()
        self.include_start: bool = True
        """ Include matching start line in the structure.bool"""
        self.end: MatchLine = MatchLastLine()
        self.include_end: bool = False
        """ Include matching end line in the structure."""

        self.user_name = 'TextFileStructure'

        self.structure_parser: Optional[TextStructureParser] = None

        self._started: bool = False
        """ True if the starting line has been reached."""
        self._ended: bool = False
        """ True if the end line has been reached, no new line will be added."""
        self._data: List[str] = []
        """ Content of the file for this structure."""

    def __eq__(self, other: 'TextFileStructure'):
        if super().__eq__(other):
            return (self.start == other.start and
                    self.include_end == other.include_end and
                    self.structure_parser == other.structure_parser)
        else:
            return False

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.add(TextFileStructure.EVENT_SET_PARSER)
        return events

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters(
            {'start': self.start,
             'include_start': self.include_start,
             'end': self.end,
             'include_end': self.include_end,
             'structure_parser': self.structure_parser,
             'user_name': self.user_name}
        )
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.start = params['start']
        self.include_start = params['include_start']
        self.end = params['end']
        self.include_end = params['include_end']
        self.set_structure_parser(params['structure_parser'])
        self.user_name = params['user_name']

    def clear(self):
        self.start.clear()
        self.end.clear()
        self._started = False
        self._ended = False
        self._data = []

    def add_line(self, line_content: str) -> bool:
        """ Checks that the line content can be added to the internal data.
        :param line_content: Adds the content to the data
        :return: True if the line is part of the structure
        """
        added = False
        # Checks that the structure end has not been reached.
        if self._ended:
            return added

        start = self.start.match(line_content)
        end = self.end.match(line_content)
        # If not started, checks that the start line is reached.
        if not self._started:
            self._started = start
            if not self._started:
                return added
            if self.include_start:
                self._data.append(line_content)
                added = True
        # Else, adds the line content to the data
        else:
            self._ended = end
            # If the end is reached and line content must be ignored, returns.
            if self._ended and not self.include_end:
                return False
            self._data.append(line_content)
            added = True
        return added

    def parse(self) -> StructureData:
        """ Parses the data from a file.
        :return: The data from the file.
        """
        return self.structure_parser.parse_data(self._data)

    def set_structure_parser(self, parser: TextStructureParser):
        old_parser = self.structure_parser
        if self.structure_parser is not None:
            self.remove_child_mutative(old_parser)
        self.structure_parser = parser
        self.add_child_mutative(parser)
        self.emit(TextFileStructure.EVENT_SET_PARSER, old_parser)


class TextFile(Source):
    EVENT_FILE_STRUCTURE_ADDED = 'file_structure_added'
    EVENT_FILE_STRUCTURE_REMOVED = 'file_structure_removed'

    def __init__(self, name=''):
        super().__init__(name)
        self._file_structure: List[TextFileStructure] = []
        # Observer ids for the TextFileStructure and TextStructureParser
        self._fs_obs_ids: List[Tuple[Set[int], Set[int]]] = []

        self.decimal_separator = '.'
        """Decimal separator."""

        self.file_path = Path()
        self.add_child_mutative(self.file_path)

    def __eq__(self, other: 'TextFile'):
        if super().__eq__(other):
            return (self._file_structure == other._file_structure and
                    self.decimal_separator == other.decimal_separator)
        else:
            return False

    @property
    def file_structure(self) -> Iterable[TextFileStructure]:
        for fs in self._file_structure:
            yield fs

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters(
            {'file_path': self.file_path,
             'file_structure': self._file_structure,
             'decimal_separator': self.decimal_separator})
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.file_path = params['file_path']
        while self._file_structure:
            self.remove_file_structure(0)
        for fs in params['file_structure']:
            self.append_file_structure(fs)
        self.decimal_separator = params['decimal_separator']

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.file_path.get_mutable_members())
        for structure in self._file_structure:
            mm.extend(structure.structure_parser.get_mutable_members())
        return mm

    def do_evaluate(self):
        content_to_file_field = (self.name, 'c_to_f')
        content_to_file_list = []
        content_table = {content_to_file_field: content_to_file_list}
        file_table = {}

        structure_data = self.parse_file(self.file_path.path)
        for k, single in structure_data.singles.items():
            try:
                file_table[k].append(single)
            except KeyError:
                file_table[k] = [single]
        if structure_data.couples:
            couples_length = len(next(iter(structure_data.couples.values())))
            content_to_file_list.extend(repeat(0, couples_length))
            for k, couples in structure_data.couples.items():
                content_table[k] = couples

        return Parcel([ParcelTable(file_table), ParcelTable(content_table)],
                      [Link(1, 0, content_to_file_field)])

    def get_data_fields(self):
        ans = set()
        for structure in self._file_structure:
            structure_fields = set(structure.structure_parser.get_data_fields())
            if structure_fields.intersection(ans): raise DuplicateFieldName()
            ans.update(structure_fields)
        return ans

    def do_rename_field(self, field, new_name):
        for structure in self._file_structure:
            if field in structure.structure_parser.get_data_fields():
                structure.structure_parser.rename_data_field(field, new_name)
                break
        else:
            raise KeyError('{} not found in {}'.format(field, self.name))

    def append_file_structure(self, file_structure: TextFileStructure):
        self._file_structure.append(file_structure)
        self.add_child_mutative(file_structure)
        self._fs_obs_ids.append((set(), set()))
        fs_index = len(self._file_structure) - 1
        self.connect_file_structure(fs_index)
        parser = file_structure.structure_parser
        if self.manager is not None and parser:
            for field in parser.get_data_fields():
                self.manager.add_field(self, field)
        self.emit(TextFile.EVENT_FILE_STRUCTURE_ADDED, file_structure, fs_index)
            
    def insert_file_structure(self, file_structure: TextFileStructure, index):
        self._file_structure.insert(index, file_structure)
        self.add_child_mutative(file_structure)
        self._fs_obs_ids.insert(index, (set(), set()))
        self.connect_file_structure(index)
        sp = file_structure.structure_parser
        if sp is not None:
            for field in sp.get_data_fields():
                self.manager.add_field(self, field)
        self.emit(TextFile.EVENT_FILE_STRUCTURE_ADDED, file_structure, index)

    def get_file_structure(self, index):
        return self._file_structure[index]

    def remove_file_structure(self, index: int):
        self.disconnect_file_structure(index)
        fs = self._file_structure.pop(index)
        self._fs_obs_ids.pop(index)
        for field in fs.structure_parser.get_data_fields():
            self.manager.remove_field(field)
        self.emit(TextFile.EVENT_FILE_STRUCTURE_REMOVED, fs, index)
        
    def connect_file_structure(self, index: int):
        fs_obs, tp_obs = self._fs_obs_ids[index]
        fs = self._file_structure[index]
        tp = fs.structure_parser
        fs_obs.add(fs.add_event_observer(TextFileStructure.EVENT_SET_PARSER,
                                         lambda t, op: self.on_parser_set(t, op, tp_obs)))
        self.observe_parser(tp, tp_obs)
        
    def observe_parser(self, tp: Optional[TextStructureParser], tp_obs: Set[int]):
        if tp is None:
            return

        tp_obs.add(tp.add_event_observer(TextStructureParser.EVENT_FIELD_ADDED,
                                         lambda t, f: self.manager.add_field(self, f)))
        tp_obs.add(tp.add_event_observer(TextStructureParser.EVENT_FIELD_REMOVED,
                                         lambda t, f: self.manager.remove_field(f)))
        tp_obs.add(tp.add_event_observer(TextStructureParser.EVENT_REQUEST_FIELD_RENAMING,
                                         lambda t, f, n: self.manager.rename_field(f, n)))
    
    def disconnect_file_structure(self, index: int):
        fs_obs, tp_obs = self._fs_obs_ids[index]
        fs = self._file_structure[index]
        tp = fs.structure_parser
        for obs in fs_obs:
            fs.remove_event_observer(obs)
        for obs in tp_obs:
            tp.remove_event_observer(obs)
    
    def on_parser_set(self, file_structure: TextFileStructure, old_parser: TextStructureParser, observers: Set[int]):
        if old_parser is not None:
            for obs in observers:
                old_parser.remove_event_observer(obs)
            for field in old_parser.get_data_fields():
                self.manager.remove_field(field)
        self.observe_parser(file_structure.structure_parser, observers)
    
    def parse_file(self, path):
        file_result = StructureData()
        for fs in self.file_structure:
            fs.clear()
        with open(path, 'r') as f:
            for line in f:
                for fs in self.file_structure:
                    fs.add_line(line)
        for fs in self.file_structure:
            file_result.update(fs.parse())
        return file_result


class MatchLine(Saveable):
    def __init__(self):
        pass

    def __eq__(self, o: object) -> bool:
        if type(o) != type(self):
            return False
        return True

    @classmethod
    def class_path(cls):
        return 'MatchLine'

    @staticmethod
    def register_match(finder: Type['MatchLine'], name: Optional[str] = None):
        """ Register a new file finder.
        :param finder: The file finder class
        :param name: Name to display to the user
        """
        if name is None:
            name = finder.__name__
        MatchLine.classes[finder] = name

    @abstractmethod
    def match(self, line_content: str) -> bool:
        """
        :param line_content: content of the line as returned by File.readline
        :return: True on match
        """
        raise NotImplementedError()

    def clear(self):
        """ Reinitializes the object internal parameters when handling a new file.
        """
        pass


class MatchFirstLine(MatchLine):
    def __init__(self):
        super().__init__()
        self.first_reached = False

    def __eq__(self, o: object) -> bool:
        if super().__eq__(o):
            return self.first_reached == o.first_reached
        return False

    def match(self, line_content: str) -> bool:
        if self.first_reached:
            return False
        else:
            self.first_reached = True
            return True

    def clear(self):
        self.first_reached = False


class MatchLastLine(MatchLine):
    def match(self, line_content: str) -> bool:
        return line_content == ''


class MatchLineNumber(MatchLine):
    def __init__(self):
        super().__init__()
        self._first_line = True
        self._line_number = 0
        self.line_to_match = 0

    def __eq__(self, other: 'MatchLineNumber') -> bool:
        if super().__eq__(other):
            return self.line_to_match == other.line_to_match
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('line_to_match', self.line_to_match)
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        try:
            self.line_to_match = state.get_parameters()['line_to_match']
        except KeyError:
            pass

    def clear(self):
        self._first_line = True
        self._line_number = 0

    def match(self, line_content):
        if self._first_line:
            self._first_line = False
        else:
            self._line_number += 1
        return self.line_to_match == self._line_number


class MatchLineRegex(MatchLine):
    def __init__(self):
        super().__init__()
        self.regex = ''

    def __eq__(self, other: 'MatchLineRegex'):
        if super().__eq__(other):
            return self.regex == other.regex
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('regex', self.regex)
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        try:
            self.regex = state.get_parameters()['regex']
        except KeyError:
            pass

    def match(self, line_content):
        return re.match(self.regex, line_content[:-1]) is not None


class MatchLineContent(MatchLine):
    def __init__(self):
        super().__init__()
        self.content = ''

    def __eq__(self, other: 'MatchLineContent') -> bool:
        if super().__eq__(other):
            return self.content == other.content
        else:
            return False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('content', self.content)
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        try:
            self.content = state.get_parameters()['content']
        except KeyError:
            pass

    def match(self, line_content):
        return line_content[:-1] == self.content


MatchFirstLine.register_class()
MatchLastLine.register_class()
MatchLineContent.register_class()
MatchLineRegex.register_class()
MatchLineNumber.register_class()
TextFile.register_class()
TextFileStructure.register_class()
