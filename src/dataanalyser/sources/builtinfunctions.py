import numexpr3 as ne
import sys


MAX_CACHE = 100
expression_order = []
expression_dict = {}


def evaluate_expression(expression, **kwargs):
    """ Uses numexpr3 to evaluate a mathematical expression.

    :param str expression: The expression to evaluate.
    :param kwargs: Parameters of the expression
    :return:

    All functions supported by numexpr3 are supported this includes:
    - trigonometric (sin, cos, tan, arcsin, arccos, arctan)
    - hyperbolic
    - logarithm (log10, )
    """

    sys._getframe().f_locals.update(kwargs)

    try:
        numExpr = expression_dict[expression]
    except KeyError:
        numExpr = ne.NumExpr(expression)
        if len(expression_order) >= MAX_CACHE:
            to_remove = expression_order.pop(0)
            del expression_dict[to_remove]
        expression_order.append(expression)
        expression_dict[expression] = numExpr
    return numExpr(**kwargs)


if __name__ == '__main__':
    print(evaluate_expression('sin(a)', a=1))
