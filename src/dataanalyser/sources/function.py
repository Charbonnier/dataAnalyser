from typing import Dict, Union, List, Any, Set, Tuple, Optional
from potable.saveable import State, Saveable
from dataanalyser.structure.database import Parcel, ParcelTable
from dataanalyser.structure.mutative import MutableMemberSetter
from dataanalyser.structure.mutstructures import SettersList
from dataanalyser.structure.source import Source
from dataanalyser.sources.builtinfunctions import evaluate_expression
import inspect


class Function:
    def __init__(self, function, friendly_name, documentation=None, signature=None):
        self.friendly_name = friendly_name
        self.module = function.__module__
        self.name = function.__name__
        self.function = function
        if documentation is None:
            documentation = inspect.getdoc(function)
        self.documentation = documentation

        self.function_parameters_names: List[str] = []
        self.function_parameters: List[inspect.Parameter] = []

        self.has_signature = False
        self.n_positional = 0
        self.keywords = set()
        self.has_positionals = False
        self.has_kwords = False
        self.has_var_positional = False
        self.has_var_keyword = False

        if signature is None:
            try:
                signature = inspect.signature(function)
            except ValueError:
                pass
            else:
                self.has_signature = True
                self.n_positional = 0
                for i, (name, parameter) in enumerate(signature.parameters.items()):
                    kind = parameter.kind
                    if kind == inspect.Parameter.POSITIONAL_ONLY or \
                       (kind == inspect.Parameter.POSITIONAL_OR_KEYWORD and parameter.default is parameter.empty):
                        self.function_parameters_names.append(name)
                        self.function_parameters.append(parameter)
                        self.n_positional += 1
                        self.has_positionals = True
                    elif kind == inspect.Parameter.KEYWORD_ONLY or \
                         kind == inspect.Parameter.POSITIONAL_OR_KEYWORD:
                        self.keywords.add(name)
                        self.function_parameters_names.append(name)
                        self.function_parameters.append(parameter)
                        self.has_kwords = True
                    elif parameter.kind == inspect.Parameter.VAR_POSITIONAL:
                        self.has_var_positional = True
                    elif parameter.kind == inspect.Parameter.VAR_KEYWORD:
                        self.has_var_keyword = True
        self.signature = signature


class MutablePositionalSetter(MutableMemberSetter):
    def __init__(self, index: int, friendly_name: str, value, expected_type=None):
        super().__init__(friendly_name, value, expected_type)
        self.index: int = index
        self.state: Optional[Exception] = None

    def set_owner_attribute_value(self, owner: 'FunctionSource', member, value):
        owner.args[self.index] = value


class PositionalList(SettersList):
    @classmethod
    def mutative_id(cls, index):
        return f'args.{index}'

    def build_mm_setter(self, mutative: 'FunctionSource', index: int, constant=None) -> MutablePositionalSetter:
        """

        :param FunctionSource mutative:
        :param int index:
        :param Any constant:
        :return:
        """
        name, state = mutative.positional_argument_name(index)
        try:
            pos_arg = mutative.args_mm_cache.pop(-1)
        except IndexError:
            pos_arg = MutablePositionalSetter(index, name, constant)
        else:
            pos_arg.index = index
            pos_arg.friendly_name = name
        pos_arg.state = state
        return pos_arg

    def remove(self, mutative: 'FunctionSource', index: int) -> MutablePositionalSetter:
        pos_arg = super().remove(mutative, index)
        name = pos_arg.friendly_name

        if name in mutative.kwargs.keys():
            mutative.update_keyword_argument_state(name)
        return pos_arg


class MutableKeywordSetter(MutableMemberSetter):
    def __init__(self, keyword, value, expected_type=None):
        super().__init__(keyword, value, expected_type)
        self.keyword = keyword

    def set_owner_attribute_value(self, owner: 'FunctionSource', member, value):
        owner.kwargs[self.keyword] = value


class ReturnedField(Saveable):
    def __init__(self, name=None, ignored=None):
        self.name = name
        self.ignored = ignored

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters(
            {'name': self.name,
             'ignored': self.ignored}
        )
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.name = params['name']
        self.ignored = params['ignored']


ReturnedField.register_class()


class FunctionSource(Source):
    functions: Dict[str, Function] = {}

    POSITIONAL_INSERTED_EVENT = 'positional_inserted_event'
    POSITIONAL_MOVED_EVENT = 'positional_moved_event'
    POSITIONAL_REMOVED_EVENT = 'positional_removed_event'

    @staticmethod
    def register_function(unique_name: str, function_object: Function):
        """
        :param unique_name: Unique name for the function.
        :param function_object: Descriptor of the function
        """

        fct = FunctionSource.functions.get(unique_name)
        if fct is not None:
            raise ValueError('{0} is already in used by {1}.{2}'.format(unique_name, fct.module, fct.name))
        FunctionSource.functions[unique_name] = function_object

    @staticmethod
    def state_keyword_error(keyword):
        return ValueError('No keyword "{0}" in signature'.format(keyword))

    @staticmethod
    def state_duplicate_error(keyword):
        return ValueError('Argument "{0}" provided more than once'.format(keyword))

    @staticmethod
    def state_to_many_arguments(n):
        return ValueError('Function takes only {0} arguments'.format(n))

    def __init__(self, args=None, kwargs=None, function_id='identity'):
        super().__init__()
        self._function_id = function_id
        self._set_function(function_id)

        self.args: List[Any] = []  # The values of the arguments, used during evaluation
        self.kwargs: Dict[str, Any] = {}  # The values of the keyword arguments, used during evaluation

        self.kwargs_state: Dict[str, Optional[Exception]] = {}
        """  False for arguments not matching the function signature """
        self.args_mm: PositionalList = PositionalList([], self.args)
        """ Mutable members for positional arguments """
        self.args_mm_cache: List[MutablePositionalSetter] = []
        """ Cache for removed arguments, used during function configuration to help the user """
        self.kwargs_mm: Dict[str, MutableKeywordSetter] = {}
        """ Mutable members for keyword arguments """
        self.build_arguments(args, kwargs)
        self.dispatch_result = False
        """True if the result of the function must be dispatched between several fields"""
        self._returned_fields: List[ReturnedField] = []
        """Names of the returned fields """

    def build_arguments(self, arguments, kw_arguments):
        if arguments is not None:
            for arg in arguments:
                self.append_positional_argument(value=arg)
        if kw_arguments is not None:
            for keyword, kwarg in kw_arguments.items():
                self.add_keyword_argument(keyword, value=kwarg)

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'n_args': len(self.args),
            'kwa_names': [k for k in self.kwargs.keys()],
            'function_id': self.function_id,
            'dispatch_result': self.dispatch_result,
            'returned_fields': self._returned_fields,
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        params = state.get_parameters()
        n_args = params['n_args']
        kwa_names = params['kwa_names']
        self._set_function(params['function_id'])
        self.build_arguments([None for _ in range(n_args)],
                             {k: None for k in kwa_names})
        self.dispatch_result = params['dispatch_result']
        self._returned_fields = params['returned_fields']
        super().set_state(state)

    @property
    def function_id(self) -> str:
        """
        :return: The identifier of the function."""
        return self._function_id

    @function_id.setter
    def function_id(self, function_id: str):
        """
        :param function_id: The identifier of the function to use.
        """
        self._set_function(function_id)

        self.update_arguments()

    @property
    def function(self):
        """
        :rtype: Function
        :return: The function object used.
        """
        return self._function_object

    def positional_argument_name(self, index: int) -> Tuple[str, Optional[ValueError]]:
        """
        :param index: index of the argument
        :return: The name and the state of the argument
        """
        name = None
        state = None
        if self.function is not None and self.function.has_signature:
            if index < self.function.n_positional:
                name = self.function.function_parameters_names[index]
                if name in self.kwargs.keys():
                    self.kwargs_state[name] = FunctionSource.state_duplicate_error(name)
            elif self.function.has_var_positional:
                name = 'vararg {0}'.format(index - self.function.n_positional)
            else:
                # To many arguments
                state = FunctionSource.state_to_many_arguments(self.function.n_positional)
        if name is None:
            name = 'argument {0}'.format(index)
        return name, state

    def update_positional_argument_state(self, index: int):
        self.args_mm[index].state = self.positional_argument_name(index)[1]

    def update_keyword_argument_state(self, keyword: str):
        state = None
        if self.function is not None and self.function.has_signature:
            parameters = self.function.signature.parameters
            try:
                p = parameters[keyword]
            # keyword is not in the function signature...
            except KeyError:
                # ... but the function accepts variable keywords
                if self.function.has_var_keyword:
                    state = None
                # ... the function will not accept it
                else:
                    state = FunctionSource.state_keyword_error(keyword)
            # keyword is in the signature
            else:
                # ok: this parameter is keyword only
                if p.kind == inspect.Parameter.KEYWORD_ONLY:
                    state = None
                # error: this parameter is dedicated to variable positionals
                elif p.kind == inspect.Parameter.VAR_POSITIONAL:
                    state = FunctionSource.state_keyword_error(keyword)
                # error: the name is used by positional arguments
                else:
                    index = self.function.function_parameters_names.index(keyword)
                    if index < len(self.args):
                        state = FunctionSource.state_duplicate_error(keyword)
                        self.args_mm[index].state = state
        self.kwargs_state[keyword] = state

    def append_positional_argument(self, value=None):
        setter = self.args_mm.append(self, value)
        self.emit(FunctionSource.POSITIONAL_INSERTED_EVENT, setter, len(self.args_mm) - 1)

    def insert_positional_argument(self, index: int, value=None):
        setter = self.args_mm.insert(self, index, value)
        self.emit(FunctionSource.POSITIONAL_INSERTED_EVENT, setter, index)

    def move_positional_argument(self, index: int, destination: int):
        self.args_mm.move(self, index, destination)
        self.emit(FunctionSource.POSITIONAL_MOVED_EVENT, index, destination)

    def remove_positional_argument(self, index: int):
        setter = self.args_mm.remove(self, index)
        self.emit(FunctionSource.POSITIONAL_REMOVED_EVENT, setter, index)

    def set_positional_value(self, index: int, mutable: MutableMemberSetter):
        if self.function.has_var_positional:
            while len(self.args) <= index:
                self.append_positional_argument(0)
        self.args_mm.set_value(index, mutable.value_source)

    def get_positionals(self):
        for i in range(self.function.n_positional):
            yield self.positional_argument_name(i), self.args_mm[i]

    def get_var_positionals(self):
        for i in range(self.function.n_positional, len(self.args_mm.setters)):
            yield self.args_mm[i]

    def get_positional(self, index: int):
        return self.args_mm[index]

    def keyword_argument_id(self, keyword: str):
        return 'kwargs.{0}'.format(keyword)

    def get_keywords(self):
        for name in self.function.keywords:
            yield (name, self.kwargs_state[name]), self.kwargs_mm.get(name, None)

    def get_var_keywords(self):
        for name, arg in self.kwargs_mm.items():
            if name in self.function.keywords:
                continue
            yield name, arg

    def get_keyword(self, name: str):
        return self.kwargs_mm[name]

    @property
    def returned_fields(self):
        return (rf for rf in self._returned_fields)

    def set_keyword_value(self, name: str, mutable: MutableKeywordSetter):
        try:
            kw_mm = self.kwargs_mm[name]
        except KeyError:
            self.add_keyword_argument(name, 0)
            kw_mm = self.kwargs_mm[name]
        kw_mm.value_source.set_type(mutable.value_source.type)
        kw_mm.value_source.set_value(mutable.value_source.value)

    def add_keyword_argument(self, keyword: str, value=None, mutable: Optional[MutableKeywordSetter] = None):
        if keyword in self.function.function_parameters_names[:self.function.n_positional] or \
           keyword in self.kwargs.keys():
            raise ValueError('Duplicate keyword {0}'.format(keyword))
        self.kwargs[keyword] = value
        mm = MutableKeywordSetter(keyword, value)
        if mutable is not None:
            mm.value_source.type = mutable.value_source.type
            mm.value_source.value = mutable.value_source.value
        self.add_mutable_member(self.keyword_argument_id(keyword),
                                mm)
        self.kwargs_mm[keyword] = mm
        self.update_keyword_argument_state(keyword)

    def remove_keyword_argument(self, keyword: str):
        try:
            del self.kwargs[keyword]
        except KeyError:
            pass
        try:
            del self.kwargs_mm[keyword]
        except KeyError:
            pass
        try:
            del self.mutable_members[self.keyword_argument_id(keyword)]
        except KeyError:
            pass
        try:
            index = self.function.function_parameters_names.index(keyword, 0, self.function.n_positional)
        except ValueError:
            pass
        else:
            self.update_positional_argument_state(index)

    def _set_function(self, function_id: str):
        """
        :param function_id: The identifier of the function to use.
        """
        try:
            self._function_object = FunctionSource.functions[function_id]
        except KeyError:
            self._function_object = None
        finally:
            self._function_id = function_id

    def update_arguments(self):
        n_positionals = self.function.n_positional
        for i in range(n_positionals):
            name, state = self.positional_argument_name(i)
            try:
                setter = self.args_mm[i]
                setter.friendly_name = name
                setter.state = state
            except IndexError:
                self.append_positional_argument()
        for name in self.function.keywords:
            self.update_keyword_argument_state(name)

        if self.function.has_var_positional:
            while self.args_mm_cache:
                self.append_positional_argument()

        elif len(self.args_mm.setters) > n_positionals:
            while len(self.args_mm.setters) > n_positionals:
                self.args_mm_cache.append(self.args_mm.remove(self, -1))

        for k in self.kwargs.keys():
            self.update_keyword_argument_state(k)

    def clear(self):
        self.args_mm.clear(self)

        for name in self.kwargs_mm.keys():
            self.mutable_members.pop(self.keyword_argument_id(name))
        self.kwargs_state = {}
        self.kwargs_mm = {}
        self.kwargs = {}

    def do_evaluate(self):
        if self.function is None:
            raise ValueError('Function {0} not found'.format(self.function_id))

        results = self.function.function(*self.args, **self.kwargs)
        if self.dispatch_result:
            result_table = {return_field.name: [result] for return_field, result in zip(self._returned_fields, results)
                            if not return_field.ignored}
        else:
            result_table = {self._returned_fields[0]: [results]}
        return Parcel([ParcelTable(result_table)], [])

    def get_data_fields(self) -> Set[Any]:
        return set([returned_field.name for returned_field in self._returned_fields
                    if not returned_field.ignored])

    def set_returned_field_ignored(self, index: int, ignored: bool):
        field = self._returned_fields[index]
        field.ignored = ignored
        if field.ignored == ignored:
            return
        elif field.ignored:
            self.manager.add_field(self, field.name)
        else:
            self.manager.remove_field(field.name)

    def insert_returned_field(self, field_name: str, ignored: bool = False, index: Optional[int] = None):
        if index is None:
            index = len(self._returned_fields)
        self._returned_fields.insert(index, ReturnedField(field_name, ignored))
        if not ignored and self.manager is not None:
            self.manager.add_field(self, field_name)

    def remove_returned_field(self, index: int):
        field = self._returned_fields.pop(index)
        if not field.ignored and self.manager is not None:
            self.manager.remove_field(field.name)

    def move_returned_field(self, old_index: int, new_index: int):
        if old_index == new_index:
            return

        field = self._returned_fields.pop(old_index)
        if new_index < old_index:
            insert_index = new_index
        else:
            insert_index = new_index - 1
        self._returned_fields.insert(insert_index, field)

    def do_rename_field(self, old_name: str, new_name: str):
        for field in self._returned_fields:
            if field.name == old_name:
                field.name = new_name
        super().do_rename_field(old_name, new_name)


def identity(*args, **kwargs):
    """Returns the arguments in a list for positional arguments and dict for keyword arguments"""
    ans = [a for a in args]
    ans.extend(kwargs.values())
    return ans


def debug_fct(pos_0, pos_1, pos_2, kw_1=0, kw_2=1, *args, kw_only_0=2, **kwargs):
    return 0


FunctionSource.register_function('identity', Function(identity, 'identity'))
FunctionSource.register_function('debug', Function(debug_fct, 'debug'))
FunctionSource.register_function('evaluate_expression', Function(evaluate_expression, 'Evaluate Expression'))

FunctionSource.register_class()
