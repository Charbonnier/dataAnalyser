from typing import Set

import numpy as np

from potable.saveable import State
from dataanalyser.sources.function import FunctionSource
from dataanalyser.structure.database import CursorRow, ParcelTable, Link, LinkUpdate, FIELD_ROW_ID, Cursor
from dataanalyser.structure.mutative import MutableMemberSetter
from dataanalyser.structure.mutstructures import SettersList


class GroupList(SettersList):
    @classmethod
    def mutative_id(cls, index):
        return f'groups.{index}'

    def build_mm_setter(self, mutative: 'AggregateSource', index: int, constant=None) -> MutableMemberSetter:
        return MutableMemberSetter('unnamed', None)

    def get_fields(self):
        return [s.value_source.value for s in self.setters]


class SortList(GroupList):
    @classmethod
    def mutative_id(cls, index):
        return f'sort.{index}'


class AggregateSource(FunctionSource):
    ASCENDING = 0
    DESCENDING = 1

    EVENT_GROUP_ADDED = 'group_added'
    EVENT_GROUP_REMOVED = 'group_removed'
    EVENT_ORDER_ADDED = 'order_added'
    EVENT_ORDER_REMOVED = 'order_removed'
    EVENT_ORDER_MOVED = 'order_moved'

    def __init__(self, args=None, kwargs=None, function_id='identity', n_groups=0, n_orders=0):
        super().__init__(args, kwargs, function_id)
        self.groups = GroupList([], [])  # Fields used to group values in different aggregates
        self.order = SortList([], [])  # Fields to sort values in an aggregate.
        self.direction = AggregateSource.ASCENDING
        self.build_groups(n_groups, n_orders)

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.update([
            AggregateSource.EVENT_GROUP_ADDED,
            AggregateSource.EVENT_GROUP_REMOVED,
            AggregateSource.EVENT_ORDER_ADDED,
            AggregateSource.EVENT_ORDER_REMOVED,
            AggregateSource.EVENT_ORDER_MOVED,
        ])
        return events

    def build_groups(self, n_groups, n_orders):
        self.groups = GroupList([], [])
        self.order = SortList([], [])
        for i in range(n_groups):
            self.groups.append(self)
        for i in range(n_orders):
            self.order.append(self)

    def remove_group(self, index) -> MutableMemberSetter:
        mut = self.groups.remove(self, index)
        self.emit(AggregateSource.EVENT_GROUP_REMOVED, index)
        return mut

    def append_group(self) -> MutableMemberSetter:
        mut = self.groups.append(self)
        self.emit(AggregateSource.EVENT_GROUP_ADDED, len(self.groups) - 1, mut)
        return mut

    def remove_order(self, index: int) -> MutableMemberSetter:
        mut = self.order.remove(self, index)
        self.emit(AggregateSource.EVENT_ORDER_REMOVED, index)
        return mut

    def append_order(self) -> MutableMemberSetter:
        mut = self.order.append(self)
        self.emit(AggregateSource.EVENT_ORDER_ADDED, len(self.order) - 1, mut)
        return mut

    def move_order(self, index: int, destination: int):
        self.order.move(self, index, destination)
        self.emit(AggregateSource.EVENT_ORDER_MOVED, index, destination)

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'n_groups': len(self.groups),
            'n_orders': len(self.order)
        })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        n_groups = params['n_groups']
        n_orders = params['n_orders']
        self.build_groups(n_groups, n_orders)

    def query_results(self, cursor: Cursor):

        array = np.ndarray((len(cursor), len(cursor.fields)), dtype=object)
        for i, row in enumerate(cursor):
            array[i, :] = row.get_values()
        reference_field = cursor.reference_field
        reference_field_index = cursor.fields_indexes[reference_field]

        key_indices = [cursor.fields_indexes[k] for k in self.groups.get_fields()]
        sort_indices = [cursor.fields_indexes[k] for k in self.order.get_fields()]
        sort_columns = [array[:, i] for i in key_indices]
        sort_columns.extend([array[:, i] for i in sort_indices])

        sorted_i = np.lexsort(sort_columns)
        sorted_rows = array[sorted_i]

        result_field = (self.name, 'arg_to_res')
        args_to_res_link = Link(0, 1, result_field)
        link_update = LinkUpdate(FIELD_ROW_ID, reference_field, True)
        n_rows = len(sorted_rows[0])

        # extract aggregates from sorted_rows, call the function
        cur_aggregate = None
        parameters = np.ndarray((len(cursor.fields),), dtype=object)
        results = []
        aggregate_limits = []
        start_index = 0
        aggregate_len = 0
        if not key_indices:
            aggregate_limits = [(0, sorted_rows.shape[0])]
        else:
            for i, k in enumerate(sorted_rows[:, key_indices]):
                if cur_aggregate is None or k != cur_aggregate:
                    if cur_aggregate is not None:
                        aggregate_limits.append((start_index, aggregate_len))
                    cur_aggregate = sorted_rows[i, key_indices]
                    start_index = i
                    aggregate_len = 1
                else:
                    aggregate_len += 1
            # Appends the last aggregate
            aggregate_limits.append((start_index, aggregate_len))

        for i, (start, aggregate_len) in enumerate(aggregate_limits):
            if aggregate_len == 0:
                continue
            aggregate = sorted_rows[start_index:start_index + aggregate_len]
            # generate input parameters
            # Note: value for fields use for grouping must be scalar, while the rest are vectors.
            for i in range(len(parameters)):
                if i in key_indices:
                    parameters[i] = aggregate[0, i]
                else:
                    parameters[i] = aggregate[:, i]

            # calculate aggregate result
            cursor_row = CursorRow(cursor.fields_indexes, parameters)

            link_table = ParcelTable({FIELD_ROW_ID: aggregate[:, reference_field_index],
                                      result_field: np.zeros(aggregate.shape[0], dtype=int)})
            result = self.evaluate(cursor_row)
            result.tables.insert(0, link_table)
            result.links.append(args_to_res_link)
            results.append(result)

            yield i, results, [link_update]


AggregateSource.register_class()
