from abc import abstractmethod

from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.figure import Figure

from potable.saveable import State
from dataanalyser.structure.mutative import Mutative, DataStacker, MutableMemberSetter
from dataanalyser.structure.view import ViewRenderer, View, ViewItem
from dataanalyser.views.scale import LinearScale
from dataanalyser.views.mplproperties import TextProperties, GridProperties

Y_AXIS = 'y'

X_AXIS = 'x'

DEFAULT_AXES = 'Default'


class PlotRenderer(ViewRenderer):
    def __init__(self) -> None:
        super().__init__()
        self.figure = Figure()
        self._canvas = self.build_canvas()
        self._axes_dict = {}
        ":type: dict[str, matplotlib.axes.Axes]"
        self.make_axes()

    @property
    def canvas(self):
        return self._canvas

    def build_canvas(self):
        return FigureCanvasAgg(self.figure)

    def get_all_axes(self):
        return self._axes_dict.items()

    def make_axes(self):
        axes = self.figure.add_subplot(111)
        self._axes_dict['Default'] = axes

    def clear(self):
        super().clear()
        self.figure.clear()
        self.make_axes()

    def get_axes(self, axes_name='Default'):
        return self._axes_dict[axes_name]

    def save(self, output_file):
        self.figure.savefig(output_file)


class PlotView(View):
    def __init__(self, name='Plot'):
        self._default_renderer = PlotRenderer()
        super().__init__(self._default_renderer, name)
        xaxis = Axis(X_AXIS)
        yaxis = Axis(Y_AXIS)

        self.figure_width = 6.4
        self.figure_height = 4.8
        self.figure_dpi = 100.

        self.axis_dict = {(DEFAULT_AXES, X_AXIS): xaxis,
                          (DEFAULT_AXES, Y_AXIS): yaxis}
        ":type: dict[(str, str), Axis]"
        self.title = PlotTitle('Plot')
        self.plots = []
        ":type: list[Plot]"

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'title': self.title,
            'plots': self.plots,
            'figure_width': self.figure_width,
            'figure_height': self.figure_height,
            'figure_dpi': self.figure_dpi
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.title = params['title']
        self.plots = params['plots']
        self.figure_width = params['figure_width']
        self.figure_height = params['figure_height']
        self.figure_dpi = params['figure_dpi']

    def clear(self):
        super().clear()
        for plot in self.plots:
            plot.clear()

    def clear_category(self):
        super().clear_category()
        for view_item in self.get_view_elements():
            if not self.is_view_item_to_render(view_item, with_category_enabled=True):
                continue
            view_item.item_to_render.clear()

    def get_view_elements(self):
        yield ViewItem(self.title, False, True)
        for axis in self.axis_dict.values():
            yield ViewItem(axis, False, True)
        for plot in self.plots:
            yield ViewItem(plot, plot.categories_enabled, plot.rendering_enabled)

    def query_data_for_plot(self, database, plot):
        assert plot in self.plots
        view_item = ViewItem(plot, plot.categories_enabled, plot.rendering_enabled)
        category_filter = self.categories_filter.get_filter(self.category_index)
        plot.clear()
        self.query_data_for_view_item(database, view_item, category_filter,
                                      with_category_enabled=view_item.category_enabled)

    def evaluate(self, database, renderer=None):
        """

        :param database:
        :param PlotRenderer renderer:
        :return:
        """
        super().evaluate(database, renderer)
        self.setup_figure(renderer)

    def setup_figure(self, renderer):
        if renderer is None:
            renderer = self._default_renderer
        renderer.figure.set_figwidth(self.figure_width)
        renderer.figure.set_figheight(self.figure_height)
        renderer.figure.set_dpi(self.figure_dpi)

    def generate_view(self, renderer):
        """
        :param PlotRenderer renderer:
        :return:
        """
        if renderer is None:
            renderer = self._default_renderer
        renderer.set_need_redraw()
        renderer.figure.suptitle(self.title.title, **self.title.properties())

        for name, axes in renderer.get_all_axes():
            self.setup_axes(axes, name)

        axes = renderer.get_axes()
        x_axis = self.axis_dict[(DEFAULT_AXES, X_AXIS)]
        y_axis = self.axis_dict[(DEFAULT_AXES, Y_AXIS)]
        x_axis.scale.clear()
        y_axis.scale.clear()
        target = AxesTarget(axes, x_axis, y_axis)
        for plot in self.plots:
            if not plot.rendering_enabled:
                continue
            plot.draw(target)
        x_min, x_max = x_axis.scale.read_limit_with_margin()
        y_min, y_max = y_axis.scale.read_limit_with_margin()
        axes.set_xlim(x_min, x_max)
        axes.set_ylim(y_min, y_max)

    def setup_axes(self, axes, name):
        axes.clear()
        try:
            axis_cfg = self.axis_dict[(name, X_AXIS)]
        except KeyError:
            pass
        else:
            self.setup_axis(axes.xaxis, axis_cfg)
            axes.set_xscale(axis_cfg.scale.mpl_scale_name())
        try:
            axis_cfg = self.axis_dict[(name, Y_AXIS)]
        except KeyError:
            pass
        else:
            self.setup_axis(axes.yaxis, axis_cfg)
            axes.set_yscale(axis_cfg.scale.mpl_scale_name())

    def setup_axis(self, axis, axis_config):
        """
        :param matplotlib.axis.Axis axis:
        :param src.views.plot.Axis axis_config:
        :return:
        """
        axis_config.setup_axis(axis)


class Plot(Mutative):

    @classmethod
    def class_path(cls):
        return 'Plot'

    def __init__(self):
        super().__init__()
        self._data_provider = self.make_data_provider()
        ":type: PlotDataProvider"
        self.rendering_enabled = True
        self.categories_enabled = False

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('data_provider', self._data_provider)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self._data_provider = state.get_parameters()['data_provider']

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.get_data_provider().get_mutable_members())
        return mm

    def make_data_provider(self):
        return DataStacker()

    def get_data_provider(self):
        return self._data_provider

    def clear(self):
        self._data_provider.clear()

    def draw(self, axes):
        """
        :param AxesTarget axes:
        :return:
        """
        self.draw_data_provider(axes)

    @abstractmethod
    def draw_data_provider(self, axes_target):
        """
        :param AxesTarget axes_target:
        :return:
        """
        raise NotImplementedError()


class PlotTitle(Mutative):
    def __init__(self, title='Plot'):
        super().__init__()
        self.title = title
        self.add_mutable_member('title', MutableMemberSetter('title', title, str))
        self.title_properties = TextProperties()

    @classmethod
    def class_path(cls):
        return 'PlotTitle'

    def properties(self):
        return self.title_properties.get_properties()

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('title_properties', self.title_properties)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.title_properties = state.get_parameters()['title_properties']


class AxesTarget:
    def __init__(self, axes, x_axis, y_axis):
        """

        :param matplotlib.axes.Axes axes:
        :param Axis x_axis:
        :param Axis y_axis:
        """
        self.axes = axes
        self.x_axis = x_axis
        self.y_axis = y_axis


class Axis(Mutative):
    def __init__(self, name='Axis'):
        super().__init__()
        self.title = PlotTitle(name)
        self.scale = LinearScale()
        self.grid_major = GridProperties('major')
        self.grid_minor = GridProperties('minor')

    @classmethod
    def class_path(cls):
        return 'Axis'

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.title.get_mutable_members())
        return mm

    def setup_axis(self, axis):
        """
        :param matplotlib.axis.Axis axis:
        :return:
        """
        axis.set_label_text(self.title.title, **self.title.properties())
        axis.grid(**self.grid_major.get_properties())
        axis.grid(**self.grid_minor.get_properties())

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'title': self.title,
            'scale': self.scale,
            'grid_major': self.grid_major,
            'grid_minor': self.grid_minor
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.title = params['title']
        self.scale = params['scale']
        self.grid_major = params['grid_major']
        self.grid_minor = params['grid_minor']


PlotView.register_class()
Plot.register_class()
PlotTitle.register_class()
Axis.register_class()
