from matplotlib import cm
from matplotlib.colors import hex2color, LinearSegmentedColormap

from potable.saveable import State
from dataanalyser.structure.mutative import MutableMemberStacker, DataStacker
from dataanalyser.views.scale import LinearScale


class Color(tuple):
    def __new__(cls, rgba):
        assert len(rgba) in (3, 4)
        return super(Color, cls).__new__(cls, rgba)


class ColorFactory(DataStacker):
    @classmethod
    def class_path(cls):
        return 'ColorFactory'

    @staticmethod
    def hex2color(color):
        if isinstance(color, str):
            return Color(hex2color(color))
        return color


class ConstantColorFactory(ColorFactory):
    def __init__(self):
        super().__init__()
        self.colors = []
        self.add_mutable_member('colors', MutableMemberStacker('color', Color((0, 0, 0, 0)), expected_type=Color))

    def clear(self):
        super().clear()
        self.colors.clear()

    def make_values(self):
        pass

    def do_get_values(self):
        return self.colors


class BaseGradientFactory(ColorFactory):
    def __init__(self):
        super().__init__()
        self.cleared = True
        self._map = cm.get_cmap('jet')
        self._under = Color((0., 0., 0., 1.))
        self._over = Color((0., 0., 0., 1.))
        self._bad = Color((0., 0., 0., 1.))
        self.color_parameter = []
        self.colors = []
        self.color_scale = LinearScale()
        self.add_mutable_member('color_parameter', MutableMemberStacker('color_parameter', 0))

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'under': self._under,
            'over': self._over,
            'bad': self._bad
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.under = params['under']
        self.over = params['over']
        self.bad = params['bad']

    @property
    def map(self):
        return self._map

    @map.setter
    def map(self, map):
        """
        :param matplotlib.colors.Colormap map:
        :return:
        """
        self._map = map
        self.map.set_under(self.under)
        self.map.set_over(self.over)

    @property
    def under(self):
        return self._under

    @under.setter
    def under(self, under):
        """
        :param str|[int, int, int] under:
        :return:
        """
        self._under = self.hex2color(under)
        self.map.set_under(self._under)

    @property
    def over(self):
        return self._over

    @over.setter
    def over(self, over):
        """
        :param str|[int, int, int] over:
        :return:
        """
        self._over = self.hex2color(over)
        self.map.set_over(self._over)

    @property
    def bad(self):
        return self._bad

    @bad.setter
    def bad(self, bad):
        """
        :param str|[int, int, int] bad:
        :return:
        """
        self._bad = self.hex2color(bad)
        self.map.set_bad(self._bad)

    def do_get_values(self):
        return self.colors

    def clear(self):
        super().clear()
        self.color_parameter.clear()
        self.colors = None

    def make_values(self):
        positions = self.color_scale.get_positions(self.color_parameter)
        self.colors = self.make_colors_from_positions(positions)

    def make_colors_from_positions(self, positions):
        return self.map(positions)


class MapColorFactory(BaseGradientFactory):
    def __init__(self):
        super().__init__()

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('map_name', self._map.name)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.set_map_from_name(state.get_parameters()['map_name'])

    def set_map_from_name(self, name):
        """
        :param str name:
        :return:
        """
        self.map = cm.get_cmap(name)


class GradientColorFactory(BaseGradientFactory):
    def __init__(self):
        super().__init__()
        self._start_color = Color((0., 1., 0.))
        self._end_color = Color((1., 0., 0.))
        self._update_map = self._do_map_update
        self.color_parameter = []
        self.colors = []
        self.color_scale = LinearScale()

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'start_color': self.start_color,
            'end_color': self.end_color
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.start_color = params['start_color']
        self.end_color = params['end_color']

    @BaseGradientFactory.map.getter
    def map(self):
        self._update_map()
        return self._map

    @property
    def start_color(self):
        return self._start_color

    @start_color.setter
    def start_color(self, start_color):
        """
        :param str|[int, int, int] start_color:
        :return:
        """
        self._start_color = self.hex2color(start_color)
        self._update_map = self._do_map_update

    @property
    def end_color(self):
        return self._end_color

    @end_color.setter
    def end_color(self, end_color):
        """
        :param str|[int, int, int] end_color:
        :return:
        """
        self._end_color = self.hex2color(end_color)
        self._update_map = self._do_map_update

    def _do_map_update(self):
        self._update_map = self._no_map_update
        self.map = LinearSegmentedColormap.from_list('gradient', [self.start_color, self.end_color], N=256)

    def _no_map_update(self):
        pass


ConstantColorFactory.register_class()
GradientColorFactory.register_class()
MapColorFactory.register_class()
