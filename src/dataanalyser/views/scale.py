from abc import abstractmethod
import numpy as np

from potable.saveable import Saveable


class Scale(Saveable):
    @classmethod
    def class_path(cls):
        return 'Scale'

    def __init__(self) -> None:
        super().__init__()
        self.min = MinMaxLimit(LimitFinder.MIN)
        self.max = MinMaxLimit(LimitFinder.MAX)
        self.vmin = None
        self.vmax = None

    @classmethod
    @abstractmethod
    def mpl_scale_name(cls):
        raise NotImplementedError()

    def get_limit(self, values):
        return self.min.get_limit(values), self.max.get_limit(values)

    def get_positions(self, values):
        self.prepare(values)
        try:
            positions = self.scale_function(values)
        except (ValueError, TypeError):
            positions = self.fall_back(values)
        return positions

    def fall_back(self, values):
        positions = np.zeros(len(values), float)
        n = len(values)
        for i, (j, _) in enumerate(sorted(enumerate(values), key=lambda t: t[1])):
            positions[i] = j/n
        return positions

    def clear(self):
        self.vmin, self.vmin = None, None

    def prepare(self, values):
        self.vmin, self.vmax = self.get_limit(values)

    def update(self, values):
        if self.vmax is None or self.vmin is None:
            self.prepare(values)
        else:
            vmin, vmax = self.get_limit(values)
            self.prepare([self.vmin, vmin, self.vmax, vmax])

    def read_limit(self):
        return self.vmin, self.vmax

    def read_limit_with_margin(self):
        vmin, vmax = self.read_limit()
        if vmin is None or vmax is None:
            return None, None
        extent = np.abs(np.subtract(vmin, vmax))
        vmin_m = self.min.read_limit_with_margin(vmin, extent)
        vmax_m = self.max.read_limit_with_margin(vmax, extent)
        return vmin_m, vmax_m

    @abstractmethod
    def scale_function(self, values):
        """
        :param values:
        :rtype: list[float]
        :return: A values between 0 and 1, the lower and upper limits of the scale respectively
        """
        raise NotImplementedError()


class LinearScale(Scale):
    @classmethod
    def mpl_scale_name(cls):
        return 'linear'

    def scale_function(self, values):
        return np.subtract(values, self.vmin) / (self.vmax - self.vmin)


class LogScale(Scale):
    @classmethod
    def mpl_scale_name(cls):
        return 'log'

    def scale_function(self, values):
        return np.log10(np.divide(np.absolute(values), self.vmin))/np.log10(self.vmax/self.vmin)

    def get_limit(self, values):
        filtered_values = np.ma.masked_equal(np.absolute(values), 0)
        vmin, vmax = super().get_limit(filtered_values)
        if not LogScale.check_limit(vmin):
            vmin = None
        if not LogScale.check_limit(vmax):
            vmax = None
        return vmin, vmax

    @classmethod
    def check_limit(cls, limit):
        if np.ma.is_masked(limit):
            return False
        if limit == 0.:
            return False
        return np.isfinite(limit)


LinearScale.register_class()
LogScale.register_class()


class LimitFinder(Saveable):
    MIN = 0
    MAX = 1

    @classmethod
    def class_path(cls):
        return 'LimitFinder'

    def __init__(self, mode) -> None:
        """
        :param int mode: search for min (0) or max (1)
        """
        super().__init__()
        self.mode = mode

    def get_limit(self, values):
        try:
            limit = float(self.limit_function(values))
        except (TypeError, ValueError):
            limit = self.fall_back(values)
        return limit

    def fall_back(self, values):
        if self.mode == LimitFinder.MIN:
            return 0.
        try:
            return float(len(values))
        except TypeError:
            return 1.

    @abstractmethod
    def limit_function(self, values):
        raise NotImplementedError()

    def read_limit_with_margin(self, limit, extent):
        return limit


class ConstantLimit(LimitFinder):
    def __init__(self, mode) -> None:
        super().__init__(mode)
        if mode == LimitFinder.MIN:
            limit = 0.
        else:
            limit = 1.
        self.limit = limit

    def limit_function(self, values):
        return self.limit


class MinMaxLimit(LimitFinder):
    DEFAULT_MARGIN = 0.1

    def __init__(self, mode) -> None:
        super().__init__(mode)
        self.margin = MinMaxLimit.DEFAULT_MARGIN

    def limit_function(self, values):
        if self.mode == LimitFinder.MIN:
            return np.min(values)
        else:
            return np.max(values)

    def read_limit_with_margin(self, limit, extent):
        if self.mode == LimitFinder.MIN:
            return limit - self.margin * extent
        else:
            return limit + self.margin * extent


ConstantLimit.register_class()
MinMaxLimit.register_class()
