import logging

from potable.saveable import State
from dataanalyser.structure.mutative import DataStacker, MutableMemberStacker
from dataanalyser.structure.view import View, ViewRenderer, ViewItem
from dataanalyser.utils import main_log_handler

logger = logging.getLogger(__name__)
logger.addHandler(main_log_handler)
logger.setLevel(logging.ERROR)


class TableRenderer(ViewRenderer):
    def __init__(self):
        super().__init__()
        self.body = TableBody()
        ":type: Body"
        self.delimiter = '\t'

    def save(self, output_file):
        super().save(output_file)
        header = self.body.columns_names
        fmt_lst = self.body.columns_formatters
        data = zip(*[self.body.columns_mms_values[c] for c in self.body.columns_mms_ids])

        with open(output_file, 'w') as f:
            f.write('\t'.join(header) + '\n')
            row_str = [''] * len(header)
            for row in data:
                for i, (v, fmt) in enumerate(zip(row, fmt_lst)):
                    v_str = TableRenderer.apply_format(fmt, v)

                    row_str[i] = v_str
                f.write('\t'.join(row_str))
                f.write('\n')

    @staticmethod
    def apply_format(fmt, value):
        if fmt:
            try:
                v_str = fmt.format(value)
            except (TypeError, ValueError):
                logger.warning('Format error', exec_info=True)
                v_str = str(value)
        else:
            v_str = str(value)
        return v_str

    def set_columns_data(self, body):
        """

        :param TableBody body:
        :return:
        """
        self.body = body


class Column(MutableMemberStacker):
    def set_owner_attribute_value(self, owner, member, value):
        """

        :param TableBody owner:
        :param str member:
        :param value:
        :return:
        """
        try:
            owner.columns_mms_values[member].append(value)
        except KeyError:
            owner.columns_mms_values[member] = [value]


class TableBody(DataStacker):
    def __init__(self, columns_ids=None):
        super().__init__()
        self.columns_names = []
        self.columns_formatters = []
        self.columns_mms = []
        ":type: list[Column]"
        self.columns_mms_ids = []
        self.columns_mms_values = {}
        self.build_columns_id(columns_ids)

    def build_columns_id(self, columns_ids):
        if columns_ids is not None:
            for column_id in columns_ids:
                self._new_column_with_id(column_id)

    @classmethod
    def class_path(cls):
        return 'TableBody'

    def clear(self):
        super().clear()
        self.columns_mms_values = {}

    def new_column(self, name, formatter):
        mm_index = len(self.columns_mms)
        mm_id = 'c_' + str(mm_index)
        mm_names = self.mutable_members.keys()
        name_index = mm_index
        while mm_id in mm_names:
            name_index += 1
            mm_id = 'c_' + str(name_index)
        mm = Column(mm_id, '')
        self.columns_names.append(name)
        self.columns_formatters.append(formatter)
        self.columns_mms.append(mm)
        self.columns_mms_ids.append(mm_id)
        self.add_mutable_member(mm_id, mm)
        return mm_id, mm

    def _new_column_with_id(self, column_id):
        try:
            self.mutable_members[column_id]
        except KeyError:
            pass
        else:
            raise ValueError('Column with id "{}" already exists')
        mm = Column(column_id, '')
        self.add_mutable_member(column_id, mm)
        self.columns_names.append('')
        self.columns_formatters.append('')
        self.columns_mms.append(mm)
        self.columns_mms_ids.append(column_id)

    def index_from_id(self, column_id):
        return self.columns_mms_ids.index(column_id)

    def remove_column(self, index):
        mm_id = self.columns_mms_ids.pop(index)
        self.columns_mms.pop(index)
        self.columns_formatters.pop(index)
        self.columns_names.pop(index)
        self.mutable_members.pop(mm_id)

    def rename_column(self, index, new_name):
        self.columns_names[index] = new_name

    def set_column_formatter(self, index, formatter):
        self.columns_formatters[index] = formatter

    def swap_columns(self, first, second):
        for lst in [self.columns_mms, self.columns_names, self.columns_mms_ids, self.columns_formatters]:
            mem = lst[first]
            lst[first] = lst[second]
            lst[second] = mem

    def reorder_columns(self, ids):
        index_lst = [self.columns_mms_ids.index(mm_id) for mm_id in ids]
        self.columns_names = [self.columns_names[i] for i in index_lst]
        self.columns_formatters = [self.columns_formatters[i] for i in index_lst]
        self.columns_mms = [self.columns_mms[i] for i in index_lst]
        self.columns_mms_ids = [self.columns_mms_ids[i] for i in index_lst]

    def get_n_columns(self):
        return len(self.columns_mms_ids)

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'columns_ids': self.columns_mms_ids,
            'columns_names': self.columns_names,
            'columns_formatters': self.columns_formatters
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.columns_mms_ids = []
        self.build_columns_id(params['columns_ids'])
        self.columns_names = params['columns_names']
        self.columns_formatters = params['columns_formatters']


class TableView(View):
    def __init__(self, name='Table'):
        self._default_renderer = TableRenderer()
        super().__init__(self._default_renderer, name)
        self.body = TableBody()

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('table_body', self.body)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.body = state.get_parameters()['table_body']

    def new_column(self, name, formatter):
        """

        :param str name:
        :param str formatter: format string
        :rtype: str, Column
        :return:
        """
        return self.body.new_column(name, formatter)

    def clear(self):
        super().clear()
        self.body.clear()

    def get_view_elements(self):
        # Body of the table:
        yield ViewItem(self.body, category_enabled=self.categories_filter.enabled, rendering_enabled=True)

    def generate_view(self, renderer=None):
        """

        :param TableRenderer renderer:
        :return:
        """
        if renderer is None:
            renderer = self._default_renderer
        renderer.set_columns_data(self.body)


TableBody.register_class()
TableView.register_class()
