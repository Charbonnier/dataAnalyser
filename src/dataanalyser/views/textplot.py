from potable.saveable import State
from dataanalyser.views.mplproperties import TextProperties
from dataanalyser.views.plot import Plot
from dataanalyser.structure.mutative import MutableMemberStacker, DataStacker


class PlotTextData:
    def __init__(self) -> None:
        super().__init__()
        self.x = None
        self.y = None
        self.text = None
        self.text_properties = None


class PlotTextDataProvider(DataStacker):
    def __init__(self):
        super().__init__()
        self.x = []
        self.y = []
        self.text = []
        self.text_properties = TextProperties()
        self.add_mutable_member('x', MutableMemberStacker('x', 0))
        self.add_mutable_member('y', MutableMemberStacker('y', 0))
        self.add_mutable_member('text', MutableMemberStacker('text', ''))

    @classmethod
    def class_path(cls):
        return 'PlotTextDataProvider'

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('text_properties', self.text_properties)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.text_properties = state.get_parameters()['text_properties']

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.text_properties.get_mutable_members())
        return mm

    def clear(self):
        super().clear()
        self.x = []
        self.y = []
        self.text = []

    def make_values(self):
        pass

    def do_get_values(self):
        """
        :rtype: list[PlotTextData]
        :return:
        """
        text_data = PlotTextData()
        text_data.text_properties = self.text_properties
        for x, y, text in zip(self.x, self.y, self.text):
            text_data.x = x
            text_data.y = y
            text_data.text = text
            yield text_data


class PlotText(Plot):
    def make_data_provider(self):
        return PlotTextDataProvider()

    def draw_data_provider(self, axes_target):
        """
        :param src.views.plot.AxesTarget axes_target:
        :return:
        """
        axes = axes_target.axes
        x_axis = axes_target.x_axis
        y_axis = axes_target.y_axis
        for text_data in self.get_data_provider().get_values():
            axes.text(text_data.x, text_data.y, text_data.text, **text_data.text_properties.get_properties())
            x_axis.scale.update([text_data.x])
            y_axis.scale.update([text_data.y])


PlotTextDataProvider.register_class()
PlotText.register_class()
