from matplotlib import rc_params

from potable.saveable import State
from dataanalyser.structure.mutative import Mutative


class Properties(Mutative):
    def __init__(self, properties):
        super().__init__()
        self.properties = {name: None for name in properties}
        self.use_default = {name: True for name in properties}

    @classmethod
    def class_path(cls):
        return 'MplProperties'

    def get_properties(self):
        return {name: value for name, value in self.properties.items()
                if not self.use_default[name]}

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'properties': self.properties,
            'use_default': self.use_default
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.properties.update(params['properties'])
        self.use_default.update(params['use_default'])


TEXT_PROPERTIES = {'rotation',
                   'fontsize',
                   'fontstretch',
                   'fontstyle',
                   'verticalalignment',
                   'horizontalalignment',
                   'color'}

GRID_PROPERTIES = {'color', 'linestyle', 'linewidth'}


class TextProperties(Properties):
    def __init__(self):
        super().__init__(TEXT_PROPERTIES)

    def initialize_from_text(self, text):
        """
        :param matplotlib.text.Text text:
        :return:
        """
        properties = text.properties()
        for name in TEXT_PROPERTIES:
            self.properties[name] = properties.get(name, None)


class GridProperties(Properties):
    def __init__(self, which='major'):
        super().__init__(GRID_PROPERTIES)
        self.which = which
        self.enabled = False
        params = rc_params()
        for name in GRID_PROPERTIES:
            self.properties[name] = params['grid.' + name]

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('enabled', self.enabled)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.enabled = state.get_parameters()['enabled']

    def get_properties(self):
        properties = {'which': self.which,
                      'b': self.enabled}
        if self.enabled:
            properties.update(super().get_properties())
        return properties
