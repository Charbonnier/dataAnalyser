from potable.saveable import State
from dataanalyser.views.colorfactory import ConstantColorFactory
from dataanalyser.views.plot import Plot
from dataanalyser.structure.mutative import MutableMemberStacker, DataStacker


class PlotXYData:
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y
        self.color = None
        self.label = ''
        self.linestyle = '-'
        self.linewidth = 1.
        self.marker = None
        self.markerfacecolor = None
        self.markeredgecolor = None
        self.markeredgewidth = 1.
        self.markersize = 1.
        self.fillstyle = 'full'


class PlotXYDataProvider(DataStacker):
    def __init__(self):
        super().__init__()
        self.x = []
        self.y = []
        self.label = []
        self.linewidth = []
        self.markersize = []
        self.add_mutable_member('x', MutableMemberStacker('x', []))
        self.add_mutable_member('y', MutableMemberStacker('y', []))
        self.add_mutable_member('label', MutableMemberStacker('label', ''))
        self.add_mutable_member('linewidth', MutableMemberStacker('linewidth', 1.))
        self.add_mutable_member('markersize', MutableMemberStacker('markersize', 1.))

        self.linestyle = '-'
        self.marker = None
        self.markeredgewidth = 1.
        self.fillstyle = 'full'

        self.color = ConstantColorFactory()
        self.markerfacecolor = ConstantColorFactory()
        self.markeredgecolor = ConstantColorFactory()

    @classmethod
    def class_path(cls):
        return 'PlotXYDataProvider'

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'linestyle': self.linestyle,
            'marker': self.marker,
            'markeredgewidth': self.markeredgewidth,
            'fillstyle': self.fillstyle,
            'color': self.color,
            'markerfacecolor': self.markerfacecolor,
            'markeredgecolor': self.markeredgecolor
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.linestyle = params['linestyle']
        self.marker = params['marker']
        self.markeredgewidth = params['markeredgewidth']
        self.fillstyle = params['fillstyle']
        self.color = params['color']
        self.markerfacecolor = params['markerfacecolor']
        self.markeredgecolor = params['markeredgecolor']

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        mm.extend(self.color.get_mutable_members())
        mm.extend(self.markerfacecolor.get_mutable_members())
        mm.extend(self.markeredgecolor.get_mutable_members())
        return mm

    def clear(self):
        super().clear()
        self.x = []
        self.y = []
        self.label = []
        self.linewidth = []
        self.markersize = []
        self.color.clear()
        self.markeredgecolor.clear()
        self.markerfacecolor.clear()

    def make_values(self):
        pass

    def do_get_values(self):
        """
        :rtype: list[PlotXYData]
        :return:
        """
        plot_data = PlotXYData()
        plot_data.linestyle = self.linestyle
        plot_data.marker = self.marker
        plot_data.markeredgewidth = self.markeredgewidth
        plot_data.fillstyle = self.fillstyle
        for x, y, label, linewidth, markersize, color, mfc, mec in zip(self.x, self.y, self.label,
                                                                       self.linewidth, self.markersize,
                                                                       self.color.get_values(),
                                                                       self.markerfacecolor.get_values(),
                                                                       self.markeredgecolor.get_values()):
            plot_data.x = x
            plot_data.y = y
            plot_data.color = color
            plot_data.label = label
            plot_data.linewidth = linewidth
            plot_data.markerfacecolor = mfc
            plot_data.markeredgecolor = mec
            plot_data.markersize = markersize
            yield plot_data


class PlotXY(Plot):
    def make_data_provider(self):
        return PlotXYDataProvider()

    def draw_data_provider(self, axes_target):
        """
        :param src.views.plot.AxesTarget axes_target:
        :return:
        """
        axes = axes_target.axes
        x_axis = axes_target.x_axis
        y_axis = axes_target.y_axis
        for plot_data in self.get_data_provider().get_values():
            axes.plot(plot_data.x, plot_data.y, color=plot_data.color, label=plot_data.label,
                      linestyle=plot_data.linestyle, linewidth=plot_data.linewidth,
                      marker=plot_data.marker, markeredgecolor=plot_data.markeredgecolor,
                      markeredgewidth=plot_data.markeredgewidth, markerfacecolor=plot_data.markerfacecolor,
                      markersize=plot_data.markersize, fillstyle=plot_data.fillstyle)
            x_axis.scale.update(plot_data.x)
            y_axis.scale.update(plot_data.y)


PlotXYDataProvider.register_class()
PlotXY.register_class()
