from dataanalyser.structure.mutative import DataStacker, MutableMemberStacker


class MappingData:
    def __init__(self):
        super().__init__()
        self.x = None
        self.y = None
        self.z = None


class MappingDataProvider(DataStacker):
    def __init__(self):
        super().__init__()
        self.x = []
        self.y = []
        self.z = []
        self.add_mutable_member('x', MutableMemberStacker('x', 0))
        self.add_mutable_member('y', MutableMemberStacker('y', 0))
        self.add_mutable_member('z', MutableMemberStacker('z', 0))

    @classmethod
    def class_path(cls):
        return 'MappingDataProvider'
