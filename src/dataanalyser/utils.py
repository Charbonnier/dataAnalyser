import logging
import re

from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.markers import MarkerStyle

main_log_handler = logging.FileHandler('dataanalyser.log')
main_log_handler.setFormatter(logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
main_log_handler.setLevel(logging.WARNING)


def increment_name(name):
    m = re.match('(?P<name>.+)_(?P<index>[0-9]+)$', name)
    if m is not None:
        index = int(m.groupdict()['index'])
        base_name = m.groupdict()['name']
    else:
        index = 0
        base_name = name
    return '{}_{}'.format(base_name, index + 1)


def make_marker_preview(marker, output_path, facecolor='none', edgecolor='none', previewsize=32,
                        fillstyle='full', facecoloralt='none'):
    inch_width = 0.2
    pix_width = previewsize
    fig = Figure(figsize=(inch_width, inch_width), dpi=int(pix_width / inch_width))
    canvas = FigureCanvasAgg(fig)
    ax = fig.add_subplot(111, frameon=False)
    ":type: matplotlib.axes.Axes"
    if marker not in MarkerStyle.filled_markers:
        facecolor = 'none'
    if fillstyle == 'none':
        facecolor = 'none'
    ax.plot([0], [0], marker=marker, markeredgewidth=1, markerfacecolor=facecolor, markeredgecolor=edgecolor,
            markersize=6, fillstyle=fillstyle, markerfacecoloralt=facecoloralt)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    fig.savefig(output_path, transparent=True, dpi=int(pix_width / inch_width))
