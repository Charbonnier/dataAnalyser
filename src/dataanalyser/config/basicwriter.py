#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""@package basicsaver: Defines:
    BasicWriter
"""
from abc import abstractmethod
from typing import List, Dict, Any
from potable.saveable import Serializer, Deserializer


class BasicWriter:
    """ Basic class for saving the sequencer state.
    """
    def extensions(self) -> List[str]:
        """ Returns the list of extensions available for this saver.
        """
        pass
    
    def write(self, path: str, elements):
        """ Saves the elements to the given path.
        :param str path: path of the file to create.
        :type elements: dict[str, core.config.saveable.Saveable]
        :param elements: dictionary of elements to save.
        """
        self.do_write(path, Serializer().serialize(elements))

    @abstractmethod
    def do_write(self, path: str, serialized: Dict[str, Any]):
        pass
    
    def read(self, path):
        """ Loads the element in the file.
        :param str path: path of the file to load.
        :return: dictionary of saved elements.
        """
        return Deserializer().deserialize(self.do_read(path))

    @abstractmethod
    def do_read(self, path: str) -> Dict[str, Any]:
        pass
    
    @staticmethod
    def write_class(value):
        """ Gets the module name and class name to use to save a given class.
        :param value: Value to save
        :rtype: (str, str)
        :return: module name and class name to use.
        """
        in_class = value.__class__
        return in_class.__module__, in_class.__name__
        
    @staticmethod
    def read_class(module_name, class_name):
        """ Gets the class
        :param str module_name: Name of the module to use.
        :param str class_name: Name of the class to get.
        :rtype: (str, str)
        :return: the class to build.
        """
        return module_name, class_name
