import json
from typing import Dict, Any

from dataanalyser.config.basicwriter import BasicWriter


class JsonWriter(BasicWriter):
    def extensions(self):
        return [('json', 'json')]

    def do_write(self, path: str, serialized: Dict[str, Any]):
        with open(path, 'w') as f:
            json.dump(serialized, f)

    def do_read(self, path: str) -> Dict[str, Any]:
        with open(path, 'r') as f:
            return json.load(f)
