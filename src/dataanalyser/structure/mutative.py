from abc import abstractmethod
from typing import Set, Dict

from potable.saveable import Saveable, State
from potable.observable import Observable


class ValueSource(Observable, Saveable):
    """Defines a source for a mutable member of a Mutative object:
        Constant: The value is the one to use
        Data field: The value identifies a field to use in the database."""
    CONSTANT = 0
    REFERENCE = 1

    EVENT_TYPE_CHANGED = "type_changed"
    EVENT_VALUE_CHANGED = "value_changed"

    def __init__(self, p_type=0, p_value=0):
        """
        :param int p_type: Parameter.CONSTANT or Parameter.REFERENCE
        :param Any p_value: value of the parameter
        """
        super().__init__()
        self._type = p_type
        self._value = p_value

    def __eq__(self, other):
        if self._type != other._type:
            return False
        if self._value != other._value:
            return False
        return True

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        se = super().get_supported_events()
        se.update([cls.EVENT_TYPE_CHANGED, cls.EVENT_VALUE_CHANGED])
        return se

    @property
    def type(self):
        return self._type

    @property
    def value(self):
        return self._value

    def set(self, value_source):
        self.set_type(value_source.type)
        self.set_value(value_source.value)

    def set_type(self, source_type):
        if self.type != source_type:
            old_type = self.type
            self._type = source_type
            self.emit(ValueSource.EVENT_TYPE_CHANGED, old_type)

    def set_value(self, value):
        if self.value != value:
            old_value = self.value
            self._value = value
            self.emit(ValueSource.EVENT_VALUE_CHANGED, old_value)

    @classmethod
    def class_path(cls):
        return 'ValueSource'

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'type': self.type,
            'value': self.value
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.set_type(params['type'])
        self.set_value(params['value'])


class MutableMemberSetter:
    __slots__ = ['friendly_name', 'expected_type', '_value_source']

    def __init__(self, friendly_name, value, expected_type=None):
        """
        :param str friendly_name: A name for the member
        :param Any value: The source for the value of the member.
        :param Any expected_type: An expected type, None if no special type is expected
        """
        self.friendly_name = friendly_name
        self._value_source = ValueSource(ValueSource.CONSTANT, value)
        self.expected_type = expected_type

    def __eq__(self, o: object) -> bool:
        if type(self) != type(o):
            return False
        return (self.friendly_name == o.friendly_name and
                self._value_source == o._value_source and
                self.expected_type == o.expected_type)

    @property
    def value_source(self):
        return self._value_source

    @value_source.setter
    def value_source(self, value_source: ValueSource):
        # TODO the fact that value_source is not replaced is not intuitive
        self.value_source.set_type(value_source.type)
        self.value_source.set_value(value_source.value)

    def copy(self):
        mm = self.__class__(self.friendly_name, None, self.expected_type)
        mm.value_source = ValueSource(self.value_source.type, self.value_source.value)
        return mm

    def apply(self, owner, member, references_values):
        """
        :param owner:
        :param member:
        :param dict[str, Any] references_values: references associated to their values.
        :return:
        """
        if self.value_source.type == ValueSource.CONSTANT:
            value = self.value_source.value
        else:
            reference = self.value_source.value
            value = references_values[reference]
        self.set_owner_attribute_value(owner, member, value)

    def set_owner_attribute_value(self, owner, member, value):
        setattr(owner, member, value)

    def set_constant_value(self, value):
        self.value_source.set_type(ValueSource.CONSTANT)
        self.value_source.set_value(value)

    def set_reference_value(self, reference):
        self.value_source.set_type(ValueSource.REFERENCE)
        self.value_source.set_value(reference)


class Mutative(Observable, Saveable):
    """Interface for classes with parameters that can reference an other value.
    A mutable member has either a constant value, or a reference to a value in the database."""
    EVENT_MUTABLE_MEMBER_ADDED = 'mutable_member_added'
    EVENT_MUTABLE_MEMBER_REMOVED = 'mutable_member_removed'
    EVENT_CHILD_MUTATIVE_ADDED = 'child_mutative_added'
    EVENT_CHILD_MUTATIVE_REMOVED = 'child_mutative_removed'

    def __init__(self):
        super().__init__()
        self.mutable_members = {}
        ":type: dict[str, MutableMemberSetter]"
        self.children_mutative: Dict[int, Mutative] = {}

    def __eq__(self, o: object) -> bool:
        if type(o) != type(self):
            return False
        return self.mutable_members == o.mutable_members

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        se = super().get_supported_events()
        se.update([Mutative.EVENT_MUTABLE_MEMBER_ADDED, Mutative.EVENT_MUTABLE_MEMBER_REMOVED,
                   Mutative.EVENT_CHILD_MUTATIVE_ADDED, Mutative.EVENT_CHILD_MUTATIVE_REMOVED])
        return se

    def add_child_mutative(self, child):
        """

        :param Mutative child:
        :return:
        """
        child_id = id(child)
        if child_id in self.children_mutative.keys():
            return
        self.children_mutative[id(child)] = child
        child.add_event_observer(Mutative.EVENT_CHILD_MUTATIVE_ADDED, self.on_sub_child_added)
        self.emit(Mutative.EVENT_CHILD_MUTATIVE_ADDED, [child])

    def remove_child_mutative(self, child):
        """

        :param Mutative child:
        :return:
        """
        try:
            self.children_mutative.pop(id(child))
        except KeyError:
            pass
        else:
            child.add_event_observer(Mutative.EVENT_CHILD_MUTATIVE_ADDED, self.on_sub_child_removed)
            self.emit(Mutative.EVENT_CHILD_MUTATIVE_REMOVED, [child])

    def on_sub_child_added(self, emitter: 'Mutative', children):
        """ Chains the child added event to the upper child."""
        children.append(self)
        self.emit(Mutative.EVENT_CHILD_MUTATIVE_ADDED, children)

    def on_sub_child_removed(self, emitter: 'Mutative', children):
        """ Chains the child removed event to the upper child."""
        children.append(self)
        self.emit(Mutative.EVENT_CHILD_MUTATIVE_REMOVED, children)

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter(
            'mutable_members',
            {
                name: mutable_member.value_source for name, mutable_member in self.mutable_members.items()
            })
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        for name, value_source in state.get_parameters()['mutable_members'].items():
            self.mutable_members[name].value_source = value_source

    def add_mutable_member(self, member_name: str, mutable_member: MutableMemberSetter):
        """ Adds a class member to the mutable members.
        :param str member_name: a member of the class.
        :param MutableMemberSetter mutable_member: Setter for this mutable member
        """
        self.mutable_members[member_name] = mutable_member
        self.emit(Mutative.EVENT_MUTABLE_MEMBER_ADDED, member_name, mutable_member)

    def remove_mutable_member(self, member_name: str) -> MutableMemberSetter:
        mutable_member = self.mutable_members.pop(member_name)
        self.emit(Mutative.EVENT_MUTABLE_MEMBER_REMOVED, member_name, mutable_member)
        return mutable_member

    def get_mutable_members(self):
        """
        :rtype: list[(Mutative, dict[str, MutableMemberSetter])]
        :return: Dictionnary of mutable members and the source of their values.
        """
        mm = [(self, {k: v for k, v in self.mutable_members.items()})]
        for child in self.children_mutative.values():
            mm.extend(child.get_mutable_members())
        return mm

    def configure_member(self, member_name, parameter):
        """ Set up a mutable member.
        :param str member_name: the name of the member to set up
        :param ValueSource parameter: the type of the member."""
        setup = self.mutable_members[member_name].value_source
        setup.set_type(parameter.type)
        setup.set_value(parameter.value)

    def apply_members_values(self, references_values):
        """ Based on the member type, the object owner will update the member values using this method.
        :param dict[str, Any] references_values: references associated to their values."""
        for member, mm_setter in self.mutable_members.items():
            mm_setter.apply(self, member, references_values)

    def get_required_fields(self):
        select = set()
        for value_source in self.value_sources():
            if value_source.type == ValueSource.REFERENCE:
                select.add(value_source.value)
        return select

    def do_remove(self):
        super().do_remove()
        for name, mm in self.mutable_members.values():
            mm.value_source.remove()
        self.mutable_members.clear()

    def value_sources(self):
        for _, mm_setters in self.get_mutable_members():
            for mm_setter in mm_setters.values():
                yield mm_setter.value_source


class MutableMemberStacker(MutableMemberSetter):
    def set_owner_attribute_value(self, owner, member, value):
        """
        :param PlotDataProvider owner:
        :param str member :
        :param value:
        """
        getattr(owner, member).append(value)


class DataStacker(Mutative):
    def __init__(self):
        super().__init__()
        self.cleared = True

    def clear(self):
        self.cleared = True

    def unset_cleared_flag(self):
        self.cleared = False

    @abstractmethod
    def make_values(self):
        raise NotImplementedError()

    @abstractmethod
    def do_get_values(self):
        raise NotImplementedError()

    def get_values(self):
        if self.cleared:
            self.unset_cleared_flag()
            self.make_values()
        return self.do_get_values()


ValueSource.register_class()
