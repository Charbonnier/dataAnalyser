import os
from abc import abstractmethod
from logging import Logger, WARNING, ERROR
from typing import Dict, Set

from potable.observable import Observable
from potable.saveable import State, Saveable
from dataanalyser.structure.categories import CategoriesFilter
from dataanalyser.structure.database import Request
from dataanalyser.utils import main_log_handler, increment_name


class ViewManager(Observable):
    EVENT_VIEW_ADDED = 'view_added'        # Callable[(ViewManager, View), None]
    EVENT_VIEW_REMOVED = 'view_removed'    # Callable[(ViewManager, View), None]
    EVENT_VIEW_RENAMED = 'view_renamed'    # Callable[(ViewManager, View, str), None]

    def __init__(self, data_source):
        """
        :param src.structure.database.DataBase data_source:
        :return:
        """
        super().__init__()
        self.database = data_source
        self.views: Dict[str, View] = {}
        self.logger = Logger('ViewManager')
        self.logger.setLevel(WARNING)
        self.logger.addHandler(main_log_handler)

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.update({
            ViewManager.EVENT_VIEW_ADDED,
            ViewManager.EVENT_VIEW_REMOVED,
            ViewManager.EVENT_VIEW_RENAMED
        })
        return events

    def register_view(self, view: 'View', rename=True):
        """
        :param View view:
        :param boolean rename: Rename the view if another view has the same name.
        :return:
        """
        name = view.name

        while True:
            try:
                self.views[name]
            except KeyError:
                break
            else:
                name = increment_name(name)
        self.views[name] = view
        view.name = name
        self.emit(ViewManager.EVENT_VIEW_ADDED, view)

    def remove_view(self, view: 'View'):
        self.emit(ViewManager.EVENT_VIEW_REMOVED, self.views.pop(view.name))

    def rename_view(self, view, name):
        """
        :param View view:
        :param str name:
        :return:
        """
        if name in self.views.keys():
            raise KeyError('View "{}" already exists'.format(name))
        old_name = name
        self.views.pop(view.name)
        self.views[name] = view
        view.name = name
        self.emit(ViewManager.EVENT_VIEW_RENAMED, view, old_name)

    def generate_view(self, view, renderer=None, category_index=None):
        """
        :param View view:
        :param ViewRenderer renderer:
        :param int|None category_index:
        """
        view.evaluate(self.database)
        view.evaluate_category(self.database, category_index)
        view.generate_view(renderer)

    def export_view(self, view, renderer=None, category_index=None, export_path='', add_category=False):
        """

        :param View view:
        :param ViewRenderer renderer:
        :param int|None category_index:
        :param str export_path:
        :return:
        """
        self.generate_view(view, renderer, category_index)
        view.export(renderer, force=True, export_path=export_path, add_category=add_category)

    def export_view_all_categories(self, view, renderer=None, export_path=''):
        """

        :param View view:
        :param ViewRenderer renderer:
        :param str export_path:
        :return:
        """
        view.evaluate(self.database)
        n_categories = len(view.categories_filter.get_categories())
        if n_categories == 0:
            view.generate_view(renderer)
            view.export(renderer, force=True, export_path=export_path)
        else:
            for category_index in range(n_categories):
                view.evaluate_category(self.database, category_index)
                view.generate_view(renderer)
                view.export(renderer, force=True, export_path=export_path, add_category=True)


class ViewRenderer:
    def __init__(self):
        self.need_redraw = True

    def set_need_redraw(self):
        self.need_redraw = True

    def clear_need_redraw(self):
        self.need_redraw = False

    def clear(self):
        self.set_need_redraw()

    def save(self, output_file):
        """ Write the view to the given file
        :param str output_file: path to the output file
        """
        pass


class ViewItem:
    def __init__(self, item, category_enabled=False, rendering_enabled=True):
        """

        :param src.structure.mutative.Mutative item:
        """
        self.category_enabled = category_enabled
        self.rendering_enabled = rendering_enabled
        self.item_to_render = item


class View(Saveable):
    def __init__(self, default_renderer, name='Unnamed'):
        """
        :param ViewRenderer default_renderer: The renderer used to generate the view
        :param str name: name of the view.
        """
        super().__init__()
        self.name = name
        self.category_index = 0
        self.categories_filter = CategoriesFilter()
        self._export_enabled = False
        self._export_target_folder = '.'
        self._export_target_name = ''
        self._export_target_category_format = '{path}_{category}'
        self._default_renderer = default_renderer

        self.logger = Logger('View "{}"'.format(self.name))
        self.logger.setLevel(ERROR)
        self.logger.addHandler(main_log_handler)

        self.grouping_enabled = False
        self.group_field = None
        self.active_group = None
        self.group_values = set()

    def append_new_group_value(self, value):
        self.group_values.add(value)

    def get_view_elements(self):
        """
        :rtype: list[ViewItem]
        """
        return []

    @classmethod
    def class_path(cls):
        return 'View'

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('name', self.name)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.name = state.get_parameters()['name']

    def export(self, renderer=None, force=False, export_path='', add_category=False):
        """
        :param ViewRenderer renderer: an alternative renderer to use.
        :param bool force: True to export the view even if export_enabled is False.
        :param str export_path: An alternative path where to export the view.
        :param bool add_category: Add the category value to the export path name.
        :return:
        """
        if not self.export_enabled and not force:
            return
        if renderer is None:
            renderer = self._default_renderer
        if not export_path:
            export_path = self.make_target_path()
        if not export_path:
            return
        if add_category:
            export_path = self.category_to_path(export_path)
        self.generate_view(renderer)
        renderer.save(export_path)

    def make_target_path(self):
        if not self._export_target_name:
            self.logger.warning('Export requested but no export name defined for view "{}"'.format(self.name))
            return None

        if self.categories_filter.enabled:
            target_name = self.category_to_path(self._export_target_name)
        else:
            target_name = self._export_target_name

        return os.path.join(self._export_target_folder, target_name)

    def category_to_path(self, base_path):
        base, sep = os.path.splitext(base_path)
        category = self.categories_filter.categories[self.category_index]
        return self._export_target_category_format.format(path=base, category=category) + sep

    @property
    def export_enabled(self):
        return self._export_enabled

    def enable_export(self, export):
        self._export_enabled = export

    def clear(self):
        """Clears the data stacked in the view."""
        self.group_values = set()

    def clear_category(self):
        pass

    def is_view_item_to_render(self, view_item, with_category_enabled=False):
        """

        :param ViewItem view_item:
        :param bool with_category_enabled:
        :return:
        """
        if not view_item.rendering_enabled:
            return False
        if with_category_enabled:
            if not view_item.category_enabled:
                return False
        else:
            if view_item.category_enabled:
                return False
        return True

    def query_data_for_view_item(self, database, view_item, category_filter=None, with_category_enabled=False):
        """

        :param src.structure.database.DataBase database:
        :param ViewItem view_item:
        :param FilterStructure category_filter:
        :param bool with_category_enabled:
        :return:
        """
        if not self.is_view_item_to_render(view_item, with_category_enabled):
            return

        view_item = view_item.item_to_render
        try:
            request = Request(view_item.get_required_fields(), category_filter)
            cursor = database.query(request)
        except Exception as ec:
            self.logger.error('Error while generating view "%s": %s', self.name, str(ec))
        else:
            for i, row in enumerate(cursor):
                for mm, _ in view_item.get_mutable_members():
                    mm.apply_members_values(row)

    def query_data_for_view_items(self, database, with_category_enabled=False):
        """ Queries the data for the views items.

        :param src.structure.database.DataBase database:
        :param bool with_category_enabled: True if categories are enabled
        :return:
        """
        if with_category_enabled:
            category_filter = self.categories_filter.get_filter(self.category_index)
        else:
            category_filter = None

        for view_item in self.get_view_elements():
            self.query_data_for_view_item(database, view_item, category_filter=category_filter,
                                          with_category_enabled=with_category_enabled)

    def evaluate(self, database, renderer=None):
        """ Generates the view by calling the method generate_view
        :param src.structure.database.DataBase database:
        :param ViewRenderer renderer: An alternative renderer to use for evaluation.
        :rtype: ViewRenderer
        :return: the renderer used to generate the view
        """
        self.clear()
        self.evaluate_category_filter(database)
        self.query_data_for_view_items(database)

    def evaluate_category_filter(self, database):
        """

        :param src.structure.database.DataBase database:
        :return:
        """
        self.categories_filter.query_categories(database)

    def evaluate_category(self, database, category_index=None):
        """

        :param src.structure.database.DataBase database:
        :param int|None category_index:
        :return:
        """
        self.category_index = category_index
        self.clear_category()
        self.query_data_for_view_items(database, True)

    def render_view(self, renderer=None):
        """ Request data
        :param ViewRenderer renderer: An alternative renderer to use for evaluation.
        :rtype: ViewRenderer
        :return: the renderer used to generate the view
        """
        if renderer is None:
            renderer = self._default_renderer
        renderer.clear()
        self.generate_view(renderer)
        return renderer

    @abstractmethod
    def generate_view(self, renderer):
        """ Override this method to implement a new view.
        :param ViewRenderer renderer: An alternative renderer to use for evaluation."""
        raise NotImplementedError()
