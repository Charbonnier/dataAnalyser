import os

from potable.saveable import State
from dataanalyser.structure.mutative import Mutative, MutableMemberSetter
from dataanalyser.structure.source import ContextSource

EVENT_PATH_MODIFIER_CHANGED = 'modifier_changed'


class Path(Mutative):
    MOD_RELATIVE_TO_WORKING = 'Working'
    MOD_RELATIVE_TO_CONFIGURATION = 'Configuration'
    MOD_RELATIVE_TO_DATA = 'Data'
    MOD_RELATIVE_TO_APPLICATION = 'Application'
    MOD_RELATIVE_TO_USER = 'User'

    def __init__(self):
        super().__init__()
        self.raw_path = '.'
        self._origin_path = '.'
        self.origin_path_mms = MutableMemberSetter('_origin_path', '.', str)
        self.add_mutable_member('raw_path', MutableMemberSetter('raw_path', '.', str))
        self._modifier = Path.MOD_RELATIVE_TO_WORKING

    def __eq__(self, other):
        if super().__eq__(other):
            return self.modifier == other.modifier
        return False

    @classmethod
    def class_path(cls):
        return 'Path'

    @property
    def modifier(self):
        return self._modifier

    @modifier.setter
    def modifier(self, modifier: str):
        self._modifier = modifier
        self.emit(EVENT_PATH_MODIFIER_CHANGED)

    def get_mutable_members(self):
        mm = super().get_mutable_members()
        smm = mm[0][1]
        if self.modifier == Path.MOD_RELATIVE_TO_WORKING:
            self.origin_path_mms.set_constant_value('')
        elif self.modifier == Path.MOD_RELATIVE_TO_USER:
            self.origin_path_mms.set_constant_value(os.path.expanduser('~'))
        elif self.modifier == Path.MOD_RELATIVE_TO_APPLICATION:
            self.origin_path_mms.set_reference_value(ContextSource.APP_DIR_NAME)
        elif self.modifier == Path.MOD_RELATIVE_TO_CONFIGURATION:
            self.origin_path_mms.set_reference_value(ContextSource.CONFIG_DIR_NAME)
        elif self.modifier == Path.MOD_RELATIVE_TO_DATA:
            self.origin_path_mms.set_reference_value(ContextSource.DATA_DIR_NAME)
        smm['_origin_path'] = self.origin_path_mms
        return mm

    def apply_members_values(self, references_values):
        self.origin_path_mms.apply(self, '_origin_path', references_values)
        super().apply_members_values(references_values)

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('modifier', self.modifier)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.modifier = state.get_parameters()['modifier']

    @property
    def path(self):
        if os.path.isabs(self.raw_path):
            return self.raw_path
        elif self.modifier == Path.MOD_RELATIVE_TO_WORKING:
            return self.raw_path
        else:
            return os.path.join(self._origin_path, self.raw_path)


Path.register_class()
