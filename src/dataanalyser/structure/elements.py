import os

from dataanalyser.structure.source import SourceManager
from dataanalyser.structure.view import ViewManager


class Elements:
    def __init__(self, database):
        """
        :param src.structure.database.DataBase database: 
        """
        self.source_manager: SourceManager = SourceManager(database)
        self.view_manager: ViewManager = ViewManager(database)
        self.application_path = os.path.dirname(os.path.dirname(__file__))
        self.configuration_path = '.'
        self.data_path = '.'
