from abc import abstractmethod, ABCMeta
from typing import List, Any

from dataanalyser.structure.mutative import MutableMemberSetter, Mutative, ValueSource


class SettersList(metaclass=ABCMeta):
    def __init__(self, mutative_list: List[MutableMemberSetter], mutative_values: List[Any]):
        self.setters: List[MutableMemberSetter] = mutative_list
        self.setter_values: List[Any] = mutative_values

    def __getitem__(self, item):
        return self.setters[item]

    def __len__(self):
        return len(self.setters)

    @classmethod
    def mutative_id(cls, index: int) -> str:
        pass

    @abstractmethod
    def build_mm_setter(self, mutative: Mutative, index: int, constant: Any = None) -> MutableMemberSetter:
        pass

    def get_mutatives(self):
        for m in self.setters:
            yield m

    def clear(self, mutative: Mutative):
        while self.setters:
            self.remove(mutative, -1)

    def append(self, mutative: Mutative, constant: Any = None) -> MutableMemberSetter:
        index = len(self.setters)
        setter = self.build_mm_setter(mutative, index, constant)
        self.setters.append(setter)
        mutative.add_mutable_member(self.mutative_id(index), setter)

        self.setter_values.append(constant)
        return setter

    def insert(self, mutative: Mutative, index: int, constant: Any = None):
        new_setter = self.append(mutative, constant)
        v_type = new_setter.value_source.type
        v_value = new_setter.value_source.value
        for i in reversed(range(index, len(self.setters) - 1)):
            self.move_setter_data(i, i + 1)
        self.setters[index].value_source.set_type(v_type)
        self.setters[index].value_source.set_value(v_value)
        self.setter_values.insert(index, self.setter_values.pop())

        return self.setters[index]

    def move(self, mutative: Mutative, old_index: int, new_index: int):
        value_source = self.setters[old_index].value_source
        self.setters[old_index].value_source = self.setters[new_index].value_source
        self.setters[new_index].value_source = value_source

    def remove(self, mutative: Mutative, index: int):
        n_setters = len(self.setters)
        if index < 0:
            index = n_setters + index
        for i in range(index + 1, n_setters):
            self.setters[i - 1].value_source = self.setters[i].value_source
        self.setter_values.pop(index)
        self.setters.pop()
        return mutative.mutable_members.pop(self.mutative_id(n_setters - 1))

    def set_value(self, index: int, value_source: ValueSource):
        self.setters[index].value_source.set_type(value_source.type)
        self.setters[index].value_source.set_value(value_source.value)

    def move_setter_data(self, old_index: int, new_index: int):
        self.setters[new_index].value_source = self.setters[old_index].value_source
