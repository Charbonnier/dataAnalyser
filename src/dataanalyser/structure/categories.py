from potable.saveable import Saveable
from dataanalyser.structure.database import FilterStructure, Request
from dataanalyser.structure.mutative import ValueSource


class CategoriesFilter(Saveable):
    def __init__(self):
        super().__init__()
        self.enabled = False
        "True to enable the categories"
        self.field = None
        """field of the database containing the categories
        :type: str|None"""
        self._tmp_categories = {}
        """temporary stores each category with its corresponding sort field
        :type: dict[Any, Any]"""
        self._need_category_update = False
        """True if a new row as been inserted and categories list needs to be regenerated"""
        self.index = None
        """index of the category selected in the _categories_found list
        :type: int|None"""
        self.categories = []
        """the different values of found categories
        :type: list[any]"""
        self.sort_enabled = False
        self.sort_field = None
        """Field to use to sort the categories in a specific order. If several different sort values are found for the
        same category value, sorting is inconsistent.
        :type: str|None"""

    @classmethod
    def class_path(cls):
        return 'CategoriesFilter'

    def is_category_enabled(self):
        return self.enabled and self.field

    def get_required_fields(self):
        """Return the fields required to build the categories list."""
        fields = set()
        if not self.is_category_enabled():
            return fields
        fields.add(self.field)
        if self.sort_enabled and self.sort_field:
            fields.add(self.sort_field)
        return fields

    def query_categories(self, database):
        """

        :param src.structure.database.DataBase database:
        :return:
        """
        self.clear()
        if not self.is_category_enabled():
            return
        request = Request(self.get_required_fields())
        cursor = database.query(request)
        for row in cursor:
            self.categorize_row(row)

    def clear(self):
        self.categories = []
        self._tmp_categories = {}

    def categorize_row(self, row):
        if not self.is_category_enabled():
            return
        category = row[self.field]
        if self.sort_enabled and self.sort_field:
            sort_value = row[self.sort_field]
        else:
            sort_value = None
        old_len = len(self._tmp_categories)
        self._tmp_categories[category] = sort_value
        self._need_category_update = self._need_category_update or old_len != len(self._tmp_categories)

    def update_categories(self):
        if self.sort_enabled and self.sort_field:
            self.categories = sorted(self._tmp_categories.items(), key=lambda t: t[1])
        else:
            self.categories = sorted(self._tmp_categories.keys())
        self._need_category_update = False

    def get_categories(self):
        """
        :rtype: list[Any]
        :return:
        """
        if self._need_category_update:
            self.update_categories()
        return self.categories

    def get_filter(self, category_index=None):
        """

        :param int|None category_index:
        :rtype FilterStructure
        :return:
        """
        if not self.is_category_enabled() or category_index is None:
            return None
        return FilterStructure('AND', '=',
                               args=[ValueSource(ValueSource.REFERENCE, self.field),
                                     ValueSource(ValueSource.CONSTANT, self.get_categories()[category_index])])

CategoriesFilter.register_class()
