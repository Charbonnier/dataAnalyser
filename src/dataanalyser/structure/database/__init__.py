from abc import abstractmethod
from collections import OrderedDict

from dataanalyser.structure.database.dbfilter import FilterStructure


class InternalField(tuple):
    def __new__(cls, *args):
        n_args = ['internal']
        n_args.extend(args)
        return tuple.__new__(cls, n_args)

    @property
    def name(self):
        return self[1]


class RowIdField(InternalField):
    def __new__(cls, table_name):
        return super().__new__(cls, 'row_id', table_name)


FIELD_ROW_ID = RowIdField(0)


class Request:
    def __init__(self, selection=None, filter_structure=None):
        """
        :param list[Any] selection: Fields to select.
        :param src.structure.database.dbfilter.FilterStructure filter_structure: filter to use in the request
        """
        if selection is None:
            selection = []
        self.selection = set(selection)

        self.filter = filter_structure

    @property
    def full_selection(self):
        if not self.filter:
            return self.selection
        full_selection = set()
        full_selection.update(self.selection)
        for fs, pos in FilterStructure.walk([self.filter]):
            if fs.is_substructure:
                continue
            for arg in fs.args:
                if arg.type == arg.REFERENCE:
                    full_selection.add(arg.value)
        return full_selection


class ParcelTable:
    def __init__(self, table=None):
        """
        :param dict[Any, list[Any]] table: One entry for each field associated with the list of values.
        All lists must have the same size
        """
        if table is None:
            table = {}
            length = 0
        else:
            length = None
            for values in table.values():
                if length != len(values):
                    if length is None:
                        length = len(values)
                    else:
                        raise ValueError('Lists must have the same size')
            if length is None:
                length = 0
        self._length = length
        self.table = table

    @property
    def fields(self):
        return self.table.keys()

    @property
    def values(self):
        return self.table.values()

    def __len__(self):
        return self._length


class Link:
    def __init__(self, origin_table, target_table, origin_field):
        """
        :param int origin_table: Index of the table that points to the target table.
        :param int target_table: Index of the target table pointed by the origin table.
        :param Any origin_field: For each entry of the origin table, contains the index of the corresponding entry
            in the target table.
        """
        self.originTable = origin_table
        self.targetTable = target_table
        self.originField = origin_field


class LinkUpdate:
    def __init__(self, origin_field, target_field, one_to_one=False):
        """ Associates an origin table in the parcel, to a target table in the database.
        :param Any origin_field: Contains the index of entries in the target table.
        :param Any target_field: Used to identify the target table
        :param bool one_to_one: if true, a one to one relationship is assumed, ignored if the field has already been
            created.
        """
        self.targetField = target_field
        self.originField = origin_field
        self.one_to_one = one_to_one


class Parcel:
    def __init__(self, tables=None, links=None):
        """ Bunch of data to add in the database.
        :param list[ParcelTable] tables: Tables to store in the parcel
        :param list[Link] links: links between tables
        """
        if tables is None:
            tables = []
        if links is None:
            links = []
        self.tables = tables
        ":type: list[ParcelTable]"
        self.links = links
        ":type: list[Link]"

    def get_field_table(self, field):
        """ Gets the table containing the field.
        :param Any field: the field to retrieve
        :rtype: int|None
        :return the index of the table containing the field."""
        for i, table in enumerate(self.tables):
            try:
                table.table[field]
            except KeyError:
                pass
            else:
                ans = i
                break
        else:
            ans = None
        return ans


class Cursor:
    def __init__(self, reference_field):
        self.reference_field = reference_field
        self.fields_indexes = OrderedDict()
        ":type: OrderedDict[str|InternalField, int]"
        self.values = []
        ":type: tuple(Any)"

    @property
    def fields(self):
        return self.fields_indexes.keys()

    def is_empty(self):
        """
        :rtype: bool
        :return: True if the Cursor contains no data but has fields.
        """
        return len(self.values) == 0 and len(self.fields_indexes) != 0

    def __iter__(self):
        """
        :rtype: CursorRow
        :return:
        """
        for row in self.values:
            yield CursorRow(self.fields_indexes, row)

    def __len__(self):
        return len(self.values)


class CursorRow:
    def __init__(self, fields_indexes, values):
        """
        :param dict[str|InternalField, int] fields_indexes:
        :param list[Any] values: """
        self._fields_indexes = fields_indexes
        self._values = values

    def __getitem__(self, item):
        return self._values[self._fields_indexes[item]]

    def get_values(self):
        return self._values


class Transaction:
    def __init__(self, identifier):
        self.identifier = identifier

    def __eq__(self, other):
        return self.identifier == other.identifier

    def __hash__(self):
        return hash(self.identifier)


class DataBase:
    Empty = None

    def __init__(self):
        pass

    @abstractmethod
    def __getitem__(self, item):
        """ Returns the values associated with the given field"""
        pass

    @abstractmethod
    def get_field_table(self, field):
        """ Get the identifier of the table containing the given field.
        :param Any field: field to search
        :rtype: int
        :return The identifier of the table."""
        pass

    @abstractmethod
    def query(self, request):
        """
        :param Request request:
        :rtype: Cursor
        :return: The result of the request as a generator with a field in the source table as table identifier,
                 index of the row in the source table and the values of each selected
                 field in a list.
        """
        raise NotImplementedError()

    @abstractmethod
    def start_update(self, parcel, links):
        """Starts an update transaction with the database.
        :param Parcel parcel: a parcel with the same structure as used for the whole transaction.
        :param list[LinkUpdate] links: the links used for the whole transaction, only targetTableRows are allowed to
            change.
        :rtype: Transaction
        :return: The transaction to specify to the update and end_update methods.
        """
        raise NotImplementedError()

    @abstractmethod
    def end_update(self, transaction):
        """Ends an update transaction.
        :param Transaction transaction: The transaction to end.
        """
        raise NotImplementedError()

    @abstractmethod
    def update(self, transaction, parcel):
        """ Adds the parcel to the database. If link is provided, the parcel will be associated to the given table.
        :param Transaction transaction: The transaction to use for this update.
        :param Parcel parcel: Parcel to add in the database
        """
        raise NotImplementedError()

    @abstractmethod
    def clear(self):
        """ Clears the database. All data, rows and tables should be cleared."""
        raise NotImplemented()

    def single_update(self, parcel, links):
        """ Useful to update the database with a parcel used only once. If the same parcel is to be ased more than once,
        splitting start, for update loop and end will be more efficient.
        :param Parcel parcel: a parcel with the same structure as used for the whole transaction.
        :param list[LinkUpdate] links: the links used for the whole transaction, only targetTableRows are allowed to
            change."""
        t = self.start_update(parcel, links)
        self.update(t, parcel)
        self.end_update(t)

    def p_find_destination_table(self, table):
        """Searches the destination table for a parcel table in the database based on the fields contained in the parcel
        table. Creates a new table if no destination was found.
        :param ParcelTable table: The parcel table for which destination is searched.
        :rtype: int
        :return: index of the destination table
        :raises ValueError: fields in the parcel table are spread in several tables in the database."""
        # For each field in the table, tries to find the destination table in the database.
        index = None
        for field in table.fields:
            try:
                fieldindex = self.get_field_table(field)
                if index == fieldindex:
                    continue
                elif index is None:
                    index = fieldindex
                else:
                    raise ValueError('Fields "{0}" should be in the same destination table'.format(
                        ','.join(table.fields)
                    ))
            except KeyError:
                pass

        return index

    def p_associate_tables_to_database(self, parcel, links):
        """ Finds the destination tables for tables in a parcel based on the fields.
        If a field of a table is found in the database, the containing table will be used as destination.
        If no field of a table can be found in the database, a new table will be created.
        :param Parcel parcel: the parcel to handle.
        :param list[LinkUpdate] links: the link of tables
        :rtype: list[int]
        :return: The index of the destination table for each table in the parcel."""

        positions = {link.originField: link for link in links if link.one_to_one}
        """:type: dict[Any, LinkUpdate]"""

        table_indexes = []
        for table in parcel.tables:
            # Finds the destination table in the provided one to one link
            pos_fields = positions.keys() & table.fields
            if pos_fields:
                link = positions[next(iter(pos_fields))]
                target_field = link.targetField
                index = self.get_field_table(target_field)

            # If no one to one link were provided for the table
            else:
                index = self.p_find_destination_table(table)

            table_indexes.append(index)
        return table_indexes

    def p_find_tables_for_update_link(self, parcel_tables_destination, parcel, link):
        """
        :param list[int] parcel_tables_destination:
        :param Parcel parcel:
        :param LinkUpdate link:
        :rtype: (int, int)
        :return: The indexes of origin and target table in the database.
        """
        origin_table = parcel_tables_destination[parcel.get_field_table(link.originField)]
        target_table = self.get_field_table(link.targetField)
        return origin_table, target_table
