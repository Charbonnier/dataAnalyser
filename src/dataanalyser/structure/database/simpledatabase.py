from dataanalyser.structure.database.__init__ import Transaction, ParcelTable, DataBase, Link, Cursor, RowIdField, CursorRow
from dataanalyser.structure.database.dbfilter import FilterStructure
from dataanalyser.structure.database.dbtablesnet import TableNet
import numpy as np


class PyUpdateTransaction(Transaction):
    def __init__(self, identifier):
        super().__init__(identifier)
        self.tablesDestinations = []
        """ Stores the index of tables destination of parcel tables
        :type: list[int]"""

        self.positionLink = {}
        """ Stores the field of one to one links.
        :type: dict[int, Any]"""

        self.parcelLinks = []
        """ Stores the origin table, the target table ond the origin field of the link.
        :type: list[Link]"""

        self.tables = {}
        """Caches the data to add in the database when the transaction ends.
        - Key: index of the destination table
        - Values:
            - Table containing the new data
            - Size of the destination table after add
            - Positions of entries in the destination table
        :type: dict[int, (PyTable, list[int], list[int])]
        """

    def update(self, parcel):
        """Updates the content of the transaction with the parcel.
        :param Parcel parcel: The parcel with the content to add."""
        destination_rows = {}
        ":type: dict[int, (list[int], list[int])]"

        # Updates the transaction table with the parcel content
        for i, table in zip(self.tablesDestinations, parcel.tables):
            old_size = self.tables[i][1][0]
            try:
                position_field = self.positionLink[i]
                entries_indexes = table.table.pop(position_field)
                new_size = max(old_size, max(entries_indexes))
            except KeyError:
                new_size = old_size + len(table)
                entries_indexes = range(old_size, new_size)
            nentries = len(self.tables[i][2])
            destination_rows[i] = range(nentries, nentries + len(entries_indexes)), entries_indexes
            self.tables[i][1][0] = new_size
            self.tables[i][0].update(table)
            self.tables[i][2].extend(entries_indexes)

        # Processes the parcel links
        for link in self.parcelLinks:
            origincolumn = self.tables[link.originTable][0].table[link.originField]
            targetpositions = destination_rows[link.targetTable][1]
            for i in destination_rows[link.originTable][0]:
                pindex = origincolumn[i]
                origincolumn[i] = targetpositions[pindex]


class PyTable(ParcelTable):
    def __init__(self, table_id='', table=None):
        super().__init__(table)
        self.last_row_id = 0
        self.table_id = table_id
        self.row_id_field = RowIdField(table_id)
        self._add_fields({self.row_id_field})
        if len(self.table) == 0:
            self._length = 0
        else:
            pass
        self._fillholes()

    def _fillholes(self):
        row_ids = self.table[self.row_id_field]
        for i in range(len(row_ids), self._length):
            row_ids.append(i)
            self.last_row_id = i
        for v in self.table.values():
            for i in range(len(v), self._length):
                v.append(DataBase.Empty)

    def update(self, parceltable, indexes=None):
        """ Updates the table content with the input parcel table.
        :param ParcelTable parceltable: Data to add.
        :param list[int] indexes: position at which each parcel entry must be putted. If not provided, entries are added
        at the end of the table.
        """
        # Elements will be append to the end of the table
        if not indexes:
            self._add_fields(parceltable.fields)
            self._length += len(parceltable)
            for pfield, pentries in parceltable.table.items():
                self.table[pfield].extend(pentries)
            self._fillholes()
        # Puts the new values at the desired locations.
        else:
            maxindex = max(indexes)

            # Resizes the table if required.
            if maxindex >= self._length:
                self._length = maxindex + 1
                self._fillholes()

            self._add_fields(parceltable.fields)
            # Sets the new values.
            for pfield, pentries in parceltable.table.items():
                col = self.table[pfield]
                for i, index in enumerate(indexes):
                    try:
                        entry = pentries[i]
                    except IndexError:
                        break
                    else:
                        col[index] = entry

    def _add_fields(self, fields):
        for field in fields - self.fields:
            self.table[field] = [DataBase.Empty for _ in range(self._length)]


class PyCursor(Cursor):
    """ Query result"""
    def __init__(self, reference_field, tables, tables_to_field, tablepath, filter_structure=None):
        """
        :param reference_field:
        :param list[PyTable] tables:
        :param dict[int, list[(Any, int)]] tables_to_field:
        :param src.structure.database.dbtablesnet.TablePath tablepath:
        :param FilterStructure|None filter_structure:
        """
        super().__init__(reference_field)
        self.tables = tables
        self.tables_to_field = tables_to_field
        self.tablepath = tablepath
        self.compiled_filter = CompiledFilter(filter_structure)

        if self.tables is None:
            return

        for origin, origin_field, target in self.tablepath:
            fields = self.tables_to_field[target]
            for field, position in fields:
                self.fields_indexes[field] = position

    def __len__(self):
        if self.tables is None:
            return 0
        roots = self.tablepath.roots
        n = 1
        for table_index in roots:
            n *= len(self.tables[table_index])
        return n

    def __iter__(self):
        if self.tables is None:
            yield CursorRow({}, [])
            return
        cursor_root = next(iter(self.tablepath.roots))
        origin_table = self.tables[cursor_root]
        table_indexes = {}
        nFields = 0
        for fields in self.tables_to_field.values():
            nFields += len(fields)
        cursor_values = [None] * nFields

        for row_index in range(len(origin_table)):
            table_indexes[cursor_root] = row_index
            for origin, origin_field, target in self.tablepath:
                if origin is not None:
                    origin_index = table_indexes[origin]
                    table_indexes[target] = self.tables[origin].table[origin_field][origin_index]
                try:
                    fields = self.tables_to_field[target]
                except KeyError:
                    continue
                table = self.tables[target]
                for field, position in fields:
                    cursor_values[position] = table.table[field][table_indexes[target]]

            row = CursorRow(self.fields_indexes, cursor_values)
            if self.compiled_filter.evaluate(row):
                yield row


class CompiledFilter:
    def __init__(self, filter_structure):
        """

        :param FilterStructure|None filter_structure:
        """
        self._fct_index = 0
        self._filter_references_to_compiled_args = {}
        "Translates filter references to their names in expr_locals"
        self._arg_index = 0
        self.expression = ''
        self.expr_locals = {}
        if not filter_structure:
            self.evaluate = self.no_filter
        else:
            self.compile_filter(filter_structure)
            self.evaluate = self.evaluate_expr

    def compile_filter(self, fs):
        self.expr_locals = {}
        self._filter_references_to_compiled_args = {}
        self._fct_index = 0
        self._arg_index = 0
        self.expression = FilterStructure.write_expression([fs], self.make_function, self.make_link, invert='not')

    def make_link(self, link):
        return link.lower

    def make_function(self, filter):
        """

        :param FilterStructure filter:
        :return:
        """
        fct_name = 'fct_{}'.format(self._fct_index)
        self._fct_index += 1
        self.expr_locals[fct_name] = filter_functions[filter.function_name]
        args = []
        for arg in filter.args:
            arg_name = 'args_{}'.format(self._arg_index)
            args.append(arg_name)
            self._arg_index += 1
            if arg.type == arg.CONSTANT:
                self.expr_locals[arg_name] = arg.value
            else:
                self.expr_locals[arg_name] = None
                self._filter_references_to_compiled_args[arg.value] = arg_name
        return '{}({})'.format(fct_name, ', '.join(args))

    def evaluate(self, row):
        pass

    def evaluate_expr(self, row):
        for ref_name, arg_name in self._filter_references_to_compiled_args.items():
            self.expr_locals[arg_name] = row[ref_name]
        return eval(self.expression, None, self.expr_locals)

    def no_filter(self, row):
        return True


def filter_is_true(v):
    if v:
        return True
    return False


def filter_is_false(v):
    return not filter_is_true(v)


filter_functions = {'<': np.less,
                    '>': np.greater,
                    '=': np.equal,
                    '<=': np.less_equal,
                    '>=': np.greater_equal,
                    '!=': np.not_equal,
                    'is True': filter_is_true,
                    'is False': filter_is_false}


class PyDB(DataBase):
    LINK_FIELD = 0

    def __init__(self, tableclass=PyTable):
        super().__init__()
        self._tableClass = tableclass
        self._tables = []
        """ List of tables.
        :type: list[PyTable]"""
        self._fieldsTable = dict()
        """ Get the table containing the field.
        :type: dict[Any, int]"""
        self._tableNet = TableNet()

    def __getitem__(self, item):
        return self._tables[self.get_field_table(item)].table[item]

    @staticmethod
    def _make_link_field(target, origin):
        """ Builds the field for a link to be stored in the origin table.
        :param int target: Index of the target table
        :param int origin: Index of the origin table
        :return: corresponding field of the link
        """
        return PyDB.LINK_FIELD, target, origin

    def get_field_table(self, field):
        return self._fieldsTable[field]

    def query(self, request):
        """
        :param src.structure.database.Request request:
        :rtype: Cursor
        :return: The result of the request as a generator with a field in the source table as table identifier,
                 index of the row in the source table and the values of each selected
                 field in a list.
        """
        if not request.selection:
            return PyCursor(None, None, None, None)
        table_to_field = {}
        # TODO: In some valid cases, fields requested by filters can add unnecessary rows.
        # This occurs if a field requested by the filter is connected n->1 to the rest of the selection.
        # In this case the field should be ignored but what to do with the filter evaluation?
        for i, field in enumerate(request.full_selection):
            table = self.get_field_table(field)
            try:
                table_to_field[table].append((field, i))
            except KeyError:
                table_to_field[table] = [(field, i)]
        tables = table_to_field.keys()
        path = self._tableNet.get_node_relation(tables)
        if len(path.roots) != 1:
            ":todo: Implement multiple roots queries support"
            raise NotImplementedError('TODO: Multiple roots queries are not supported')
        root_table = next(iter(path.roots))
        root_field = self._tables[root_table].row_id_field
        table_to_field[root_table].append((root_field, len(request.full_selection)))
        return PyCursor(root_field, self._tables, table_to_field, path, request.filter)

    def _create_table(self):
        """ Creates a new table in the database to contain the given table.
        :rtype: int
        :return: The index of the new table"""
        table_id = len(self._tables)
        self._tables.append(PyTable(table_id))
        self._tableNet.add_node(table_id)
        return table_id

    def p_find_destination_table(self, table):
        index = super().p_find_destination_table(table)

        # If no field were found, create a new table in the database to store the fields.
        if index is None:
            index = self._create_table()

        return index

    def _make_link(self, link):
        """Link tables together
        :param Link link: the link to create."""
        self._tableNet.add_link(link.originTable, link.originField, link.targetTable)

    def start_update(self, parcel, links):
        """Starts an update transaction with the database.
        :param Parcel parcel: a parcel with the same structure as used for the whole transaction.
        :param list[LinkUpdate] links: the links used for the whole transaction, only targetTableRows are allowed to
            change.
        :rtype: Transaction
        :return: The transaction to specify to the update and end_update methods.
        """
        transaction = PyUpdateTransaction(0)
        transaction.tablesDestinations = self.p_associate_tables_to_database(parcel, links)

        # Creates the tables
        for i in transaction.tablesDestinations:
            try:
                dbtable = self._tables[i]
            except IndexError:
                size = 0
            else:
                size = len(dbtable)
            transaction.tables[i] = PyTable(), [size], []

        # Handles the links provided for update
        for link in links:
            origin_table, target_table = self.p_find_tables_for_update_link(transaction.tablesDestinations,
                                                                            parcel,
                                                                            link)
            if not link.one_to_one:
                self._make_link(Link(origin_table, target_table, link.originField))
            else:
                transaction.positionLink[origin_table] = link.originField

        # Handles the links provided in the parcel
        for link in parcel.links:
            plink = Link(transaction.tablesDestinations[link.originTable],
                         transaction.tablesDestinations[link.targetTable],
                         link.originField)
            self._make_link(plink)
            transaction.parcelLinks.append(plink)

        return transaction

    def end_update(self, transaction):
        """
        :param PyUpdateTransaction transaction:
        :return:
        """
        for tindex, (table, size, entriesindexes) in transaction.tables.items():
            self._tables[tindex].update(table, entriesindexes)
        self._fieldsTable = {}

        for i, table in enumerate(self._tables):
            self._fieldsTable.update({field: i for field in table.fields})

    def update(self, transaction, parcel):
        """ Adds the parcel to the database. If link is provided, the parcel will be associated to the given table.
        :param PyUpdateTransaction transaction: The transaction to use for this update.
        :param Parcel parcel: Parcel to add in the database
        """
        transaction.update(parcel)

    def clear(self):
        self._tables.clear()
        self._fieldsTable.clear()
        self._tableNet.clear()
