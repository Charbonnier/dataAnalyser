from potable.saveable import State, Saveable
from dataanalyser.structure.mutative import ValueSource


class FilterFunctionDescription:
    def __init__(self, n_operands):
        self.n_operands = n_operands


class FilterStructure(Saveable):
    """
        This class describes the filters to apply to a selection of a request performed on a database.
        It does not the filtering by itself, the filter function are implemented by the database itself. Thus,
        depending on the implementation it can lead to an SQL clause or pure python.
    """
    def __init__(self, link='', function_name='<', invert=False, sub_structure=None, args=None):
        """

        :param str link: Link with other elements of the filter clause, must be AND_LINK or OR_LINK.
        At implementation, the link of the first element of a filter clause is always ignored.

        :param str function_name: identifier of the function to use, must be in filter_functions

        :param bool invert: whether or not the result of the clause must be inverted

        :param list[FilterStructure] sub_structure: filter sub structure, if provided the the containing FilterStructure
        acts as parenthesis. In that case, the function should be ignored when implementing the filter.

        :param list[ValueSource] args: arguments to pass to the filter function.
        """
        super().__init__()
        self.link = link
        if args is None:
            args = []
        self.args = args
        self.function_name = function_name
        self.set_function(function_name)
        self.invert = invert
        if sub_structure is None:
            sub_structure = []
        self.sub_structure = sub_structure
        ":type: list[FilterStructure]"

    @classmethod
    def class_path(cls):
        return 'FilterStructure'

    def get_state(self) -> State:
        state = super().get_state()
        state.update_parameters({
            'link': self.link,
            'function_name': self.function_name,
            'invert': self.invert,
            'sub_structure': self.sub_structure,
            'args': self.args
        })
        return state

    def set_state(self, state: State):
        super().set_state(state)
        params = state.get_parameters()
        self.link = params['link']
        fct_name = params['function_name']
        self.set_function(fct_name)
        self.invert = params['invert']
        self.sub_structure = params['sub_structure']
        self.args = params['args']

    def set_function(self, function_name):
        self.function_name = function_name
        n_args = filter_functions[function_name].n_operands
        for i in range(n_args):
            if i < len(self.args):
                continue
            self.args.append(ValueSource(ValueSource.CONSTANT, 0))
        if n_args < len(self.args):
            self.args = self.args[:n_args]

    @property
    def is_substructure(self):
        if self.sub_structure:
            return True
        return False

    @staticmethod
    def walk(fs_list):
        """

        :param list[FilterStructure] fs_list:
        :rtype: list[(FilterStructure, int)]
        :return:
        """

        queue = [(fs_list, 0)]
        while queue:
            fs_list, pos = queue.pop()
            n_fs = len(fs_list)
            while pos < n_fs:
                fs = fs_list[pos]
                if fs.sub_structure:
                    queue.append((fs_list, pos + 1))
                    queue.append((fs.sub_structure, 0))
                    yield fs, pos
                    pos = 0
                    break
                else:
                    yield fs, pos
                    pos += 1
            # If queue is empty the filter has been walked entirely.
            # Otherwise, only a sub_structure has been completed.
            if queue and pos != 0:
                # None indicates the end of a substructure
                yield None, pos

    @staticmethod
    def write_expression(fs_list, fct_translator, link_translator, start_clause='(', end_clause=')', invert='NOT'):
        expression_lst = []
        for fs, pos in FilterStructure.walk(fs_list):
            if fs is None:
                expression_lst.append(end_clause)
            else:
                if fs.is_substructure:
                    fs_str = start_clause
                else:
                    fs_str = fct_translator(fs)

                if fs.invert:
                    fs_str = '{} {}'.format(invert, fs_str)

                if pos == 0:
                    expression_lst.append(fs_str)
                else:
                    expression_lst.append(' {} {}'.format(link_translator(fs.link), fs_str))
        return ''.join(expression_lst)


AND_LINK = 'AND'
OR_LINK = 'OR'

filter_links = {AND_LINK, OR_LINK}
filter_functions = {'<': FilterFunctionDescription(2),
                    '>': FilterFunctionDescription(2),
                    '=': FilterFunctionDescription(2),
                    '<=': FilterFunctionDescription(2),
                    '>=': FilterFunctionDescription(2),
                    '!=': FilterFunctionDescription(2),
                    'is True': FilterFunctionDescription(1),
                    'is False': FilterFunctionDescription(1)}

FilterStructure.register_class()
