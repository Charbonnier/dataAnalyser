from collections import deque


class TableNodeNet:
    __slots__ = ['origins', 'targets']

    def __init__(self, origins=None, targets=None):
        """ Stores the connections of a table.
        :param set[int] origins: identifier of tables pointing to this table
        :param dict[int, Any] targets: identifier targets of the table and field containing the target indexes."""
        if origins is None:
            origins = set()
        if targets is None:
            targets = {}
        self.origins = origins
        self.targets = targets


class TablePath:
    def __init__(self, roots, links):
        """
        :param set[int] roots:
        :param dict[int, set[tuple[Any, int]]] links:
        """
        self.roots = roots
        self.links = links
        self.origins = {}
        ":type: dict[int, set[int]]"
        for root in self.roots - self.links.keys():
            self.links[root] = set()

        for table, link_list in links.items():
            self._update_origins(link_list, table)

    @property
    def tables(self):
        tables = set()
        tables.update(self.origins.keys())
        tables.update(self.roots)
        return tables

    def _update_origins(self, link_list, table):
        for link in link_list:
            self._add_origin(table, link)

    def _add_origin(self, table, link):
        try:
            self.origins[link[1]].add(table)
        except KeyError:
            self.origins[link[1]] = {table, }

    def __eq__(self, other):
        return self.roots == other.roots and self.links == other.links

    def __iadd__(self, other):
        # Adds roots from other if they are not in self.origins
        non_root_tables = self.origins.keys()
        for root in other.roots:
            if root in non_root_tables:
                continue
            else:
                self.roots.add(root)

        for t, l in other.links.items():
            try:
                self.links[t].update(l)
            except KeyError:
                self.links[t] = l
            self._update_origins(l, t)
        return self

    def __repr__(self):
        return 'TablePath({0}, {1})'.format(repr(self.roots), repr(self.links))

    def __iter__(self):
        paths = [(None, None, o) for o in self.roots]
        while paths:
            path = paths.pop()
            try:
                links = self.links[path[2]]
            except KeyError:
                pass
            else:
                for link in links:
                    paths.append((path[2], link[0], link[1]))
            yield path

    def add_root(self, table):
        """ Adds a root to the TablePath
        :param int table: The table to add.
        """
        if table in self.roots:
            return
        self.roots.add(table)
        if table not in self.links.keys():
            self.links[table] = set()

    def add_link(self, origin, link):
        """ Adds a link to the TablePath
        :param int origin: origin table
        :param (Any, int) link: field and target table
        """
        try:
            self.links[origin].add(link)
        except KeyError:
            self.links[origin] = {link, }

        self._add_origin(origin, link)

    def remove_link(self, origin, link):
        """ Removes a link from the TablePath
        :param int origin: origin table
        :param (Any, int) link: field and target table
        """
        self.links[origin].remove(link)
        origins = self.origins[link[1]]
        origins.remove(origin)
        if len(origins) == 0:
            del self.origins[link[1]]

    def pop(self, table):
        """ Removes a table from the TablePath.
        :param int table: table to remove
        :rtype: set[tuple[Any, int]]
        :return: Links starting from the removed table.
        """
        try:
            self.roots.remove(table)
        except KeyError:
            links_to_remove = set()
            # Removes the links pointing to the removed table.
            for origin in self.origins[table]:
                for l in self.links[origin]:
                    if l[1] == table:
                        break
                else:
                    continue
                links_to_remove.add((origin, l))

            for o, l in links_to_remove:
                self.remove_link(o, l)

        # Removes the links of this root.
        links = self.links.pop(table)
        for l in links:
            self.origins[l[1]].remove(table)
        return links

    def _get_multiple_origins(self):
        """ Returns the list of tables with more than on origin.
        :rtype dict[int, [[int]]]
        :return: tables with more than one origin and start of their path.
        """
        ans = {}
        for table, origins in self.origins.items():
            if len(origins) > 1:
                ans[table] = [[o] for o in origins]
        return ans

    def _break_paths(self, table, paths):
        """ Breaks the longest paths by removing the links pointing to table.
        :param int table: the destination table
        :param list[list[int]] paths: the different paths pointing to that table
        :rtype set(int)
        :return: The tables for which a link was removed"""
        path_to_keep = None
        modified_tables = set()
        for path in sorted(paths, key=lambda p: len(p)):
            if path_to_keep is None:
                path_to_keep = path
                continue
            origin = path[0]
            for link in self.links[origin]:
                if link[1] == table:
                    break
            else:
                raise ValueError("Input path says that {0} points to {1} but no link where found in TablePath."
                                 .format(origin, table))
            self.remove_link(origin, link)
            modified_tables.add(origin)
        return modified_tables

    def break_parallel_paths(self, to_keep):
        """ Removes undesired parallel paths as well as multiple paths to the same tables.
        :param set[int] to_keep: The list of tables to keep
        :rtype: set[int]
        :return: The new ends of the path, these tables might not be necessary anymore"""
        multi_source_tables = self._get_multiple_origins()

        pending_tables = deque([(table, len(origins)) for table, origins in multi_source_tables.items()])
        new_path_ends = set()
        while pending_tables:
            table, remaining = pending_tables.pop()
            path = multi_source_tables[table][remaining - 1]
            root_reached = self._path_down(path)
            if root_reached:
                remaining -= 1
                if remaining == 0:
                    # All parallel path to this table have been found, time to break undesired ones
                    modified_tables = {t for t in self._break_paths(table, multi_source_tables[table])
                                       if len(self.links[t]) == 0}
                    self.clean_leaves(modified_tables, to_keep)
                else:
                    # Other parallel path to this table are still pending
                    pending_tables.append((table, remaining))
            else:
                # A branch with parallel path has been reached, need to handle this new before continuing.
                pending_tables.appendleft((table, remaining))
        return new_path_ends

    def clean_leaves(self, leaves, to_keep):
        """
        :param set[int] leaves: The list of leaves to remove
        :param set[int] to_keep: The list of tables to keep
        """
        for table in leaves - to_keep:
            while True:
                try:
                    origin = next(iter(self.origins[table]))
                except KeyError:
                    break
                self.pop(table)
                if len(self.links[origin]) == 1:
                    if origin in to_keep:
                        break
                    table = origin
                else:
                    break

    def _path_down(self, path):
        """
        :param list[int] path: path to the root
        :rtype: bool
        :return: True if the end is reached
        """
        root_reached = False
        last_node = path[-1]
        while True:
            try:
                origins = self.origins[last_node]
            except KeyError:
                # Origin of the path is reached
                root_reached = True
                break
            else:
                if len(origins) > 1:
                    # A node with multiple origins has been reached
                    break
                else:
                    # Appends the origin to the path.
                    last_node = next(iter(origins))
                    path.append(last_node)
        return root_reached

    def get_highest(self, tables):
        """ Returns the farthest tables from the root.
        :param set[int] tables: the tables to look in"""
        not_highest = set()
        for table in tables:
            if table in not_highest:
                continue
            try:
                origins = {o for o in self.origins[table]}
            except KeyError:
                continue
            while origins:
                origin = origins.pop()
                if origin in tables:
                    not_highest.add(origin)
                try:
                    origins.update(self.origins[origin])
                except KeyError:
                    pass

        return tables - not_highest

    def clean_roots(self, roots=None):
        """ Cleans the paths by removing the undesired roots. Also removes unnecessary roots by keeping only roots
        that bind several branches of the path.
        :param TablePath self: table path.
        :param set[int] roots: Roots to keep, if None or empty, nothing is done.
        """
        if not roots:
            return

        to_remove = self.roots - roots
        while to_remove:
            root = to_remove.pop()
            links = self.links[root]

            self.pop(root)

            for _, table in links:
                origins = self.origins[table]
                # If the table is in root, stop handling this branch.
                if table in roots:
                    # If the table has no origin in roots, add the table to path_roots
                    if not origins & self.links.keys():
                        self.add_root(table)
                # If the table has no more origin, add it for removing.
                elif not origins:
                    to_remove.add(table)


class TableNet:
    """Describes the relationship between tables, especially target and origin tables and fields of a table.
    """
    def __init__(self, tables=None, links=None):
        """
        :param set[int] tables: identifier of tables to to add to the net.
        :param set[(int, Any, int)] links: links (origin, field in origin table, target) to add to the net.
        """
        self._nodes = {}
        """ For each key node stores the target and origin nodes
        :type: dict[int, TableNodeNet]"""

        if tables is None:
            tables = []
        if links is None:
            links = []

        for node in tables:
            self.add_node(node)

        for origin, field, target in links:
            self.add_link(origin, field, target)

    def clear(self):
        self._nodes.clear()

    def add_node(self, identifier):
        """ Adds a new node to the net.
        :param int identifier: the node to add."""
        if identifier not in self._nodes.keys():
            self._nodes[identifier] = TableNodeNet()

    def add_link(self, origin, field, target):
        """ Adds a link to the net.
        :param int origin: the origin node of the link, must be part of the tree
        :param Any field: the field of origin table pointing to the target
        :param int target: the target node of the link, must be part of the tree"""
        assert origin in self._nodes.keys(), 'Origin node must be in the net'
        assert target in self._nodes.keys(), 'Target node must be in the net'
        self._nodes[origin].targets[target] = field
        self._nodes[target].origins.add(origin)

    def get_node_relation(self, tables):
        """ Returns a path to link nodes together.
        :param set[int] tables: nodes to link together.
        :rtype: TablePath
        :return: The path making the relation between input tables
        """
        table_path = TablePath(set(), {})
        common_tables = []
        ":type: list[set[int]]"
        for i, table in enumerate(tables):
            n_table_path = self.get_paths_to_root(table)

            path_tables = n_table_path.tables
            for j, common_set in enumerate(common_tables):
                common = common_set & path_tables
                if common:
                    common_tables[j] = common
                    break
            else:
                common_tables.append(path_tables)

            table_path += n_table_path

        probable_roots = set()
        for ct in common_tables:
            probable_roots.update(table_path.get_highest(ct))

        keep_tables = set(tables)
        keep_tables.update((t for t in probable_roots))
        table_path.clean_roots(keep_tables)
        table_path.break_parallel_paths(tables)
        table_path.clean_roots(keep_tables)
        return table_path

    def get_paths_to_root(self, table, roots=None):
        """Returns the list of paths to a root (table with no origin) for the given table.
        :param int table: the table to use
        :param set[int] roots: Root table on which to stop, stops on root if None.
        :rtype: TablePath
        :return: Roots and the different links to the destination table."""
        pending_tables = {table}
        if roots is None:
            roots = set()

        table_path = TablePath(set(), {})

        while pending_tables:
            table = pending_tables.pop()
            node = self._nodes[table]
            origins = node.origins
            if not origins:
                table_path.add_root(table)
            else:
                for origin in origins:
                    # If origin is in roots, stops the branch analysis.
                    if origin in roots:
                        table_path.add_root(origin)
                    else:
                        pending_tables.add(origin)
                    link = self._nodes[origin].targets[table], table
                    table_path.add_link(origin, link)

        return table_path
