from enum import Enum


class NodeState(Enum):
    ok = 0  # dependencies are met
    missing = 1  # not in the dependency tree
    broken = 2  # at least one dependency is missing


class NodeNet:
    """Bidirectionnal storage of node's connections. Both dependency and dependent nodes are stored."""
    __slots__ = ['depends_on', 'required_for', 'state', 'missing']

    def __init__(self, state=None):
        self.depends_on = set()
        """Lists nodes on which it depends.
        :type: set[Any]"""
        self.required_for = set()
        """Lists nodes which require this node.
        :type: set[Any]"""
        self.missing = set()
        """Set of dependencies marked as missing
        :type: set[Any]"""
        if state is None:
            state = NodeState.ok
        self.state = state
        """Status of the node net.
        :type: NodeState"""


class DependencyTree:
    def __init__(self, dependencies=None):
        """
        :param dict[Any, set[Any]] dependencies: each  key is a node identifier associated to the set of dependencies
                                               identifiers.
        """
        self._dependencies = {}
        """ Each key is associated to the set of dependencies identifiers and the set of identifiers requiring this node
        in their dependencies.
        :type: dict[Any, NodeNet]"""
        if dependencies is not None:
            self._build_tree(dependencies)

    def __getitem__(self, node_id):
        """ Returns the dependencies of the given node identifier.
        :param Any node_id: The identifier of the node.
        :rtype: set[Any]
        :return: The dependencies of the input node identifier.
        """
        return self._dependencies[node_id].depends_on

    def __eq__(self, other):
        """ Compares two dependency trees.
        :param dict[Any, set[Any]] | DependencyTree other: the other tree to compare.
        :rtype: bool"""
        if isinstance(other, dict):
            for node, dependencies in other.items():
                try:
                    node_net = self._dependencies[node]
                except KeyError:
                    return False
                if node_net.depends_on != dependencies:
                    return False
            return True

        elif isinstance(other, DependencyTree):
            return self._dependencies == other._dependencies
        return False

    def __iter__(self):
        """ Iterates on all nodes in random order
        :rtype: Any"""
        pass

    def __str__(self):
        pass

    def __repr__(self):
        pass

    def _build_tree(self, dependencies):
        """
        :param dict[Any, set[Any]] dependencies: each key is a node identifier associated to the set of dependencies
        identifiers.
        """
        for node, depends_on_set in dependencies.items():
            self.add_node(node, depends_on_set)

    def _make_node_net(self, node, raise_if_exists=True):
        """Make a NodeNet for the given node and add it to the tree. If the NodeNet already exists return the existing
        one.
        If raise_if_exists is set, and the NodeNet already exists in the tree, raises ValueError otherwise.
        :param Any node: node to fetch.
        :param bool raise_if_exists: Raise if the NodeSet already exists in the tree and is not flagged as missing.
        :rtype: NodeNet
        :return: The NodeNet corresponding to node."""
        try:
            node_net = self._dependencies[node]
            if raise_if_exists and node_net.state != NodeState.missing:
                raise ValueError('"{0}" already exists.'.format(node))
        except KeyError:
            node_net = NodeNet(NodeState.missing)
            self._dependencies[node] = node_net
        return node_net

    def _set_missing(self, node, node_net, dependency):
        if dependency in node_net.depends_on:
            node_net.missing.add(dependency)
            node_net.state = NodeState.broken
        else:
            raise ValueError('{0} does not depends on {1}'.format(node, dependency))

    def _clear_missing(self, node_id, node_net, dependency):
        """ Set a missing dependency as present.
        :param Any node_id: id of the node that depends on dependency
        :param NodeNet node_net: the NodeNet of node
        :param dependency: the dependency that is not missing anymore.
        :rtype: NodeState
        :return: The new state of node
        """
        if node_net.state != NodeState.broken:
            raise ValueError('{0} does not miss dependencies'.format(node_id))

        try:
            node_net.missing.remove(dependency)
        except KeyError:
            pass

        if not node_net.missing:
            node_net.state = NodeState.ok
        return node_net.state

    def get_requires(self, node_id):
        """Returns the identifiers of the nodes that requires this node_id.
        :param Any node_id: The node id for which dependent node are searched.
        :raises KeyError: if node_id is not in the tree.
        :rtype: set[Any]
        :return: The set of identifiers dependending on this node."""
        return self._dependencies[node_id].required_for

    def add_node(self, new_node, dependency_ids=None):
        """ Adds the given node to the tree.
        :param Any new_node: the identifier of the node to add.
        :param set[Any] dependency_ids: the dependencies of the node to add.
        :raises KeyError: if the node id already exists in the tree.
        :rtype: dict[Any, NodeState]
        :return: Dictionary of nodes affected by the change and their new state."""
        affected = {}
        if dependency_ids is None:
            dependency_ids = set()

        node_net = self._make_node_net(new_node)
        node_net.state = NodeState.ok
        affected[new_node] = node_net.state

        # Updates nodes already in the tree that requires this node
        for required_for in node_net.required_for:
            required_for_net = self._dependencies[required_for]
            affected[required_for] = self._clear_missing(required_for, required_for_net, new_node)

        # Update dependencies of new_node
        for dependency in dependency_ids:
            affected.update(self._add_dependency(new_node, node_net, dependency))
        return affected

    def remove_node(self, node_id):
        """ Removes the node from the tree. Note that the node identifier might still be referenced as a dependency for
        other nodes.
        :param Any node_id: the identifier of the node to remove.
        :raises KeyError: if the node id does not exists in the tree.
        :rtype: dict[Any, NodeState]
        :return: Dictionary of nodes affected by the change and their new state."""
        node_net = self._dependencies[node_id]
        node_net.state = NodeState.missing
        affected = {node_id: node_net.state}
        for dependency_id in set(node_net.depends_on):
            affected.update(self._remove_dependency_net(node_id, node_net, dependency_id))

        for requires_this in node_net.required_for:
            requires_this_net = self._dependencies[requires_this]
            self._set_missing(requires_this, requires_this_net, node_id)
            affected[requires_this] = requires_this_net.state
        if self._check_and_destroy(node_id, node_net):
            affected.pop(node_id)
        return affected

    def rename_node(self, node_id, new_id):
        if new_id in self._dependencies:
            raise ValueError('Node "{}" already exists'.format(new_id))
        node_net = self._dependencies.pop(node_id)
        for dependent in node_net.required_for:
            dd = self._dependencies[dependent]
            dd.depends_on.remove(node_id)
            dd.depends_on.add(new_id)
        for dependency in node_net.depends_on:
            dd = self._dependencies[dependency]
            dd.required_for.remove(node_id)
            dd.required_for.add(new_id)
        self._dependencies[new_id] = node_net





    def add_dependency(self, node_id, dependency_id):
        """Adds a dependency to the given node.
        :raises KeyError: if node_id is not in the tree.
        :raises ValueError: If adding the dependency creates a dependency loop.
        :rtype: dict[Any, NodeState]
        :return: Dictionary of nodes affected by the change and their new state."""
        node_net = self._dependencies[node_id]
        return self._add_dependency(node_id, node_net, dependency_id)

    def _add_dependency(self, node_id, node_net, new_dependency_id):
        try:
            self[new_dependency_id]
        except KeyError:
            pass
        else:
            # Check that a dependency loop will not be created
            if self.check_dependency_loop(node_id, new_dependency_id):
                raise ValueError('Adding {0} as dependency of {1} would generate a dependency loop'.format(
                    new_dependency_id, node_id))

        node_net.depends_on.add(new_dependency_id)
        dependency_net = self._make_node_net(new_dependency_id, raise_if_exists=False)
        if dependency_net.state == NodeState.missing:
            self._set_missing(node_id, node_net, new_dependency_id)
        dependency_net.required_for.add(node_id)
        return {node_id: node_net.state, new_dependency_id: dependency_net.state}

    def check_dependency_loop(self, node_id, new_dependency_id):
        if node_id == new_dependency_id:
            return True
        for dependency_id in self.walk_dependencies(node_id):
            if dependency_id == new_dependency_id:
                return True
        else:
            return False

    def remove_dependency(self, node_id, dependency_id):
        """Removes a dependency from a given node.
        :param Any node_id: the node id from which the dependency will be removed.
        :param Any dependency_id: the dependency to remove.
        :raises KeyError: if node_id is not in the tree
        :raises ValueError: if node_id does not depend on dependency_id
        :rtype: dict[Any, NodeState]
        :return: Dictionary of nodes affected by the change and their new state."""
        node_net = self._dependencies[node_id]
        return self._remove_dependency_net(node_id, node_net, dependency_id)

    def _remove_dependency_net(self, node_id, node_net, dependency_id):
        try:
            node_net.depends_on.remove(dependency_id)
            try:
                self._clear_missing(node_id, node_net, dependency_id)
            except ValueError:
                pass
        except KeyError:
            raise ValueError('{0} does not depend on {1}'.format(node_id, dependency_id))

        dependency_net = self._dependencies[dependency_id]
        dependency_net.required_for.remove(node_id)
        if self._check_and_destroy(dependency_id, dependency_net):
            affected = {dependency_id: dependency_net.state}
        else:
            affected = {node_id: node_net.state, dependency_id: dependency_net.state}
        return affected

    def _check_and_destroy(self, node_id, node_net):
        """Destroy the node if it is missing and not required anymore
        :param Any node_id: the node to destroy
        :param NodeNet node_net: the node net
        :rtype: bool
        :return: True if the node was destroyed"""
        if node_net.state == NodeState.missing and not node_net.required_for:
            self._dependencies.pop(node_id)
            return True
        return False

    def items(self):
        """
        :rtype: tuple[(Any, set[Any])]
        :return: Generator giving for each node, its identifier and its dependencies.
        """
        for node_id, node_net in self._dependencies.items():
            yield node_id, node_net.depends_on

    def walk_dependencies(self, start_node):
        pending = {start_node, }
        while pending:
            node_id = pending.pop()
            node_net = self._dependencies[node_id]
            for requires_this in node_net.required_for:
                pending.add(requires_this)
            yield node_id

    def walk(self, start_node=None, handled_nodes=None):
        """ Iterates on the tree in the order of dependencies. A node is yield only if all its dependencies have been
        previously handled.
        :param Any start_node: The start node of the walk.
        :param set[Any] handled_nodes: The nodes already handled.
        :rtype: tuple[Any, set[Any], set[Any], NodeState]
        :return The node identifier, its dependencies, the nodes requiring this node and the node status flag.
        """
        if handled_nodes is None:
            handled_nodes = set()
        if start_node is None:
            pending = {node_id for node_id, node_net in self._dependencies.items() if not node_net.depends_on}
        else:
            pending = {start_node, }
        while pending:
            node_id = pending.pop()
            handled_nodes.add(node_id)
            node_net = self._dependencies[node_id]
            for requires_this in node_net.required_for:
                if not self._dependencies[requires_this].depends_on - handled_nodes:
                    pending.add(requires_this)
            yield node_id, node_net.depends_on, node_net.required_for, node_net.state
