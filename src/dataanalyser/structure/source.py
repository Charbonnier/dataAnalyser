import os
from abc import abstractmethod
from logging import Logger, WARNING
from typing import Set, Dict, List, Optional, Any, Iterable

from potable.saveable import State
from potable.observable import Observable

from dataanalyser.structure.database import LinkUpdate, FIELD_ROW_ID, Parcel, ParcelTable, FIELD_ROW_ID as row_id, Request, \
    Cursor, CursorRow, DataBase
from dataanalyser.structure.dependencytree import DependencyTree
from dataanalyser.structure.mutative import Mutative, ValueSource
from dataanalyser.utils import main_log_handler


class FieldNameError(Exception):
    pass


class DuplicateFieldName(Exception):
    pass


class EvaluationError(Exception):
    def __init__(self, exception, node, *args: object, **kwargs: object) -> None:
        super().__init__(*args, **kwargs)
        self.exception = exception
        self.node = node


class EvaluationStep:
    PERFORMING_QUERY = 'Querying data'
    EVALUATING = 'Calculating'
    UPDTATING_DB = 'Updating Database'

    def __init__(self) -> None:
        self.source = ''  # Source being evaluated
        self.step_name = ''  # Step of the process
        self.item_index = 0  # Index of the step
        self.n_items = 0  # Total steps


class SourceManager(Observable):
    EVENT_SOURCE_ADDED = 'source_added'          # Callable[(SourceManager, Source), None]
    EVENT_SOURCE_REMOVED = 'source_removed'      # Callable[(SourceManager, Source), None]
    EVENT_SOURCE_RENAMED = 'source_renamed'      # Callable[(SourceManager, Source, str), None]
    EVENT_FIELD_ADDED = 'field_added'            # Callable[(SourceManager, Source, str), None]
    EVENT_FIELD_REMOVED = 'field_removed'        # Callable[(SourceManager, Source, str, bool), None]
    EVENT_FIELD_RENAMED = 'field_renamed'        # Callable[(SourceManager, Source, str, str), None]
    EVENT_MISSING_ADDED = 'missing_added'        # Callable[(SourceManager, Source, str), None]
    EVENT_MISSING_REMOVED = 'missing_removed'    # Callable[(SourceManager, str), None]
    EVENT_DEP_ADDED = 'dependency_added'         # Callable[(SourceManager, Source, Source|None, str), None]
    EVENT_DEP_REMOVED = 'dependency_removed'     # Callable[(SourceManager, Source, str), None]

    def __init__(self, database: DataBase):
        super().__init__()
        self.sources: Dict[str, 'Source'] = {}
        """ Tracks registered sources."""
        self.fields_provider: Dict[str, 'Source'] = {}
        """ Fields names associated with their source"""
        self.fields_users: Dict[str, Set[str]] = {}
        """ Fields names associated to the sources that requires them        """
        self._missing_fields: Dict[Any, Set[str]] = {}
        """ Fields required by a source but provided by none"""
        self.database = database
        self.history = {}
        self.dependency_tree = DependencyTree()
        self.name_sep = '.'
        self.logger = Logger('SourceManager')
        self.logger.setLevel(WARNING)
        self.logger.addHandler(main_log_handler)

        self.context_source = ContextSource()
        """ The context source stores global constants such as pathes to data, configuration and application
        directories. It allows to reference these constants as inputs for other sources.
        """
        self.register_source(self.context_source)

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.update([SourceManager.EVENT_SOURCE_ADDED,
                       SourceManager.EVENT_SOURCE_REMOVED,
                       SourceManager.EVENT_SOURCE_RENAMED,
                       SourceManager.EVENT_FIELD_ADDED,
                       SourceManager.EVENT_FIELD_REMOVED,
                       SourceManager.EVENT_FIELD_RENAMED,
                       SourceManager.EVENT_MISSING_ADDED,
                       SourceManager.EVENT_MISSING_REMOVED])
        return events

    def load_sources(self, sources: Iterable['Source']):
        if len(self.sources) > 1:
            ValueError('SourceManager must be empty to load sources')

        for source in sources:
            if source.name == ContextSource.NAME:
                continue
            self.register_source(source)

    def register_source(self, source: 'Source'):
        """ Register a new source to the manager
        :param source:
        """
        if source.manager is not None:
            raise ValueError(f'{source.name} already has a manager')
        source.manager = self

        s_name = source.name
        i = 0
        while source.name in self.sources:
            source.name = f'{s_name}{self.name_sep}{i}'
            i += 1
        s_name = source.name
        self.sources[s_name] = source
        self.dependency_tree.add_node(s_name, set())
        self.emit(SourceManager.EVENT_SOURCE_ADDED, source)

        for field in source.get_data_fields():
            self.add_field(source, field)

        for dependency in source.get_required_fields():
            self.add_dependency_field(source, dependency)

    def remove_source(self, source: 'Source'):
        """

        :param source:
        :return:
        """
        if source.manager is not self:
            raise ValueError('This manager does not manage this source')
        for field in source.get_data_fields():
            self.remove_field(field)
        self.dependency_tree.remove_node(source.name)
        self.sources.pop(source.name).manager = None
        self.emit(SourceManager.EVENT_SOURCE_REMOVED, source)

    def rename_source(self, source: 'Source', new_name: str):
        """ This method must be called to rename a source.

        :param source:
        :param new_name:
        :return:
        """
        if source.manager is not self:
            raise ValueError('This manager does not manage this source')
        if new_name in self.sources:
            raise ValueError('A source with this name already exists')
        old_name = source.name
        source.name = new_name
        self.sources[new_name] = self.sources.pop(old_name)
        for field in source.get_required_fields():
            users = self.fields_users[field]
            users.remove(old_name)
            users.add(new_name)
            try:
                users = self._missing_fields[field]
            except KeyError:
                pass
            else:
                users.remove(old_name)
                users.add(old_name)
        self.dependency_tree.rename_node(old_name, new_name)
        self.emit(SourceManager.EVENT_SOURCE_RENAMED, source, old_name)

    def request_field_name(self, field_base: str = 'new') -> str:
        """Called by sources managed by self to obtain a new unique name for a field.
        :param field_base: base name used to generate the new name.
                           Field_base is returned as is if there is no conflict.
        :return: a unique name for the field.
        """
        field = field_base
        i = 0
        # Check for duplicate field
        while field in self.fields_provider:
            field = f'{field_base}{self.name_sep}{i}'
            i += 1
        return field

    def add_field(self, source: 'Source', field: str) -> str:
        """ Adds a field to the manager.
        This method is called by sources managed by self.
        
        :param source:
        :param field:
        :return: The name of the field actually created
        """
        new_field = self.request_field_name(field)

        # If field must be renamed, renames it in the source
        if new_field != field:
            source.rename_field(field, new_field)
            field = new_field
        self.fields_provider[field] = source
        self.fields_users[field] = set()

        # If the field was previously missing, provide it.
        if field in self._missing_fields:
            missing = True
            self._provide_missing_dependency(source, field)
        else:
            missing = False
        self.emit(SourceManager.EVENT_FIELD_ADDED, source, field, missing)
        return field

    def remove_field(self, field: str):
        """ Removes a field from the manager.
        This method is called by sources managed by self.

        :param field:
        :return:
        """
        provider = self._do_remove_field(field)
        users = self.fields_users[field]
        if users:
            missing = True
            for s_name in users:
                self._add_missing_dependency(self.sources[s_name], field)
        else:
            missing = False
            self.fields_users.pop(field)
        self.emit(SourceManager.EVENT_FIELD_REMOVED, provider, field, missing)

    def rename_field(self, old_name: str, new_name: str):
        """Call this method to rename a field.
        This method is called by sources managed by self.
        :param old_name:
        :param new_name:
        """
        if old_name == new_name:
            return
        if new_name in self.fields_provider:
            raise DuplicateFieldName(f'Cannot rename field {old_name} to {new_name}\n{new_name} already exists.')

        # Removes the old_field
        provider = self._do_remove_field(old_name)
        provider.rename_field(old_name, new_name)
        users = self.fields_users.pop(old_name)

        # Adds the new field
        new_name = self.add_field(provider, new_name)

        # Provide the missing dependency, if new_name was missing
        if new_name in self._missing_fields:
            self._provide_missing_dependency(provider, new_name)

        # Updates the dependencies of sources that were dependent of old_name
        for s_name in users:
            source = self.sources[s_name]
            source.manager_renames_dependency(old_name, new_name)
            self._add_field_user(new_name, s_name)
        self.emit(SourceManager.EVENT_FIELD_RENAMED, provider, old_name, new_name)
        self.emit(SourceManager.EVENT_FIELD_REMOVED, provider, old_name, False)

    def add_dependency_field(self, source: 'Source', field: str):
        """ Adds a dependency to a source
        This method is called by sources managed by self.

        :param source:
        :param field:
        :return:
        """
        self._add_field_user(field, source.name)
        try:
            dependency_source = self.fields_provider[field]
        except KeyError:
            self._add_missing_dependency(source, field)
            dependency_source = None
        else:
            self.dependency_tree.add_dependency(source.name, dependency_source.name)
        self.emit(SourceManager.EVENT_DEP_ADDED, source, dependency_source, field)

    def remove_dependency_field(self, source: 'Source', field: str):
        """ Called by sources when they no longer use a dependency field

        :param source:
        :param field:
        :return:
        """
        self.fields_users[field].remove(source.name)
        if field in self._missing_fields:
            self._remove_missing_dependency(source, field)
        else:
            # updates the dependency tree accordingly
            provider = self.fields_provider[field]
            dependencies = source.get_required_fields()
            for f in provider.get_data_fields():
                if f in dependencies:
                    # provider is still required by source, nothing to do
                    break
            else:
                # provider is no more required by source.
                self.dependency_tree.remove_dependency(source.name, provider.name)
        self.emit(SourceManager.EVENT_DEP_REMOVED, source, field)

    def get_fields(self):
        return self.fields_provider.items()

    def get_missing_fields(self):
        return self._missing_fields.keys()

    def _do_remove_field(self, field: str) -> 'Source':
        """ Called internally to remove a field.

        :param field:
        :return: The provider of the field
        """
        try:
            provider = self.fields_provider.pop(field)
        except KeyError:
            raise FieldNameError(f'Cannot remove field {field}\n{field} does not exists.')
        return provider

    def _add_missing_dependency(self, source: 'Source', field: str):
        """ Called when a source request a field dependency that is not provided by any source.

        :param source:
        :param field:
        :return:
        """
        try:
            self._missing_fields[field].add(source.name)
        except KeyError:
            self._missing_fields[field] = {source.name}
        self.emit(SourceManager.EVENT_MISSING_ADDED, source, field)

    def _remove_missing_dependency(self, source: 'Source', field: str):
        """ Called when a missing dependency field is removed from a source.
        If the field is no more used by any source, the EVENT_MISSING_REMOVED is emitted.

        :param source:
        :param field:
        :return:
        """
        d_sources = self._missing_fields[field]
        d_sources.remove(source.name)
        if not d_sources:
            self._missing_fields.pop(field)
            self.emit(SourceManager.EVENT_MISSING_REMOVED, field)

    def _provide_missing_dependency(self, source: 'Source', field: str):
        """ Called when a missing field is provided by a source.

        :param source: The source that provides the, previously, missing field.
        :param field: The missing field
        :return:
        """
        for d_source_name in self._missing_fields.pop(field):
            self.dependency_tree.add_dependency(d_source_name, source.name)
        self.emit(SourceManager.EVENT_MISSING_REMOVED, field)

    def init_evaluation(self):
        self.history = {}
        self.database.clear()

    def walk_evaluation(self) -> List[EvaluationStep]:
        self.init_evaluation()
        # for d_source_name in self._missing_fields.pop(field):
        #     self.dependency_tree.add_dependency(d_source_name, source.name)
        # self.emit(SourceManager.EVENT_MISSING_REMOVED, field)

        evaluation_state = EvaluationStep()
        for source_name, dependencies, dependent, state in self.dependency_tree.walk():
            try:
                source = self.sources[source_name]
                # Ignores sources that return no data
                if not source.get_data_fields():
                    continue

                # Queries input for the source
                evaluation_state.step_name = EvaluationStep.PERFORMING_QUERY
                evaluation_state.source = source_name
                evaluation_state.item_index = 0
                evaluation_state.n_items = 1
                yield evaluation_state

                request = Request(source.get_required_fields())
                cursor = self.database.query(request)

                evaluation_state.item_index = 1
                yield evaluation_state

                # Performs the source evaluation
                evaluation_state.item_index = 0
                evaluation_state.step_name = EvaluationStep.EVALUATING
                evaluation_state.n_items = len(cursor)

                if cursor.is_empty():
                    self.logger.warning('Warning, no input for "%s"', source_name)
                    yield evaluation_state

                results = []
                links = []
                for i, results, links in source.query_results(cursor):
                    evaluation_state.item_index = i
                    yield evaluation_state

                # Updates the database with the source results.
                evaluation_state.step_name = EvaluationStep.UPDTATING_DB
                evaluation_state.item_index = 0
                evaluation_state.n_items = len(results)

                transaction = None
                for i, result in enumerate(results):
                    if transaction is None:
                        transaction = self.database.start_update(result, links)
                    self.database.update(transaction, result)
                    evaluation_state.item_index = i
                    yield evaluation_state
                self.database.end_update(transaction)
            except Exception as ec:
                self.logger.error('Error on node "%s": %s', source_name, str(ec))
                raise EvaluationError(ec, source_name)

    def _add_field_user(self, field: str, source_name: str):
        """

        :param field:
        :param source_name:
        :return:
        """
        try:
            self.fields_users[field].add(source_name)
        except KeyError:
            self.fields_users[field] = {source_name}
            self.logger.warning('User "%s" added to the non existing field "%s"', source_name, field)


class Source(Mutative):
    EVENT_FIELD_RENAMED = 'field_renamed'

    def __init__(self, name=''):
        super().__init__()
        if not name:
            name = self.__class__.__name__
        self.name: str = name
        """A unique name identifying the source."""
        self.manager: Optional[SourceManager] = None
        self.manager_is_renaming_field = False
        self._value_source_obs: Dict[int, Set[int]] = {}

    @classmethod
    def get_supported_events(cls) -> Set[str]:
        events = super().get_supported_events()
        events.add(Source.EVENT_FIELD_RENAMED)
        return events

    @classmethod
    def class_path(cls):
        return 'Source'

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('name', self.name)
        state.add_version(self.__class__.__name__, 1)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        for value_source in self.value_sources():
            self.connect_value_source(value_source)
        self.name = state.get_parameters()['name']

    def query_results(self, cursor: Cursor):
        results = []
        reference_field = cursor.reference_field
        if reference_field is not None:
            links = [LinkUpdate(row_id, reference_field, True)]
        else:
            links = []

        for i, row in enumerate(cursor):
            result = self.evaluate(row)
            if reference_field is not None:
                result.tables[0].table[FIELD_ROW_ID] = [row[reference_field]]
            results.append(result)
            yield i, results, links

    def evaluate(self, parameters: CursorRow) -> Parcel:
        """
        :param parameters: The parameters returned by the request.
        :return: The result of the evaluation.
        """
        for sub_mm, _ in self.get_mutable_members():
            sub_mm.apply_members_values(parameters)
        return self.do_evaluate()

    def add_child_mutative(self, child):
        self.on_sub_child_added(self, [child])
        super().add_child_mutative(child)

    def remove_child_mutative(self, child):
        super().remove_child_mutative(child)
        self.on_sub_child_removed(self, [child])

    def on_sub_child_added(self, emitter: Mutative, children: List[Mutative]):
        child = children[0]
        for vs in child.value_sources():
            self.connect_value_source(vs)
            if self.manager is not None and vs.type == ValueSource.REFERENCE:
                self.manager.add_dependency_field(self, vs.value)
        super().on_sub_child_added(emitter, children)

    def on_sub_child_removed(self, emitter: Mutative, children):
        child = children[0]
        for vs in child.value_sources():
            self.disconnect_value_source(vs)
            if self.manager is not None and vs.type == ValueSource.REFERENCE:
                self.remove_dependency_if_not_used(vs.value)
        super().on_sub_child_removed(emitter, children)

    @abstractmethod
    def do_evaluate(self) -> Parcel:
        raise NotImplementedError()

    @abstractmethod
    def get_data_fields(self) -> Set[Any]:
        """
        :return: The list of fields handled by this source.
        """
        raise NotImplementedError()

    def rename_field(self, field, new_name):
        self.do_rename_field(field, new_name)
        self.emit(Source.EVENT_FIELD_RENAMED, field, new_name)

    def connect_value_source(self, value_source: ValueSource):
        try:
            ids = self._value_source_obs[id(value_source)]
        except KeyError:
            ids = set()
            self._value_source_obs[id(value_source)] = ids
        ids.add(value_source.add_event_observer(ValueSource.EVENT_TYPE_CHANGED, self.on_value_source_type_changed))
        ids.add(value_source.add_event_observer(ValueSource.EVENT_VALUE_CHANGED, self.on_value_source_value_changed))
        ids.add(value_source.add_event_observer(Observable.EVENT_REMOVED, self.on_value_source_removed))
        return ids

    def disconnect_value_source(self, value_source: ValueSource):
        ids = self._value_source_obs.pop(id(value_source))
        for obs_id in ids:
            value_source.remove_event_observer(obs_id)

    def add_mutable_member(self, member_name, mutable_member):
        super().add_mutable_member(member_name, mutable_member)
        vs = mutable_member.value_source
        self.connect_value_source(mutable_member.value_source)
        if vs.type is vs.REFERENCE:
            self.manager.add_dependency_field(self, vs.value)
        return mutable_member

    def remove_mutable_member(self, member_name):
        mutable_member = super().remove_mutable_member(member_name)
        vs = mutable_member.value_source
        self.disconnect_value_source(vs)
        if vs.type is vs.REFERENCE:
            self.remove_dependency_if_not_used(vs.value)
        return mutable_member

    def on_value_source_type_changed(self, value_source: ValueSource, old_type):
        if self.manager is None:
            return
        if old_type == ValueSource.REFERENCE:
            old_dep = value_source.value
            self.remove_dependency_if_not_used(old_dep)
        elif value_source.type == ValueSource.REFERENCE:
            self.manager.add_dependency_field(self, value_source.value)

    def remove_dependency_if_not_used(self, old_dep):
        """ Removes a dependency if it is no longer in use """
        if old_dep not in self.get_required_fields():
            self.manager.remove_dependency_field(self, old_dep)

    def on_value_source_value_changed(self, value_source: ValueSource, old_value):
        if value_source.type != ValueSource.REFERENCE or self.manager is None or self.manager_is_renaming_field:
            return
        self.manager.remove_dependency_field(self, old_value)
        self.manager.add_dependency_field(self, value_source.value)

    def on_value_source_removed(self, value_source: ValueSource):
        self._value_source_obs.pop(id(value_source))
        if value_source.type == ValueSource.REFERENCE:
            self.remove_dependency_if_not_used(value_source.value)

    @abstractmethod
    def do_rename_field(self, field: Any, new_name: Any):
        """
        :param field:
        :param new_name:
        :return:
        """
        pass

    def manager_renames_dependency(self, old_name: str, new_name: str):
        """ Called by the SourceManager when it is renaming a dependency

        :param old_name:
        :param new_name:
        :return:
        """
        self.manager_is_renaming_field = True
        try:
            for value_source in self.value_sources():
                if value_source.type == ValueSource.REFERENCE and value_source.value == old_name:
                    value_source.set_value(new_name)
        finally:
            self.manager_is_renaming_field = False


class ContextSource(Source):
    NAME = '<ContextSource>'
    APP_DIR_NAME = 'AppDir'
    CONFIG_DIR_NAME = 'ConfigDir'
    DATA_DIR_NAME = 'DataDir'

    def __init__(self):
        super().__init__(ContextSource.NAME)
        self.app_dir = os.path.dirname(os.path.dirname(__file__))
        self.config_dir = '.'
        self.data_dir = '.'

    def get_data_fields(self):
        return {ContextSource.APP_DIR_NAME, ContextSource.CONFIG_DIR_NAME, ContextSource.DATA_DIR_NAME}

    def do_rename_field(self, field, new_name):
        raise NotImplementedError('Context fields can NOT be renamed.')

    def do_evaluate(self):
        results = {ContextSource.APP_DIR_NAME: [self.app_dir],
                   ContextSource.CONFIG_DIR_NAME: [self.config_dir],
                   ContextSource.DATA_DIR_NAME: [self.data_dir]}
        return Parcel([ParcelTable(results)], [])


ContextSource.register_class()
