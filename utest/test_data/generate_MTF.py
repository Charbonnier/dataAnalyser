import os

import numpy as np
from scipy.special import jn

from checker_utils import get_test_resources

test_date = 'Sun Jul 24 09:24:37 2016'
mtf_file_name = 'Square_Pixel_x={0:.1f}.csv'
mtf_measurements = ['Measurement {0}'.format(i) for i in range(3)]
radius = 40
pixel_size = 8
steps = 50
dynamic = 2**12
max_dynamic = .9
target_dynamic = .8
min_dynamic = .5
max_integration_time = 1000
min_integration_time = 10
x_lst = np.linspace(-radius, radius, 2*steps+1)


def psf(n):
    airy = []
    for i in n:
        row = []
        airy.append(row)
        for j in n:
            r = np.sqrt(i ** 2 + j ** 2)
            row.append((jn(1, r) / r) ** 2)
    airy = np.array(airy)
    airy[np.where(np.isnan(airy))] = 0.25
    return airy


def lsf(n):
    return np.sum(psf(n), 0)


def esf(n):
    return np.cumsum(lsf(n))


def pixel_esf(n, pixel_steps):
    pixel = np.zeros(len(n))
    pixel[range(int(len(n) /2), int(len(n)/2) + pixel_steps)] = 1
    return np.convolve(esf(n), pixel, 'same')


def mtf(a):
    return np.absolute(np.fft.fftshift(np.fft.fft(a)))


def gauss(x, mean, std):
    """
    :param np.ndarray x:
    :rtype: np.ndarray
    :return:
    """
    return 1/(std*np.sqrt(2*np.pi))*np.exp(-(x-mean)**2/200)


def spectrum(wavelengths):
    s = gauss(wavelengths, 450, 10)
    return s/np.amax(s)


def generate_MTF_data():
    """Generate simulated data returned by a fake spectrometer during an Edge Spread Function measurement.
    :rtype: list[dict[str, Any]]"""
    pixel_steps = int(radius / steps * pixel_size)
    p_esf = pixel_esf(x_lst, pixel_steps)
    p_esf /= np.amax(p_esf)
    wavelengths = np.linspace(400, 800, 401)
    integration_time = min_integration_time
    base_spectrum = spectrum(wavelengths) * dynamic * max_dynamic / min_integration_time
    path = get_test_resources()
    mtf_path = os.path.join(path, 'MTF')
    measurements = []
    for measurement_set in mtf_measurements:
        r_path = os.path.join(mtf_path, measurement_set)

        for x, i in zip(x_lst, p_esf):
            peak_intensity = i * max_dynamic * integration_time / min_integration_time
            if not min_dynamic < peak_intensity < max_dynamic:
                integration_time = max(min_integration_time, min(max_integration_time,
                                                                 target_dynamic / (i + 1e-34) * min_integration_time))
            s = np.round(i * base_spectrum * integration_time)
            data_dict = {'path': os.path.join(r_path, mtf_file_name.format(x)),
                         'date': test_date,
                         'position': x,
                         'integration time': integration_time,
                         'wavelength': wavelengths,
                         'intensities': s}
            measurements.append(data_dict)
    return measurements


def generate_MTF_data_as_str():
    ans = []
    for data_dict in generate_MTF_data():
        str_data_dict = {'path': data_dict['path'], 'date': str(data_dict['date']),
                         'position': '{0}'.format(data_dict['position']),
                         'integration time': '{0}'.format(data_dict['integration time']),
                         'wavelength': ['{0}'.format(w) for w in data_dict['wavelength']],
                         'intensities': ['{0}'.format(i_s) for i_s in data_dict['intensities']]}
        ans.append(str_data_dict)
    return ans


def generate_MTF_files():
    for data_dict in generate_MTF_data():
        path = data_dict['path']
        try:
            os.makedirs(os.path.dirname(path))
        except FileExistsError:
            pass

        with open(os.path.join(path), 'w') as f:
            f.write('Date:\t{0}\n'.format(data_dict['date']))
            f.write('Position:\t{0}\n'.format(data_dict['position']))
            f.write('Integration time:\t{0}\n'.format(data_dict['integration time']))
            for w, i_s in zip(data_dict['wavelength'], data_dict['intensities']):
                f.write('{0}\t{1}\n'.format(w, i_s))

if __name__ == '__main__':
    generate_MTF_files()