from unittest import TestCase

from dataanalyser.structure.dependencytree import DependencyTree, NodeState

simpleDependencyDict = {'a': {'b', 'c'}, 'd': {'b', 'c'}, 'b': set(), 'c': set()}
complexDependencyDict = {'a': {'b', 'c', 'e'}, 'd': {'b', 'c'}, 'b': {'c', }, 'e': set(), 'c': set()}


class TestDependencyTree(TestCase):
    def test_build(self):
        d = DependencyTree(simpleDependencyDict)
        self.assertEqual(d, simpleDependencyDict,
                         "__init__ or __eq__ with a dict failed.")
        """ Checks the __eq__ implementation."""
        self.assertEqual(d, d,
                         "__eq__ with an other DependencyTree fails.")

    def test_get_requires(self):
        d = DependencyTree(simpleDependencyDict)
        self.assertEqual(d.get_requires('a'), set(),
                         "Incorrect answer from add_node('a').")
        self.assertEqual(d.get_requires('b'), {'a', 'd'},
                         "Incorrect answer from add_node('b').")
        self.assertEqual(d.get_requires('c'), {'a', 'd'},
                         "Incorrect answer from add_node('c').")
        self.assertEqual(d.get_requires('d'), set(),
                         "Incorrect answer from add_node('d').")

    def test_add_existing_node_raises(self):
        d = DependencyTree(simpleDependencyDict)
        """ Adding a node that already exists in the tree should raise."""
        self.assertRaises(ValueError, d.add_node, 'a')

    def test_add_node(self):
        d = DependencyTree(simpleDependencyDict)
        """ Adding a node not in the tree is ok and only affects this node."""
        self.assertEqual(d.add_node('e'), {'e': NodeState.ok},
                         "Incorrect answer from add_node('e').")

    def test_add_missing_node(self):
        d = DependencyTree(simpleDependencyDict)
        d.remove_node('c')
        """ Adding a node marked as missing is ok."""
        self.assertEqual(d.add_node('c'), {'c': NodeState.ok, 'a': NodeState.ok, 'd': NodeState.ok},
                         "Incorrect answer from add_node('c').")

    def test_remove_node(self):
        d = DependencyTree(simpleDependencyDict)
        """ Trying to remove a node not in the tree raises."""
        self.assertRaises(KeyError, d.remove_node, 'not existing node')
        """ Removes a node required by other nodes.
            - The node should be marked as missing, and dependent node as broken."""
        self.assertEqual(d.remove_node('c'), {'c': NodeState.missing, 'a': NodeState.broken, 'd': NodeState.broken},
                         "Incorrect answer from remove_dependency('c').")
        """ Removes a node required by nobody.
             - The node should disappear, and dependency are affected but state is unchanged."""
        self.assertEqual(d.remove_node('a'), {'c': NodeState.missing, 'b': NodeState.ok},
                         "Incorrect answer from remove_dependency('a').")

    def test_add_dependency_to_non_existing_node(self):
        d = DependencyTree(simpleDependencyDict)
        """ Adding a dependency to a node not in the tree raises."""
        self.assertRaises(KeyError, d.add_dependency, 'e', 'a')

    def test_add_dependency(self):
        d = DependencyTree(simpleDependencyDict)
        """ Adding a dependency to a node is ok, even if the dependency Id is not in the tree."""
        self.assertEqual(d.add_dependency('a', 'e'), {'a': NodeState.broken, 'e': NodeState.missing},
                         "Incorrect answer from add_dependency().")

    def test_add_dependency_loop(self):
        d = DependencyTree(simpleDependencyDict)
        d.add_node('f')
        d.add_dependency('f', 'a')
        """ Creates a dependency loop: c -> f -> a -> c
            - Should raise
            - Should leave the tree unaffected"""
        self.assertRaises(ValueError, d.add_dependency, 'c', 'f')
        self.assertEqual(d['c'], set(),
                         "Creating a dependency loop should not change the tree. ('c')")
        self.assertEqual(d.get_requires('f'), set(),
                         "Creating a dependency loop should not change the tree. ('f')")

    def test_remove_dependency(self):
        d = DependencyTree(simpleDependencyDict)
        self.assertEqual(d.remove_dependency('a', 'b'), {'a': NodeState.ok, 'b': NodeState.ok},
                         "Incorrect answer from remove_dependency()")
        self.assertEqual(d['a'], {'c'},
                         "Unexpected dependencies after remove_dependency().")
        """ Must raise as 'b' is not a dependency of 'a' anymore."""
        self.assertRaises(ValueError, d.remove_dependency, 'a', 'b')
        """ Must raise as 'f' is not in the tree."""
        self.assertRaises(KeyError, d.remove_dependency, 'f', 'g')
        """ A missing node not required anymore should be destroyed"""
        d.remove_node('b')
        d.remove_dependency('d', 'b')
        self.assertRaises(KeyError, d.__getitem__, 'b')

    def test_items(self):
        d = DependencyTree(simpleDependencyDict)
        self.assertEqual(dict(d.items()), simpleDependencyDict, "Items are different from initial dependency tree.")

    def test__getitem__(self):
        d = DependencyTree(simpleDependencyDict)
        self.assertEqual(simpleDependencyDict['a'], d['a'])

    def test_walk(self):
        d = DependencyTree(complexDependencyDict)
        evaluated = set()
        for nodeId, dependencies, requiring, state in d.walk():
            self.assertNotIn(nodeId, evaluated, "Node {0} already walked by.".format(nodeId))
            for d in dependencies:
                self.assertIn(d, evaluated, "Trying to walk by a node but dependencies are not met.")
            evaluated.add(nodeId)
