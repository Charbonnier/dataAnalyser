from unittest import TestCase

from dataanalyser.config.jsonwriter import JsonWriter
from dataanalyser.sources.textfile import TextFile
from dataanalyser.structure.database.simpledatabase import PyDB
from dataanalyser.structure.source import SourceManager, FieldNameError, ContextSource


class TestSourceManager(TestCase):
    def test_context_source(self):
        manager = SourceManager(PyDB())
        self.assertIn(ContextSource.NAME, manager.sources.keys(), 'ContextSource not found')

    def test_register_source(self):
        source1 = TextFile('source1')
        source2 = TextFile('source2')
        manager = SourceManager(PyDB())
        manager.register_source(source1)
        self.assertRaises(ValueError, manager.register_source, source1)
        self.assertRaises(FieldNameError, manager.register_source, source2)
        renamed_fields = manager.register_source(source2, rename_duplicate=True)
        self.assertEqual(renamed_fields, {'Path': 'Path.source2'})
        manager2 = SourceManager(PyDB())
        self.assertRaises(ValueError, manager2.register_source, source1)

    def test_remove_source(self):
        source1 = TextFile('source1')
        source2 = TextFile('source2')
        manager = SourceManager(PyDB())
        manager.register_source(source1)
        manager.remove_source(source1)
        self.assertRaises(ValueError, manager.remove_source, source2)
        manager.register_source(source1)

    def test_walk_evaluation(self):
        manager = SourceManager(PyDB())
        writer = JsonWriter()
        data = writer.read(r'../../../dataanalyser/test_resources/test_SourceManager.walk_evaluation.json')
        sources = data['sources']
        for source in sources.values():
            manager.register_source(source)
        for _ in manager.walk_evaluation():
            pass
