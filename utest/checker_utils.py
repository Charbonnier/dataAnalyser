import os


def get_test_resources():
    path = __file__
    while True:
        path = os.path.dirname(path)
        if os.path.basename(path) == 'dataanalyser':
            path = os.path.dirname(path)
            break
    return os.path.join(path, 'test_resources')