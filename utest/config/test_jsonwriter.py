from unittest import TestCase
from tempfile import NamedTemporaryFile

from potable.saveable import Saveable, State
from dataanalyser.config.jsonwriter import JsonWriter
from dataanalyser.structure.mutative import ValueSource


class DebugSaveable(Saveable):
    def __init__(self, param=None) -> None:
        super().__init__()
        self.param = param

    def get_state(self) -> State:
        state = super().get_state()
        state.add_parameter('param', self.param)
        return state

    def set_state(self, state: State):
        super().set_state(state)
        self.param = state.get_parameters()['param']


DebugSaveable.register_class()


def write_read(data):
    f = NamedTemporaryFile()
    saver = JsonWriter()
    saver.write(f.name, data)
    result = saver.read(f.name)
    return result


class TestValueSourceBuilder(TestCase):
    def test_read_write(self):
        data = {'constant': ValueSource(ValueSource.CONSTANT, 0),
                'reference': ValueSource(ValueSource.REFERENCE, 'some field')}
        result = write_read(DebugSaveable(data)).param
        self.assertEqual(data, result)


class TestRawBuilders(TestCase):
    def test_read_write(self):
        data = {
            'some_string': 'Portez ce vieux whisky au juge blond qui fume.\nThe quick brown fox jumps over the lazy dog.',
            'bool_false': False, 'bool_true': True, 'none': None, 'int': 42, 'float': 3.141592,
            'dictionary': {'a': 1, 'b': 2},
            'list': [False, True, None, 42, 3.141592, {'a': 1, 'b': 2}],
            'tuple': (False, True, None, 42, 3.141592, {'a': 1, 'b': 2}),
            'set': {False, True, None, 42, 3.141592}
        }
        result = write_read(DebugSaveable(data)).param
        self.assertEqual(data, result)
