from unittest import TestCase
import numpy as np

from dataanalyser.structure.database import Parcel, ParcelTable
from dataanalyser.structure.database.simpledatabase import PyDB
from dataanalyser.structure.view import ViewManager
from dataanalyser.views.colorfactory import MapColorFactory
from dataanalyser.views.plot import PlotView
from dataanalyser.views.xyplot import PlotXY


class TestPlotXY(TestCase):
    def test_draw(self):
        db = PyDB()
        x = np.linspace(-50., 50., 100)
        db.single_update(Parcel([ParcelTable({'y2': [np.sin((x + phase) * np.pi / 180.) for phase in range(5)],
                                              'phase': [i for i in range(5)]})]), [])
        viewmanager = ViewManager(db)
        plot_view = PlotView()
        viewmanager.views['plot'] = plot_view

        first_plot = PlotXY()
        second_plot = PlotXY()
        second_plot.categories_enabled = True

        plot_view.plots.append(first_plot)
        plot_view.plots.append(second_plot)

        first_data = first_plot.get_data_provider()
        ":type: dataanalyser.views.xyplot.PlotXYDataProvider"
        first_data.mutable_members['x'].set_constant_value(x)
        first_data.mutable_members['y'].set_constant_value(np.cos(x * np.pi / 180.))
        first_data.color.mutable_members['colors'].set_constant_value('#FF0000')
        second_data = second_plot.get_data_provider()
        ":type: dataanalyser.views.xyplot.PlotXYDataProvider"
        second_data.mutable_members['x'].set_constant_value(x)
        second_data.mutable_members['y'].set_reference_value('y2')
        second_data.color = MapColorFactory()
        second_data.color.mutable_members['color_parameter'].set_reference_value('phase')

        viewmanager.generate_view(plot_view)
        plot_view._default_renderer.save('test_plotXY_draw.png')
        plot_view._default_renderer.save('test_plotXY_draw2.png')
        viewmanager.export_view(plot_view, export_path='test_plotXY_export.png')
        plot_view.categories_filter.field = 'phase'
        plot_view.categories_filter.enabled = True
        viewmanager.export_view_all_categories(plot_view, export_path='test_plotXY_export_categories.png')
