from unittest import TestCase

from dataanalyser.views.tableview import TableView
from config.test_jsonwriter import write_read


class TestTableView(TestCase):
    def make_table_view(self):
        table_view = TableView()
        x_id, column = table_view.new_column('X', '{:.6e}')
        for x in [1.09, 2.12, 5.3, None, 'ui']:
            column.set_owner_attribute_value(table_view.body, x_id, x)

        msg_id, column = table_view.new_column('msg', '{:s}')
        for msg in ['a', 'b', None, 5, 'd']:
            column.set_owner_attribute_value(table_view.body, msg_id, msg)
        return table_view

    def test_export(self):
        table_view = self.make_table_view()
        table_view.generate_view()
        table_view.export(force=True, export_path='table_view.csv')

    def test_save_load(self):
        table_view = self.make_table_view()

        new_table_view = write_read({'t': table_view})['t']