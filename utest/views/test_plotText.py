from unittest import TestCase
import numpy as np

from dataanalyser.structure.database import Parcel, ParcelTable
from dataanalyser.structure.database.simpledatabase import PyDB
from dataanalyser.structure.view import ViewManager
from dataanalyser.views.plot import PlotView
from dataanalyser.views.textplot import PlotText


class TestPlotText(TestCase):
    def test_draw(self):
        db = PyDB()
        x_data = np.linspace(0, 10., 11)
        db.single_update(Parcel([ParcelTable({'x': [x for x in x_data],
                                              'y': [x for x in x_data],
                                              'text': ['text {}'.format(x) for x in x_data]})]), [])
        viewmanager = ViewManager(db)
        plot_view = PlotView()
        viewmanager.views['plot'] = plot_view

        second_plot = PlotText()

        plot_view.plots.append(second_plot)

        second_data = second_plot.get_data_provider()
        ":type: dataanalyser.views.xyplot.PlotXYDataProvider"
        second_data.mutable_members['x'].set_reference_value('x')
        second_data.mutable_members['y'].set_reference_value('y')
        second_data.mutable_members['text'].set_reference_value('text')

        viewmanager.generate_view(plot_view)
        plot_view._default_renderer.save('test_plotText_draw.png')
        viewmanager.export_view(plot_view, export_path='test_plotText_export.png')

