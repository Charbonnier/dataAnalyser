from tempfile import NamedTemporaryFile
from unittest import TestCase

from dataanalyser.config.jsonwriter import JsonWriter
from dataanalyser.views.colorfactory import GradientColorFactory, MapColorFactory
from dataanalyser.views.scale import ConstantLimit


class TestGradientColorFactory(TestCase):
    def test_save(self):
        gc = self.make_gradient_color()
        ":type: GradientColorFactory"
        data = {'gradient': gc}
        f = NamedTemporaryFile()
        saver = XMLWriter()
        saver.write(f.name, data)
        result = saver.read(f.name)
        r_gc = result['gradient']
        self.assertEqual(gc.start_color, r_gc.start_color)
        self.assertEqual(gc.end_color, r_gc.end_color)
        self.assertEqual(gc.under, r_gc.under)
        self.assertEqual(gc.over, r_gc.over)
        self.assertEqual(gc.bad, r_gc.bad)
        return result

    def test_get_values(self):
        cf = self.make_gradient_color()
        for v in [0, 3, 4]:
            cf.apply_members_values({'cp': v})
        colors = cf.get_values()
        equal = colors == [[1., 0., 0., 1.], [1./255*63, 1./255*192, 0., 1.], [0., 1., 0., 1.]]
        self.assertTrue(equal.all())

        cf.color_scale.min = ConstantLimit(ConstantLimit.MIN)
        cf.color_scale.min.limit = 0
        cf.color_scale.max = ConstantLimit(ConstantLimit.MAX)
        cf.color_scale.max.limit = 2

        cf.clear()
        for v in [-1, 0, 1, 2, 3]:
            cf.apply_members_values({'cp': v})
        colors = cf.get_values()
        equal = colors == [[0., 0., 0., 1.], [1., 0., 0., 1.], [1./255 * 127, 1./255 * 128, 0., 1.],
                           [0., 1., 0., 1.], [0., 0., 0., 1.]]
        self.assertTrue(equal.all())

    def make_gradient_color(self):
        cf = GradientColorFactory()
        cf.start_color = (1., 0., 0.)
        cf.end_color = (0., 1., 0.)
        cf.mutable_members['color_parameter'].set_reference_value('cp')
        return cf


class TestMapColorFactory(TestCase):
    def test_save(self):
        gc = self.make_map()
        ":type: MapColorFactory"
        data = {'gradient': gc}
        f = NamedTemporaryFile()
        saver = XMLWriter()
        saver.write(f.name, data)
        result = saver.read(f.name)
        r_gc = result['gradient']
        self.assertEqual(gc.map.name, r_gc.map.name)
        self.assertEqual(gc.under, r_gc.under)
        self.assertEqual(gc.over, r_gc.over)
        self.assertEqual(gc.bad, r_gc.bad)
        return result

    def test_get_values(self):
        cf = self.make_map()

        for v in [-1, 0, 1, 2, 3]:
            cf.apply_members_values({'cp': v})
        colors = cf.get_values()
        equal = colors == cf.map([-0.5, 0, 0.5, 1., 1.5])
        self.assertTrue(equal.all())

    def make_map(self):
        cf = MapColorFactory()
        cf.over = (1., 0., 0., 1.)
        cf.under = (0., 0., 0., 1.)
        cf.mutable_members['color_parameter'].set_reference_value('cp')
        cf.color_scale.min = ConstantLimit(ConstantLimit.MIN)
        cf.color_scale.min.limit = 0
        cf.color_scale.max = ConstantLimit(ConstantLimit.MAX)
        cf.color_scale.max.limit = 2
        cf.set_map_from_name('jet')
        return cf