from unittest import TestCase

from config.test_jsonwriter import write_read
from dataanalyser.structure.database.dbfilter import FilterStructure, AND_LINK, OR_LINK
from dataanalyser.structure.mutative import ValueSource


class FSDebug(FilterStructure):
    def set_function(self, function_name):
        self.function_name = function_name


class TestFilterStructure(TestCase):
    @staticmethod
    def build_filter():
        # ("a" < 50.0 OR ("b" < 50.0 OR "c" < 50.0 AND (NOT "f" < 50.0)) AND "d" < 50.0) AND "e" < 50.0
        a = FSDebug(AND_LINK, '<',
                    args=[ValueSource(ValueSource.REFERENCE, 'a'), ValueSource(ValueSource.CONSTANT, 50.)])
        b = FSDebug(AND_LINK, '<',
                    args=[ValueSource(ValueSource.REFERENCE, 'b'), ValueSource(ValueSource.CONSTANT, 50.)])
        c = FSDebug(OR_LINK, '<',
                    args=[ValueSource(ValueSource.REFERENCE, 'c'), ValueSource(ValueSource.CONSTANT, 50.)])
        f = FSDebug(AND_LINK, '<',
                    invert=True, args=[ValueSource(ValueSource.REFERENCE, 'f'), ValueSource(ValueSource.CONSTANT, 50.)])
        d = FSDebug(AND_LINK, '<',
                    args=[ValueSource(ValueSource.REFERENCE, 'd'), ValueSource(ValueSource.CONSTANT, 50.)])
        e = FSDebug(AND_LINK, '<',
                    args=[ValueSource(ValueSource.REFERENCE, 'e'), ValueSource(ValueSource.CONSTANT, 50.)])
        s0 = FSDebug(AND_LINK, 's0', sub_structure=[f])
        s1 = FSDebug(OR_LINK, 's1', sub_structure=[b, c, s0])
        s2 = FSDebug(AND_LINK, 's2', sub_structure=[a, s1, d])
        return [s2, e]

    @staticmethod
    def write_function(fs):
        fct_str = TestFilterStructure.write_arg(fs.args[0]) + ' ' + fs.function_name
        if len(fs.args) > 1:
            fct_str += ' ' + TestFilterStructure.write_arg(fs.args[1])
        return fct_str

    @staticmethod
    def write_arg(arg):
        if arg.type == ValueSource.REFERENCE:
            return '"{}"'.format(arg.value)
        else:
            return str(arg.value)

    @staticmethod
    def write_expression(filters):
        return FilterStructure.write_expression(filters, TestFilterStructure.write_function, lambda lnk: lnk)

    def test_walk(self):
        f_lst = TestFilterStructure.build_filter()
        expression = TestFilterStructure.write_expression(f_lst)
        expected = '("a" < 50.0 OR ("b" < 50.0 OR "c" < 50.0 AND (NOT "f" < 50.0)) AND "d" < 50.0) AND "e" < 50.0'
        self.assertEqual(expression, expected)

    def test_save_filter(self):
        f_lst = TestFilterStructure.build_filter()
        f_lst_readed = write_read({'filters': f_lst})
        expression = TestFilterStructure.write_expression(f_lst)
        expression_readed = TestFilterStructure.write_expression(f_lst_readed['filters'])
        self.assertEqual(expression, expression_readed)
