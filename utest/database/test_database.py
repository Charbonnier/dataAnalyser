import math
import random
from unittest import TestCase

from dataanalyser.structure.database import Parcel, ParcelTable, Link, Request, LinkUpdate, FilterStructure
from dataanalyser.structure.database.simpledatabase import PyDB
from dataanalyser.structure.mutative import ValueSource

Database = PyDB


class TestData0:
    """ Starts by loading simulated data representing the measurement of a bunch of devices.
        For each device a pair of files have been generated:
            file A: Contains the description of the device and test information
            file B: Contains the result of several successive measurement.
        All files are stored in the same folder.
        File names contain basic information allowing identifying the device:
            - The device serial number.
            - Some part identifying the type of the file.
        For each measurement the following data are stored:
            - timestamp of each measurement (fist measurement starting at 0)
            - temperature of the device
            - power drawn by the device
            - score represents a kind of merit factor, higher is better.
    """

    def __init__(self, n_devices=20, n_measurements=5):
        self.nMeasurements = n_measurements
        self.nDevices = n_devices
        self.dataTable = {'folder': ['2016-04-29_Measurement'],
                          'descriptionFileName': [self.device_serial_number(i) + '.desc' for i in range(n_devices)],
                          'measurementFileName': [self.build_measurement_filename(self.device_serial_number(i))
                                                  for i in range(n_devices)],
                          'serialNumber': [self.device_serial_number(i) for i in range(n_devices)],
                          'timeStamps': [range(0, 10 * n_measurements, 10)] * n_devices,
                          'temperatures': self.temperatures(),
                          'powers': self.powers(),
                          'scores': self.scores()
                          }

    def __getitem__(self, item):
        return self.dataTable[item]

    @staticmethod
    def build_measurement_filename(serial_number):
        return serial_number + '.meas'

    def make_database(self):
        # Creates the Database
        position_f = 'position'
        db = Database()
        # Stores the folder name in the Database
        db.single_update(Parcel([ParcelTable({'folder': self['folder']})]), [])
        # Stores the description file and its content in the Database
        db.single_update(Parcel([ParcelTable({'descriptionFileName': self['descriptionFileName'],
                                              'serialNumber': self['serialNumber'],
                                              position_f: [0 for _ in range(self.nDevices)]})]),
                         [LinkUpdate(position_f, 'folder')])
        # Stores the calculated measurement file name in the Database
        cursor = db.query(Request(['serialNumber']))
        for row, values in enumerate(cursor):
            measurement_table = ParcelTable(
                {'timeStamps': self['timeStamps'][row],
                 'temperatures': self['temperatures'][row],
                 'powers': self['powers'][row],
                 'scores': self['scores'][row],
                 'device': [0] * self.nMeasurements
                 }
            )

            db.single_update(Parcel([ParcelTable(
                {'measurementFileName': [TestData0.build_measurement_filename(values['serialNumber'])],
                 position_f: [row]}),
                measurement_table], [Link(1, 0, 'device')]),
                [LinkUpdate(position_f, cursor.reference_field, True)])
        return db

    def temperatures(self):
        ans = []
        for i in range(self.nDevices):
            tend = 35 + random.random() * 25
            ans.append([25 + tend * (1 - math.exp(-m / self.nDevices) + self.noise(1))
                        for m in range(self.nMeasurements)])
        return ans

    def powers(self):
        ans = []
        for i in range(self.nDevices):
            p = 50 + random.randint(0, 5)
            ans.append([p + self.noise(1) for _ in range(self.nMeasurements)])
        return ans

    def scores(self):
        ans = []
        for i in range(self.nDevices):
            s = 4800 + random.random() * 350
            ans.append([s + self.noise(100) for _ in range(self.nMeasurements)])
        return ans

    @staticmethod
    def noise(amplitude):
        return (random.random() - 0.5) * amplitude

    @staticmethod
    def device_serial_number(index):
        return '2016-{0:05}'.format(index)


class TestPyDB(TestCase):
    def test_simple_query(self):
        db = TestData0().make_database()
        cursor = db.query(Request(['serialNumber']))
        v = None
        for i, v in enumerate(cursor):
            self.assertEqual(v['serialNumber'], TestData0.device_serial_number(i))
        self.assertIsNotNone(v, msg='Query returned no value')

    def test_multiple_fields_query(self):
        test_data = TestData0()
        db = test_data.make_database()
        cursor = db.query(Request(['serialNumber', 'powers']))
        results = {}
        for row in cursor:
            serial_number = row['serialNumber']
            powers = row['powers']
            try:
                results[serial_number].add(powers)
            except KeyError:
                results[serial_number] = {powers, }
        for serial_number, powers in results.items():
            i_device = test_data.dataTable['serialNumber'].index(serial_number)
            self.assertEqual(powers, set(test_data.dataTable['powers'][i_device]))

    def test_multiple_fields_query_with_filter(self):
        test_data = TestData0()
        db = test_data.make_database()
        filter_structure = FilterStructure('AND', '>', args=[ValueSource(ValueSource.REFERENCE, 'powers'),
                                                             ValueSource(ValueSource.CONSTANT, 50.)])
        cursor = db.query(Request(['serialNumber', 'powers'], filter_structure))
        results = {}
        for row in cursor:
            serial_number = row['serialNumber']
            powers = row['powers']
            try:
                results[serial_number].add(powers)
            except KeyError:
                results[serial_number] = {powers, }
        for serial_number, powers in results.items():
            i_device = test_data.dataTable['serialNumber'].index(serial_number)
            ref_data = {v for v in test_data.dataTable['powers'][i_device] if v > 50.}
            self.assertEqual(powers, ref_data)


    def test_update(self):
        db = Database()

        # Adds a single value to the database
        number_f = 'number'
        number_v = [0]
        parcel = Parcel([ParcelTable({number_f: number_v})])
        db.single_update(parcel, [])

        self.assertEqual(db[number_f], number_v,
                         "Incorrect value stored in the field")
        # Appends new values to the field
        value2 = [1, 2]
        number_v = number_v + value2
        db.single_update(Parcel([ParcelTable({number_f: value2})]), [])
        self.assertEqual(db[number_f], number_v,
                         "Updating an existing field should extend the values in the parcel")

        # Adds a new field in the database
        position_f = 'position'
        english_f = 'english'
        english_v = ['zero', 'one', 'two']
        t = db.start_update(Parcel([ParcelTable({english_f: [], position_f: []})]),
                            [LinkUpdate(position_f, number_f, True)])
        ":type: src.structure.simpledatabase.PyUpdateTransaction"
        for i, v in enumerate(english_v):
            db.update(t, Parcel([ParcelTable({english_f: [v], position_f: [i]})]))
        db.end_update(t)

        self.assertEqual(db.get_field_table(number_f), db.get_field_table(english_f),
                         "english_f and number_f should be in the same table")

        # Adds a new number
        number_v.append(3)
        db.single_update(Parcel([ParcelTable({number_f: [number_v[3]]})]), [])
        self.assertEqual(db[number_f], number_v)
        self.assertEqual(len(db[english_f]), len(number_v),
                         "Fields in the same table should have the same number of entries")
        english_v.append('three')
        db.single_update(Parcel([ParcelTable({english_f: [english_v[3]], position_f: [3]})]),
                         [LinkUpdate(position_f, number_f, True)])
        self.assertEqual(db[english_f], english_v)

    def test_update_reordering(self):
        db = Database()
        number_f = 'number'
        number_v = [0, 1, 2, 3]
        db.single_update(Parcel([ParcelTable({number_f: number_v})]), [])

        # Checks update reordering
        # Adds a new field in the table in one shot
        position_f = 'position'
        french_f = 'français'
        french_v = ['zero', 'deux', 'trois', 'un']
        db.single_update(Parcel([ParcelTable({french_f: french_v, position_f: [0, 2, 3, 1]})]),
                         [LinkUpdate(position_f, number_f, True)])
        self.assertEqual(db.get_field_table(french_f), db.get_field_table(number_f),
                         "french_f and number_f should be in the same table")

        # Checks association between numbers and corresponding french.
        # Note that testing db[french_f] == french_v might depend on the implementation while the following test should
        # not.
        self.assertEqual(dict(zip(db[french_f], db[number_f])), dict(zip(['zero', 'un', 'deux', 'trois'], number_v)),
                         "Incorrect association of values in number_f and french_f")

    def test_update_multiple_tables(self):
        db = Database()
        number_f = 'number'
        number_v = [random.randint(10, 900) for _ in range(20)]
        db.single_update(Parcel([ParcelTable({number_f: number_v})]), [])

        position_f = 'position'
        rest_f = '3_rest'
        rest_v = ['one', 'two', 'zero']
        rest = [1, 2, 0]
        link = [rest.index(v % 3) for v in number_v]
        db.single_update(Parcel([ParcelTable({rest_f: rest_v}),
                                 ParcelTable({Database.LINK_FIELD: link,
                                              position_f: range(len(number_v))})],
                                [Link(1, 0, Database.LINK_FIELD)]),
                         [LinkUpdate(position_f, number_f, True)])

        self.assertNotEqual(db.get_field_table(rest_f), db.get_field_table(number_f),
                            "'3_rest' and 'number' should not be in the same table")

        result = db.query(Request([number_f, rest_f]))
        for row in result:
            self.assertEqual(rest_v[rest.index(row[number_f] % 3)], row[rest_f])

    def test_full(self):
        db = TestData0().make_database()
        # Checks that the serial number and the measurement file name are in the same table since the
        # relation is 1-1
        self.assertEqual(db.get_field_table('serialNumber'), db.get_field_table('measurementFileName'),
                         "'serialNumber' and 'measurementFileName' should be in the same table")

        # Checks that the serial number is associated with the correct measurement file name
        cursor = db.query(Request(['serialNumber', 'measurementFileName']))
        for row in cursor:
            serial = row['serialNumber']
            fileName = row['measurementFileName']
            self.assertTrue(fileName.startswith(serial),
                            "The measurement file name should start with the device serial number")
