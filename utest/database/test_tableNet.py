from unittest import TestCase

from dataanalyser.structure.database.dbtablesnet import TableNet, TablePath


class TestTableNet(TestCase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.addTypeEqualityFunc(TablePath, self.tablePathEqual)

    def tablePathEqual(self, t1, t2, msg=None):
        self.assertDictEqual(t1.links, t2.links, msg)
        self.assertSetEqual(t1.roots, t2.roots, msg)

    def build_test_net(self):
        net = TableNet()
        for node in range(9):
            net.add_node(node)
        net.add_link(0, '0-1', 1)
        net.add_link(1, '1-2', 2)
        net.add_link(2, '2-3', 3)
        net.add_link(0, '0-4', 4)
        net.add_link(4, '4-1', 1)
        net.add_link(4, '4-5', 5)
        net.add_link(0, '0-5', 5)
        net.add_link(6, '6-2', 2)
        net.add_link(1, '1-7', 7)
        net.add_link(2, '2-8', 8)
        return net

    def test_add_link(self):
        net = TableNet()
        self.assertRaises(AssertionError, net.add_link, 0, 'field', 1)
        net.add_node(0)
        self.assertRaises(AssertionError, net.add_link, 0, 'field', 1)
        net.add_node(1)
        net.add_link(0, 'field', 1)

    def test_two_tables_path(self):
        """Checks the path calculation for a simple request with only two tables on the same branch (1, 3)."""
        net = self.build_test_net()
        expected_path = TablePath({1}, {1: {('1-2', 2), }, 2: {('2-3', 3), }})
        self.assertEqual(net.get_node_relation({1, 3}), expected_path)

    def test_three_tables_path(self):
        """Checks the path calculation for a simple request with tree tables on the same branch (1, 3, 4)."""
        net = self.build_test_net()
        expected_path = TablePath({4}, {1: {('1-2', 2), }, 2: {('2-3', 3), }, 4: {('4-1', 1)}})
        self.assertEqual(net.get_node_relation({1, 3, 4}), expected_path)

    def test_unrelated_path(self):
        """Checks the path calculation for a request with two tables without relationships (0 and 6)."""
        net = self.build_test_net()
        expected_path = TablePath({0, 6}, {})
        self.assertEqual(net.get_node_relation({0, 6}), expected_path)

    def test_indirect_relation_path(self):
        """If tables of different branches are requested without any common root, the common root should be returned in
        the path (3,8,7)."""
        net = self.build_test_net()
        expected_path = TablePath({1, }, {1: {('1-2', 2), ('1-7', 7)}, 2: {('2-3', 3), ('2-8', 8)}})
        self.assertEqual(net.get_node_relation({3, 8, 7}), expected_path)

    def test_multiple_roots(self):
        """Checks the path calculation for a request with two tables without relationships (1 and 5)."""
        net = self.build_test_net()
        expected_path = TablePath({4}, {4: {('4-1', 1), ('4-5', 5)}})
        self.assertEqual(net.get_node_relation({1, 5}), expected_path)

    def test_multiple_path(self):
        """Checks the path calculation for a request with several paths position (0 and 3)
        Only the shortest path should be kept."""
        net = self.build_test_net()
        expected_path = TablePath({0}, {0: {('0-1', 1), }, 1: {('1-2', 2), }, 2: {('2-3', 3), }})
        """ The following answer is correct but has two paths, only one should be returned, the shortest if possible.
        >>> {0}, {0: {('0-1', 1), ('0-4', 4)}, 4: {(4-1), 1}, 1: {('1-2', 2), }, 2: {('2-3', 3), }}
        """
        self.assertEqual(net.get_node_relation({0, 3}), expected_path)

    def test_branch_path(self):
        """Checks the path calculation or a request with tables on different branches with the same origin (1, 7, 3)."""
        net = self.build_test_net()
        expected_path = TablePath({1}, {1: {('1-2', 2), ('1-7', 7)}, 2: {('2-3', 3), }})
        self.assertEqual(net.get_node_relation({1, 7, 3}), expected_path)

    def test_get_paths_to_root(self):
        net = self.build_test_net()
        expected_path = TablePath({0, 6}, {6: {('6-2', 2), }, 2: {('2-3', 3), }, 0: {('0-1', 1), ('0-4', 4)},
                                           4: {('4-1', 1), }, 1: {('1-2', 2), }})
        self.assertEqual(net.get_paths_to_root(3), expected_path)
