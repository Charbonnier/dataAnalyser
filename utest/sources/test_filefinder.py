import os
from unittest import TestCase
from itertools import product

from dataanalyser.sources.filefinder import FindByRegex, FindByName
from dataanalyser.sources.textparser.textcaster import TextToFloat

from checker_utils import get_test_resources
from test_data.generate_MTF import x_lst, mtf_file_name, mtf_measurements
from config.test_jsonwriter import write_read, DebugSaveable


class TestFindByRegex(TestCase):
    def __init__(self, methodName: str = ...) -> None:
        super().__init__(methodName)
        self.obs_export_field_added_called = False
        self.obs_export_field_removed_called = False
        self.obs_export_field_renamed_called = False

    def obs_export_field_added(self, finder: FindByRegex, exp_name: str):
        self.obs_export_field_added_called = True

    def obs_export_field_removed(self, finder: FindByRegex, exp_name: str):
        self.obs_export_field_removed_called = True

    def obs_export_field_renamed(self, finder: FindByRegex, attribute, new_name: str):
        self.obs_export_field_renamed_called = True

    def test_find_files(self):
        finder = FindByRegex()
        finder.add_event_observer(FindByRegex.EVENT_EXPORT_FIELD_ADDED, self.obs_export_field_added)
        finder.add_event_observer(FindByRegex.EVENT_EXPORT_FIELD_REMOVED, self.obs_export_field_removed)

        raw_path = os.path.abspath(os.path.join(get_test_resources()))
        finder.search_path.mutable_members['raw_path'].set_constant_value(raw_path)
        finder.regex = r'Square_Pixel_x=([+-]?[0-9]+\.[0-9]{1})\.csv'
        regex_group = ('regex', 'index', '1')
        finder.group_caster[regex_group] = TextToFloat()

        # Checks event calls
        finder.enable_attribute(regex_group, True)
        self.assertTrue(self.obs_export_field_added_called, 'EVENT_EXPORT_FIELD_ADDED has not been called')

        finder.enable_attribute(regex_group, False)
        self.assertTrue(self.obs_export_field_removed_called, 'EVENT_EXPORT_FIELD_REMOVED has not been called')

        finder.enable_attribute(regex_group, True)

        finder.set_attribute_export_name(regex_group, 'x')
        self.assertTrue(self.obs_export_field_removed_called, 'EVENT_EXPORT_FIELD_REMOVED has not been called')

        for mm, _ in finder.get_mutable_members():
            mm.apply_members_values({})
        expected_files = {os.path.join(finder.search_path.path, 'MTF', meas, mtf_file_name.format(x)):
                          {regex_group: float('{0:.1f}'.format(x))}
                          for meas, x in product(mtf_measurements, x_lst)}
        returned_files = finder.get_files_and_attributes()
        self.assertEqual(returned_files.keys(), expected_files.keys())
        for p, a in returned_files.items():
            self.assertEqual(a, expected_files[p])

    def test_find_files_match_subdir(self):
        finder = self.make_finder_subdirs()
        expected_files = {os.path.join(finder.search_path.path, 'MTF', mtf_measurements[0], mtf_file_name.format(x))
                          for x in x_lst}
        self.assertEqual(set(finder.find_files()), expected_files)

    def make_finder_subdirs(self):
        finder = FindByRegex()
        raw_path = os.path.abspath(os.path.join(get_test_resources()))
        finder.search_path.mutable_members['raw_path'].set_constant_value(raw_path)
        finder.match_subdirs = True
        finder.regex = r'MTF[\\/]' + mtf_measurements[0] + r'[\\/]Square_Pixel_x=([+-]?[0-9]+\.[0-9]{1})\.csv'
        regex_group = ('regex', 'index', '1')
        finder.group_caster[regex_group] = TextToFloat()
        finder.enable_attribute(regex_group, True)
        finder.set_attribute_export_name(regex_group, 'x')
        for mm, __ in finder.get_mutable_members():
            mm.apply_members_values({})
        return finder

    def test_save_load(self):
        ff = self.make_finder_subdirs()
        new_ff = write_read(DebugSaveable(ff)).param
        self.assertEqual(ff, new_ff)


class TestFindByName(TestCase):
    def test_find_files(self):
        finder = self.make_finder()
        expected_files = {os.path.join(finder.search_path.path, finder.file_name): {}}
        self.assertEqual(finder.get_files_and_attributes(), expected_files)

    def make_finder(self):
        finder = FindByName()
        raw_path = os.path.abspath(os.path.join(get_test_resources(), 'MTF', 'Measurement 0'))
        finder.search_path.mutable_members['raw_path'].set_constant_value(raw_path)
        finder.file_name = mtf_file_name.format(x_lst[0])
        for mm, __ in finder.get_mutable_members():
            mm.apply_members_values({})
        return finder

    def test_save_load(self):
        ff = self.make_finder()
        new_ff = write_read(DebugSaveable(ff)).param
        self.assertEqual(ff, new_ff)
