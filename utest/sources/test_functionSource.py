from random import randint
from unittest import TestCase

from config.test_jsonwriter import write_read, DebugSaveable
from dataanalyser.sources.function import FunctionSource, Function
from dataanalyser.structure.mutative import MutableMemberSetter


def identity_test(a, b, c=1, *args, k, **kwargs):
    """Returns the arguments in a list for positional arguments and dict for keyword arguments"""
    out_args = [a, b, c]
    out_args.extend(args)
    out_kwargs = {'k': k}
    out_kwargs.update(kwargs)
    return out_args, out_kwargs


def no_varargs(a, b, c=1):
    return [a, b, c]


FunctionSource.register_function('test_identity', Function(identity_test, 'identity'))
FunctionSource.register_function('no_varargs', Function(no_varargs, 'no_varargs'))
FunctionSource.register_function('no_signature', Function(int, 'no_signature'))


class TestFunctionSource(TestCase):
    simple_test_identity_parameters = [1, 2, 3, 4, 5], {'k': 6, 'l': 7, 'm': 8}
    @staticmethod
    def make_identity(fct_class=None):
        if fct_class is None:
            fct_class = FunctionSource
        fct = fct_class()
        fct.function_id = 'test_identity'
        args, kwargs = TestFunctionSource.simple_test_identity_parameters
        for i, arg in enumerate(args):
            fct.set_positional_value(i, MutableMemberSetter(str(i), arg))
            # The following line is not to be used in normal code.
            # fct.apply_members_values should be called instead.
            fct.args[i] = arg
        for k, v in kwargs.items():
            fct.set_keyword_value(k, MutableMemberSetter(k, v))
            # The following line is not to be used in normal code.
            # fct.apply_members_values should be called instead.
            fct.kwargs[k] = v
        fct.dispatch_result = True
        fct.insert_returned_field('args', False)
        fct.insert_returned_field('kwargs', False)
        return fct

    def make_no_varargs(self):
        fct = FunctionSource()
        for arg in range(3):
            fct.append_positional_argument(arg)
        fct.function_id = 'no_varargs'
        fct.insert_returned_field('args', False)
        return fct

    def test_make_function(self):
        self.make_identity()

    def test_evaluate(self):
        fct = self.make_identity()
        parcel = fct.evaluate({})
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        self.assertEqual(result, TestFunctionSource.simple_test_identity_parameters)

    def test_save_load(self):
        fct = self.make_identity()
        new_fct = write_read(DebugSaveable(fct)).param
        parcel = new_fct.evaluate(None)
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        self.assertEqual(result, TestFunctionSource.simple_test_identity_parameters)

    def test_duplicate_error(self):
        fct = self.make_identity()
        self.assertRaises(ValueError, fct.add_keyword_argument, 'a', 15)
        self.assertRaises(ValueError, fct.add_keyword_argument, 'c', 15)

    def test_keyword_error(self):
        fct = self.make_no_varargs()
        fct.add_keyword_argument('d', 10)
        self.assertEqual(str(fct.kwargs_state['d']), str(FunctionSource.state_keyword_error('d')))

    def test_to_many_args(self):
        fct = self.make_no_varargs()
        fct.append_positional_argument(5)
        self.assertEqual(str(fct.args_mm[-1].state),
                         str(FunctionSource.state_to_many_arguments(fct.function.n_positional)))

    def test_no_signature(self):
        fct = self.make_identity()
        fct.function_id = 'no_signature'
        for arg_mm in fct.args_mm:
            self.assertIsNone(arg_mm.state)
        for state in fct.kwargs_state.values():
            self.assertIsNone(state)
        for i, mm in enumerate(fct.args_mm):
            self.assertEqual(mm.friendly_name, fct.positional_argument_name(i)[0])

    def test_remove_positional(self):
        fct = self.make_identity()
        fct.remove_positional_argument(1)
        self.assertEqual(fct.args_mm[0].friendly_name, 'a')
        args, kwargs = self.simple_test_identity_parameters
        args = args[:]
        args.pop(1)
        parcel = fct.evaluate(None)
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        self.assertEqual(result, (args, kwargs))

    def test_insert_positional(self):
        fct = self.make_identity()
        fct.insert_positional_argument(0, 10)
        self.assertEqual(fct.args_mm[0].friendly_name, 'a')
        parcel = fct.evaluate(None)
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        args, kwargs = self.simple_test_identity_parameters
        args = args[:]
        args.insert(0, 10)
        self.assertEqual(result, (args, kwargs))

    def test_remove_keyword(self):
        fct = self.make_identity()
        fct.remove_keyword_argument('l')
        parcel = fct.evaluate(None)
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        args, kwargs = self.simple_test_identity_parameters
        kwargs = {k: v for k, v in kwargs.items() if k != 'l'}
        self.assertEqual(result, (args, kwargs))

    def test_apply(self):
        fct = self.make_identity()

        parameters = {}
        input_args = []
        input_kwargs = {}
        for i, mm in enumerate(fct.args_mm):
            mm.value_source.set_type(mm.value_source.REFERENCE)
            parameter_name = str(i)
            mm.value_source.set_value(parameter_name)
            parameter_value = randint(0, 100)
            parameters[parameter_name] = parameter_value
            input_args.append(parameter_value)

        for kw, mm in fct.kwargs_mm.items():
            mm.value_source.set_type(mm.value_source.REFERENCE)
            mm.value_source.set_value(kw)
            parameter_value = randint(0, 100)
            parameters[kw] = parameter_value
            input_kwargs[kw] = parameter_value

        parcel = fct.evaluate(parameters)
        result = parcel.tables[0].table['args'][0], parcel.tables[0].table['kwargs'][0]
        self.assertEqual(result, (input_args, input_kwargs))
        fct.set_returned_field_ignored(0, True)
        parcel = fct.evaluate(parameters)
        self.assertRaises(KeyError, parcel.tables[0].table.__getitem__, 'args')

    def test_change_function(self):
        fct = self.make_identity()
        n_args = len(fct.args_mm)
        args = [(mm.friendly_name, mm.value_source.value) for mm in fct.args_mm]
        fct.function_id = 'no_varargs'
        fct.function_id = 'test_identity'
        # Checks that the number of positional arguments have been restored
        self.assertEqual(n_args, len(fct.args_mm))
        for i, arg in enumerate(args):
            self.assertEqual((fct.args_mm[i].friendly_name, fct.args_mm[i].value_source.value), arg)
