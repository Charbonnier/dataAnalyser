import numpy as np
from itertools import zip_longest, product
from random import randint
from unittest import TestCase

from config.test_jsonwriter import write_read, DebugSaveable
from dataanalyser.sources.textparser.basetextparser import StructureData
from dataanalyser.sources.textparser.tableparser import TableParser, NamedColumn, ColumnIndex, ColumnParser, IndexColumn
from dataanalyser.sources.textparser.textcaster import TextToInt


def random_rows_with_headers():
    data = {c: [randint(0, 1000) for _ in range(10)] for c in ['a', 'b', 'c', 'd', 'e', 'foo']}
    raw_data = [h + '\t' + '\t'.join([str(v) for v in col]) + '\n' for h, col in data.items()]
    return raw_data, data


def random_columns_with_headers():
    data = {c: [str(randint(0, 1000)) for _ in range(10)] for c in ['a', 'b', 'c', 'd', 'e', 'foo']}
    items = data.items()
    raw_data = ['\t'.join([h for h, _ in items])]
    columns = [t[1] for t in items]
    rows = ['\t'.join(column) for column in zip_longest(*columns, fillvalue='')]
    raw_data.extend(rows)
    return raw_data, data


def random_rows():
    data = [[str(randint(0, 1000)) for _ in range(10)] for _ in range(10)]
    raw_data = ['\t'.join(v) + '\n' for v in data]
    return raw_data, data


def random_columns():
    data = [[str(randint(0, 1000)) for _ in range(10)] for _ in range(10)]
    raw_data = ['\t'.join(column) for column in zip_longest(*data, fillvalue='')]
    return raw_data, data


class TestTableParser(TestCase):
    def test_row_header(self):
        tp = TableParser()
        tp.data_in_row = True
        tp.has_header = True
        nc_c = NamedColumn()
        nc_c.header_name = 'foo'
        nc_e = NamedColumn()
        nc_e.header_name = 'e'
        index_parser = ColumnIndex()
        index_parser.index = 0
        index_parser.enable_stepping = True
        index_parser.step = 3
        index_parser.text_caster = TextToInt()
        nc_c_i = tp.add_column(nc_c)
        nc_e_i = tp.add_column(nc_e)
        column_parser = ColumnParser()
        column_parser.text_caster = TextToInt()
        tp.add_parser(nc_c_i, 'column', column_parser)
        tp.add_parser(nc_c_i, 'index', index_parser)
        tp.add_parser(nc_e_i, 'column_e', column_parser)

        raw_data, data = random_rows_with_headers()
        result = tp.parse_data(raw_data)
        expected_result = StructureData()
        expected_result.singles = {'column': data['foo'], 'column_e': data['e']}
        expected_result.couples = {'index': [data['foo'][i] for i in range(0, 10, 3)]}
        self.assertTrue((result.singles['column'] == data['foo']).all())
        self.assertTrue((result.singles['column_e'] == data['e']).all())
        self.assertEqual(result.couples['index'], [data['foo'][i] for i in range(0, 10, 3)])

    def test_get_data_fields(self):
        tp = self.make_tableparser_row()
        raw_data, _ = random_rows()
        result = tp.parse_data(raw_data)
        fields = set(result.couples.keys())
        fields.update(result.singles.keys())
        self.assertEqual(fields, set(tp.get_data_fields()))

    def test_row(self):
        tp = self.make_tableparser_row()

        raw_data, data = random_rows()
        result = tp.parse_data(raw_data)
        expected_result = StructureData()
        expected_result.singles = {'column_2': np.array(data[2])}
        column_1_3 = [np.array(data[i]) for i in range(1, len(data), 3)]
        expected_result.couples = {'column_1_3': [np.array(data[i]) for i in range(1, len(data), 3)],
                                   'index_1_3': [data[j][i] for j, i in
                                                 product(range(1, len(data), 3), range(0, 10, 3))]
                                   }
        self.assertTrue((result.singles['column_2'] == np.array(data[2])).all())

        for a, r in zip(column_1_3, result.couples['column_1_3']):
            self.assertTrue((np.array(a) == r).all())

        self.assertTrue(result.couples['index_1_3'] == [data[j][i] for j, i in
                                                        product(range(1, len(data), 3), range(0, 10, 3))])

    def test_save_load(self):
        tp = self.make_tableparser_row()
        ds = DebugSaveable(tp)
        new_tp = write_read(ds).param
        self.assertEqual(tp, new_tp)

    def make_tableparser_row(self):
        tp = TableParser()
        tp.data_in_row = True
        tp.has_header = False
        nc_1_3 = IndexColumn()
        nc_1_3.index = 1
        nc_1_3.enable_stepping = True
        nc_1_3.step = 3
        nc_2 = IndexColumn()
        nc_2.index = 2
        index_parser = ColumnIndex()
        index_parser.index = 0
        index_parser.enable_stepping = True
        index_parser.step = 3
        nc_1_3_i = tp.add_column(nc_1_3)
        nc_2_i = tp.add_column(nc_2)
        tp.add_parser(nc_1_3_i, 'column_1_3', ColumnParser())
        tp.add_parser(nc_1_3_i, 'index_1_3', index_parser)
        tp.add_parser(nc_2_i, 'column_2', ColumnParser())
        return tp

    def test_columns(self):
        raw_data, expected_result = random_columns()
        tp = TableParser()
        tp.data_in_row = False
        tp.has_header = False
        _, result = tp.extract_header_and_columns(raw_data)

        self.assertEqual(expected_result, result)

    def test_columns_header(self):
        raw_data, data = random_columns_with_headers()
        tp = TableParser()
        tp.data_in_row = False
        tp.has_header = True
        header, columns = tp.extract_header_and_columns(raw_data)
        expected_result = dict(zip(header, columns))

        self.assertEqual(data, expected_result)
