from unittest import TestCase

from config.test_jsonwriter import write_read, DebugSaveable
from sources.test_textFile import some_file_data
from dataanalyser.sources.textparser.basetextparser import StructureData
from dataanalyser.sources.textparser.iniparser import IniParser
from dataanalyser.sources.textparser.textcaster import TextToInt


class TestIniParser(TestCase):
    def test_parse(self):
        ip = self.make_iniparser()
        result = ip.parse_data(some_file_data[0:14])
        expected_result = StructureData()
        expected_result.singles = {'instrument': 'some spectrometer',
                                   'min_exposure': 10}
        self.assertEqual(result, expected_result)

    def test_save_load(self):
        ip = self.make_iniparser()
        ds = DebugSaveable(ip)
        new_ip = write_read(ds).param
        self.assertEqual(ip, new_ip)

    def make_iniparser(self):
        ip = IniParser()
        ip.add_data_field('instrument', 'Instrument', 'Spectrometer')
        ip.add_data_field('min_exposure', 'Spectrometer', 'MinExposure', TextToInt())

        return ip

    def test_get_data_fields(self):
        ip = self.make_iniparser()
        result = ip.parse_data(some_file_data[0:14])
        fields = set(result.couples.keys())
        fields.update(result.singles.keys())
        self.assertEqual(fields, set(ip.get_data_fields()))
