from unittest import TestCase
import numpy as np

from dataanalyser.sources.builtinfunctions import evaluate_expression


class TestEvaluateExpression(TestCase):
    def test_evaluate_expression(self):
        v = evaluate_expression('sin(a)', a=np.linspace(0, np.pi, 10))
        r_v = evaluate_expression('sin(a)', a=np.linspace(np.pi, 0, 10))
        self.assertTrue(np.allclose(v, r_v[::-1]))
