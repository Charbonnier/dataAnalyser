import os
import re
import numpy as np
from random import randint
from unittest import TestCase

from checker_utils import get_test_resources
from config.test_jsonwriter import write_read, DebugSaveable
from sources.textparser.test_tableparser import random_rows_with_headers, random_rows
from test_data.generate_MTF import generate_MTF_data_as_str
from dataanalyser.sources.textfile import MatchFirstLine, MatchLastLine, MatchLineNumber, \
    MatchLineRegex, MatchLineContent, TextFile, TextFileStructure
from dataanalyser.sources.filefinder import FindByRegex
from dataanalyser.sources.textparser.tableparser import NamedColumn, IndexColumn, TableParser, ColumnIndex, ColumnParser
from dataanalyser.structure.mutative import ValueSource

data_folder = './'

some_file_data = []
with open(os.path.join(get_test_resources(), 'MTF', 'setup.cfg'), 'r') as f:
    while True:
        line = f.readline()
        some_file_data.append(line)
        if not line:
            break


class TestTextFile(TestCase):
    def test_mutative(self):
        parser = self.make_mtf_parser()
        mm = parser.get_mutable_members()
        parameters = {}
        for mutative, mp in mm:
            for member in mp.keys():
                if mp[member].value_source.type == ValueSource.CONSTANT:
                    mp[member].value_source.set_value(None)
                else:
                    parameters[mp[member].value_source.value] = None
            mutative.apply_members_values(parameters)
        for mutative, mp in mm:
            for member in mp.keys():
                self.assertIsNone(getattr(mutative, member))

    def test_save_load(self):
        tf = self.make_mtf_parser()
        ds = DebugSaveable(tf)
        new_tf = write_read(ds).param
        self.assertEqual(tf, new_tf)

    def test_get_data_fields(self):
        parser = self.make_mtf_parser()
        parser.get_mutable_members()
        file = next(iter(self.make_finder().get_files_and_attributes().keys()))
        result = parser.evaluate({'file_path': file})
        fields = set()
        for table in result.tables:
            fields.update(table.fields)
        parser_fields = set(parser.get_data_fields())
        parser_fields.add((parser.name, 'c_to_f'))
        self.assertEqual(fields, parser_fields)

    def test_evaluate_1(self):
        returned_data = self.get_one_element_per_file()
        expected_data = {d['path']: d for d in generate_MTF_data_as_str()}
        for f, d in returned_data.items():
            self.my_assert_dict_equal(expected_data[f], d)

    def my_assert_dict_equal(self, d1, d2):
        self.assertEqual(d1.keys(), d2.keys())
        for k, item in d1.items():
            try:
                self.assertEqual((k, item), (k, d2[k]))
            except KeyError:
                self.fail('The answer misses argument keyword {}.'.format(k))
            except ValueError:
                self.assertTrue(np.array_equal(item, d2[k]))

    def test_evaluate_2(self):
        singles, couples = self.get_multiple_elements_per_file()
        expected_data = {d['path']: d for d in generate_MTF_data_as_str()}
        expected_intensities = {}
        for f, d in singles.items():
            expected_intensities[f] = expected_data[f].pop('intensities')
            self.my_assert_dict_equal(expected_data[f], d)
            expected_intensity_column = expected_intensities[f]
            for i, intensity in enumerate(couples[f]['intensities']):
                self.assertEqual(expected_intensity_column[i], intensity)

    def test_rename_field(self):
        mtf_parser = self.make_mtf_parser()
        mtf_parser.rename_field('date', 'Date')
        mtf_parser.rename_field('wavelength', 'Wavelength')
        file = next(iter(self.make_finder().get_files_and_attributes().keys()))
        mtf_parser.evaluate({'file_path': file})

    def get_one_element_per_file(self):
        ans = {}
        parser = self.make_mtf_parser()
        for file in self.make_finder().get_files_and_attributes():
            parcel = parser.evaluate({'file_path': file})
            self.pack_result(ans, file, parcel)
        return ans

    def pack_result(self, ans, file, parcel):
        ans[file] = {k: v[0] for k, v in parcel.tables[0].table.items()}
        ans[file]['path'] = file

    def get_multiple_elements_per_file(self):
        singles = {}
        couples = {}
        parser = self.make_mtf_parser()
        fs_list = [fs for fs in parser.file_structure]
        content_parser: TableParser = fs_list[1].structure_parser
        intensities_parser = ColumnIndex()
        intensities_parser.index = 0
        intensities_parser.step = 1
        intensities_parser.enable_stepping = True
        content_parser.columns_parsers[1]['intensities'] = intensities_parser
        for file in self.make_finder().get_files_and_attributes():
            parcel = parser.evaluate({'file_path': file})
            self.pack_result(singles, file, parcel)
            couples[file] = {k: v for k, v in parcel.tables[1].table.items()}
        return singles, couples

    def make_mtf_parser(self):
        p = TextFile()
        p.file_path.mutable_members['raw_path'].set_reference_value('file_path')
        header_structure = self.make_header_structure()
        content_structure = self.make_content_structure()
        p.append_file_structure(header_structure)
        p.append_file_structure(content_structure)
        return p

    def make_finder(self):
        finder = FindByRegex()
        raw_path = os.path.abspath(os.path.join(get_test_resources()))
        finder.search_path.mutable_members['raw_path'].set_constant_value(raw_path)
        finder.regex = r'Square_Pixel_x=([+-]?[0-9]+\.[0-9]{1})\.csv'
        for mm, _ in finder.get_mutable_members():
            mm.apply_members_values({})
        return finder

    def make_content_structure(self):
        content = self.make_content_parser()
        content_structure = TextFileStructure()
        content_structure.structure_parser = content
        content_structure.start = MatchLineNumber()
        content_structure.start.line_to_match = 3
        content_structure.end = MatchLastLine()
        content_structure.include_start = True
        content_structure.include_end = True
        return content_structure

    def make_header_structure(self):
        header = self.make_header_parser()
        header_structure = TextFileStructure()
        header_structure.structure_parser = header
        header_structure.start = MatchFirstLine()
        header_structure.end = MatchLineNumber()
        header_structure.end.line_to_match = 2
        header_structure.include_start = True
        header_structure.include_end = True
        return header_structure

    def make_table_parser(self, columns, columns_parsers, structure):
        for column, parsers in zip(columns,
                                   columns_parsers):
            column_index = structure.add_column(column)
            for name, parser in parsers.items():
                structure.add_parser(column_index, name, parser)

    def make_content_parser(self):
        # Content
        content = TableParser()
        content.data_in_row = False
        content.has_header = False
        content.delimiter = '\t'
        wavelength_content = IndexColumn(0, False)
        wavelength_content.mutable_members['index'].value_source.set_value(wavelength_content.index)
        intensity_content = IndexColumn(1, False)
        intensity_content.mutable_members['index'].value_source.set_value(intensity_content.index)

        columns = [wavelength_content, intensity_content]
        columns_parsers = [{'wavelength': ColumnParser()}, {'intensities': ColumnParser()}]

        self.make_table_parser(columns, columns_parsers, content)
        return content

    def make_header_parser(self):
        # Header
        header = TableParser()
        header.data_in_row = True
        header.has_header = True
        header.delimiter = '\t'
        columns = [(NamedColumn('Date:')),
                   (NamedColumn('Position:')),
                   (NamedColumn('Integration time:'))]
        columns_parsers = [{'date': (ColumnIndex(0, False))},
                           {'position': (ColumnIndex(0, False))},
                           {'integration time': (ColumnIndex(0, False))}]

        self.make_table_parser(columns, columns_parsers, header)
        return header


class TestMatchFirstLine(TestCase):
    def test_match(self):
        mfl = MatchFirstLine()
        expected_result = []
        result = []
        for i, line in enumerate(some_file_data):
            result.append(mfl.match(line))
            expected_result.append(i == 0)
        self.assertEqual(result, expected_result)

    def test_save_load(self):
        mfl = MatchFirstLine()
        new_mfl = write_read({'mfl': mfl})['mfl']
        self.assertEqual(mfl, new_mfl)


class TestMatchLastLine(TestCase):
    def test_match(self):
        mfl = MatchLastLine()
        expected_result = []
        result = []
        for line in some_file_data:
            result.append(mfl.match(line))
            expected_result.append(line == '')
        self.assertEqual(result, expected_result)

    def test_save_load(self):
        mll = MatchLastLine()
        new_mll = write_read({'mll': mll})['mll']
        self.assertEqual(mll, new_mll)


class TestMatchLineNumber(TestCase):
    def test_match(self):
        mln = MatchLineNumber()
        self.matchlinenumber(mln)
        mln.clear()
        self.matchlinenumber(mln)

    def matchlinenumber(self, mln):
        mln.line_to_match = randint(0, len(some_file_data) - 1)
        expected_result = []
        result = []
        for index, line in enumerate(some_file_data):
            result.append(mln.match(line))
            expected_result.append(index == mln.line_to_match)
        self.assertEqual(result, expected_result)

    def test_save_load(self):
        mln = MatchLineNumber()
        mln.line_to_match = 10
        new_mln = write_read({'mln': mln})['mln']
        self.assertEqual(mln, new_mln)


class TestMatchLineRegex(TestCase):
    def test_match(self):
        mlr = MatchLineRegex()
        mlr.regex = '^\[.+\]$'
        expected_result = []
        result = []
        for line in some_file_data:
            result.append(mlr.match(line))
            expected_result.append(re.match(mlr.regex, line[:-1]))

    def test_save_load(self):
        mlr = MatchLineRegex()
        mlr.regex = '^\[.+\]$'
        new_mlr = write_read({'mlr': mlr})['mlr']
        self.assertEqual(mlr, new_mlr)


class TestMatchLineContent(TestCase):
    def test_match(self):
        mlc = MatchLineContent()
        mlc.content = '[Spectrometer]'
        expected_result = []
        result = []
        for line in some_file_data:
            result.append(mlc.match(line))
            expected_result.append(re.match(mlc.content, line[:-1]))

    def test_save_load(self):
        mlc = MatchLineContent()
        mlc.content = '[Spectrometer]'
        new_mlc = write_read({'mlc': mlc})['mlc']
        self.assertEqual(mlc, new_mlc)


class TestNamedColumn(TestCase):
    def test_get_columns(self):
        _, data = random_rows_with_headers()
        header, columns = [], []
        for h, c in data.items():
            header.append(h)
            columns.append(c)
        nc = NamedColumn()
        nc.header_name = 'c'
        self.assertEqual(nc.get_columns(columns, header), [data['c']])

    def test_save_load(self):
        nc = NamedColumn()
        nc.header_name = 'c'
        new_nc = write_read({'nc': nc})['nc']
        self.assertEqual(nc, new_nc)


class TestIndexColumn(TestCase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        _, self.data = random_rows()

    def test_get_columns(self):
        index = randint(0, len(self.data) - 1)
        ic = IndexColumn()
        ic.index = index
        self.assertEqual(ic.get_columns(self.data), [self.data[index]])

    def test_get_columns_step(self):
        step = 3
        index = randint(0, 2)
        ic = self.make_index_columns(index, step)
        self.assertEqual(ic.get_columns(self.data), [self.data[i] for i in range(index, len(self.data), step)])

    def make_index_columns(self, index, step):
        ic = IndexColumn()
        ic.index = index
        ic.enable_stepping = True
        ic.step = step
        return ic

    def test_save_load(self):
        ic = self.make_index_columns(3, 5)
        new_ic = write_read({'ic': ic})['ic']
        self.assertEqual(ic, new_ic)
