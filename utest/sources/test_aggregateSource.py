from collections import OrderedDict
from unittest import TestCase
import numpy as np

from config.test_jsonwriter import write_read
from dataanalyser.sources.aggregate import AggregateSource
from dataanalyser.sources.function import FunctionSource, Function
from dataanalyser.structure.database import Cursor
from dataanalyser.structure.mutative import MutableMemberSetter, ValueSource


def identity_test(*args):
    return args


FunctionSource.register_function('test_agg', Function(identity_test, 'identity'))

cursor = Cursor('row_id')
cursor.fields_indexes = OrderedDict([('row_id', 0), ('group', 1), ('number', 2)])
groups = ['even', 'odd']
numbers = np.random.randint(0, 1000, 10)

for i,  n in enumerate(numbers):
    g = groups[n % 2]
    cursor.values.append([i, g, n])


class TestAggregateSource(TestCase):
    def test_save_load(self):
        agg = AggregateSource()
        agg.function_id = 'test_agg'
        mm_setter = agg.append_order()
        mm_setter.value_source = ValueSource(ValueSource.REFERENCE, 'order_0')
        mm_setter = agg.append_group()
        mm_setter.value_source = ValueSource(ValueSource.REFERENCE, 'group_0')
        new_agg = write_read({'agg': agg})['agg']

    def test_query_results(self):
        agg = AggregateSource()
        agg.function_id = 'test_agg'
        arg_a = MutableMemberSetter('a', 0)
        arg_a.value_source = ValueSource(ValueSource.REFERENCE, 'number')

        agg.set_positional_value(0, arg_a)

        setter = agg.groups.append(agg)
        setter.value_source = ValueSource(ValueSource.REFERENCE, 'group')
        agg.dispatch_result = True
        agg.returned_fields.append('out_a')

        results = None
        links = None
        for i, results, links in agg.query_results(cursor):
            pass
        print(results)
